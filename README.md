Sytare: SYsTème embArqué faible consommation à mémoiRE persistante
==================================================================

## Getting started

We use CMake as build system generator, so you currently need:

 * CMake 3.0 or greater
 * GNU Make
 * `msp430-elf` GCC and binutils
 * mspdebug for programming target boards


To build a basic Sytare application and program it to a board:

```bash
$ mkdir src/build
$ cd src/build
$ cmake ..
$ make app_leds
$ make program-app_leds
```

See [src/cmake/README.md](https://gitlab.inria.fr/citi-lab/sytare/blob/master/src/cmake/README.md)
for more details.


## Contributing

The `master` branch is the main development branch that will be updated by
GitLab Merge Requests for code contributions. Documentation updates can however
be pushed directly to `master` without the need for a Merge Request.
