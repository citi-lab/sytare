                                                                   Mar 26, 2008
 TEXAS INSTRUMENTS
   3535 40th Ave. NW, Suite 103
   ValleyHigh Business Center
   ROCHESTER, MN 55901

   Texas Instruments	
   PART NO. CC2500EM 062
   REV. A

   PCB DESCRIPTION: 2 LAYER PCB, .062" NOMINAL THICKNESS FR4 WITH 1OZ CU PER LAYER.
	DIMENSIONS IN MIL (.001 INCH)
	6 MIL MIN TRACE AND SPACE WIDTH
	SOLDERMASK TWO SIDES
	SILKSCREEN TOP SIDE ONLY
	

  
 THIS ARCHIVED (WINZIP) FILE "CC2500EM 062 RevA.ZIP" CONTAINS 25 FILES.

   CC2500EM 062.GTO = TOP SILKSCREEN GERBER FILE
   CC2500EM 062.GTS = TOP SOLDER MASK GERBER FILE
   CC2500EM 062.GTL = LAYER 1 TOP SIGNAL GERBER FILE
   CC2500EM 062.GBL = LAYER 2 BOTTOM SIGNAL GERBER FILE
   CC2500EM 062.GBS = BOTTOM SOLDER MASK GERBER FILE
   CC2500EM 062.GTP = TOP PASTE STENCIL GERBER FILE
   CC2500EM 062.GBP = BOTTOM PASTE STENCIL GERBER FILE
   CC2500EM 062.GG1 = DRILL GUIDE GERBER FILE
   CC2500EM 062.GD1 = DRILL DRAWIING GERBER FILE
   CC2500EM 062.GM3 = FAB DIMENSION GERBER FILE
   CC2500EM 062.GM4 = BOARD OUTLINE GERBER FILE
   CC2500EM 062.GM1 = TOP ASSEMBLY DRAWING GERBER FILE
   CC2500EM 062.GM5 = BOTTOM ASSEMBLY DRAWING GERBER FILE
   CC2500EM 062.DRR = DRILL SIZE REPORT
   CC2500EM 062.TXT = ASCII DRILL FILE
   CC2500EM 062.DRL = BINARY DRILL FILE
   CC2500EM 062.APR = APERTURE FILE
   README.TXT = THIS FILE
   PICK PLACE FOR CC2500EM 062.XLS = PICK AND PLACE SPREADSHEET
   CC2500EM 062 BOM RevA.XLS = BILL OF MATERIALS
   CC2500EM 062 ASCII.PCB = BOARD DESIGN IN PROTEL PCB V2.8 ASCII FORMAT
   CC2500EM 062 RevA.DDB = PROTEL BOARD DESIGN DATABASE FILE (DESIGN EXPLORER 99SE)
   CC2500EM 062 ARTWORK RevA.PDF = BOARD ARTWORK PDF FILE
   CC2500EM 062 SCHEMATIC RevA.PDF = SCHEMATIC PDF FILE
   CC2500EM 062 BOM RevA.PDF = BILL OF MATERIALS PDF FILE	

 
READ.ME
