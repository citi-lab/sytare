#!/usr/bin/env bash

if [ ! -d "$1" ]
then
    echo "Please specify a document folder as first argument."
    echo
    echo "Example:"
    echo "$0 my_doc"
    exit 1
fi

DIR="$(readlink -f $1)"
REPORT_MD="${DIR}.md"
OUTPUT="${DIR}.pdf"

PANDOC_FLAGS="--from=markdown+yaml_metadata_block --listings -H $(pwd)/tex/listings-setup.tex --latex-engine=xelatex"

INTRO_MD="$DIR/intro.md"
PREHOOK="$DIR/prehook.sh"

if [ -f "$PREHOOK" ] && [ -x "$PREHOOK" ]
then
	pushd $DIR
	"$PREHOOK"
	popd
fi

echo "clear report"
echo "" > $REPORT_MD

if [ -f "$INTRO_MD" ]
then
    echo "append intro ($(wc -l $INTRO_MD | cut -d' ' -f1) lines)"
    cat "$INTRO_MD" >> $REPORT_MD
    echo "" >> $REPORT_MD
fi

for doc in `find $DIR -name "*.md" ! -name "$(basename $INTRO_MD)" | sort`
do
    echo "append $doc ($(wc -l $doc | cut -d' ' -f1) lines)"
    cat "$doc" >> $REPORT_MD
    echo "" >> $REPORT_MD
    echo "\\newpage" >> $REPORT_MD
    echo "" >> $REPORT_MD
done


echo
echo "create pdf using pandoc and latex: $OUTPUT"

pushd $DIR
pandoc $REPORT_MD $PANDOC_FLAGS -o $OUTPUT
popd

echo "remove temporary markdown files"
rm $REPORT_MD
