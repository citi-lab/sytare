import numpy as np
import matplotlib.pyplot as plt

leds = {
	'old unconditional': lambda x: -195.906 / (-0.378837 + x) + 99.4456,
	'old dirty only':    lambda x: -204.199 / (-0.0317466 + x) + 99.4251,
	'new unconditional': lambda x: -199.376 / (-6.35895e-24 + x) + 99.5328,
	'new dirty only':    lambda x: -201.917 / (-7.27919e-23 + x) + 99.6013,
}

wsn = {
	'old unconditional': lambda x: -286.238 / (-7.12422e-16 + x) + 96.8272,
	'old dirty only':    lambda x: -338.625 / (-7.52204e-15 + x) + 97.4532,
	'new unconditional': lambda x: -293.564 / (-5.77204e-14 + x) + 96.2335,
	'new dirty only':    lambda x: -302.372 / (-5.47201e-16 + x) + 97.1976,
#	'gpio': lambda x: -255.112 / (-8.35966e-15 + x) + 97.1412,
}

wsn_pes_dirty = lambda x: -302.372 / (-5.47201e-16 + x) + 97.1976
wsn_pes_uncnd = lambda x: -293.564 / (-5.77204e-14 + x) + 96.2335
wsn_old_dirty = lambda x: -338.625 / (-7.52204e-15 + x) + 97.4532



def plot(app):
	x = np.linspace(5, 1200, 10000)

	for label, func in app.items():
		plt.plot(x, func(x), '-', label=label)

	plt.ylabel('Efficiency [%]')
	plt.xlabel('T_on [ms]')

	plt.grid(True)
	plt.legend(loc='lower right')


plt.figure(1, figsize=(16,9))
#plt.suptitle("apps/leds efficiency", fontsize=24)

plt.subplot(121)
plot(leds)
plt.ylim([0, 100])

plt.subplot(122)
plot(leds)
plt.ylim([98, 100])

plt.savefig('img/plt_leds.png', dpi=300, bbox_inches='tight', pad_inches=0)


plt.figure(2, figsize=(16,9))
#plt.suptitle("apps/wsn efficiency", fontsize=24)

plt.subplot(121)
plot(wsn)
plt.ylim([0, 100])

plt.subplot(122)
plot(wsn)
plt.ylim([95, 98])

plt.savefig('img/plt_wsn.png', dpi=300, bbox_inches='tight', pad_inches=0)

#plt.show()
