In this document we compare the system performance of the Sytare[^1] project on
two toy applications `apps/leds` and `apps/wsn` using Schlumpf[^2].

[^1]: [https://gitlab.inria.fr/citi-lab/sytare](https://gitlab.inria.fr/citi-lab/sytare)
[^2]: [https://gitlab.inria.fr/citi-lab/schlumpf](https://gitlab.inria.fr/citi-lab/schlumpf)

The comparison will be done based on two different states of project: a snapshot
taken on August 30th before merging the new driver interface
`save/register/restore` and after the merge, respectively call "old" and "new"
in the following.

For both states of Sytare, a performance benchmark has been done with the
vanilla version and a modified version that will save driver state
unconditionally, i.e. independent of any notion of dirtiness.


State               Git Hash
------------------- ---------
old unconditional   8ef58d2
old dirty only      9e27096
new unconditional   d666b8d
new dirty only      ea9c52b
------------------- ---------
Table: Application state mapping

\newpage


# apps/leds

The application displays the numbers from 1 to 63 on an LED strip and executes
for roughly one second. This only involves GPIO access and busy-waiting to
achieve the desried runtime to simulate some processing.

This is of course the most basic example and far-away from real world
applications. However, it can give an impression of the upper bound of system
performance because only general checkpointing and GPIO persistency contribute
to the overall overhead.

```c
for(count = 1; count < 64; count += 1) {
	display_leds(c)		// 10 syscalls
	delay(1/64s)
}
```

## Results

![Hyperbola fitted curves of apps/leds results](img/plt_leds.png)

Overall efficiency doesn't change a lot with a slight advantage for the new
driver interface. The results meet the expectations, i.e. handling dirtiness
improves performance and the new implementation is more efficient than the old.

However, all curves are that close to each other that it's hard to conclude that
the performances really differ in the end.


![apps/leds old dirty only](img/leds_old_dirty.png)

![apps/leds old unconditional](img/leds_old_unconditional.png)

![apps/leds new dirty only](img/leds_new_dirty.png)

![apps/leds new unconditional](img/leds_new_unconditional.png)


\newpage

# apps/wsn

The application sends 50 packets each with a 10x averaged temperature
measurement. Delays are employed so that the application has a runtime
of roughly one second.

This application is closer to the real world than `apps/leds` because
it uses a radio transceiver that is connected via an SPI bus and the
internal temperature sensor. The GPIO driver is used internally by the
SPI as well as the transceiver driver.

```c
for(count = 0; count < 50; count += 1) {
	sample = 0
	for(nsamples = 0; nsamples < 10; nsamples += 1) {
		sample += measure_temperature()		// 1 syscall
		delay(1ms)
	}

	pkt = sample / 10						// 4 Byte
	send(pkt)								// 3 syscalls

	delay(5ms)
}
```

## Results

![Hyperbola fitted curves of apps/wsn results](img/plt_wsn.png)

Unconditional device context saving is less favorable for this application,
probably because more drivers are used and therefore overall saving takes more
time.

All experiments except "old dirty only" show outliers between 25ms and 150ms. We
don't have a bulletproof explanation, but suspect that this is due to problems
with Schlumpf and increased currents due to the radio operation. That's why this
phenomenon doesn't show with app/leds. But it could also be some yet unknown
cause.


![apps/wsn old dirty only](img/wsn_old_dirty.png)

![apps/wsn old dirty only](img/wsn_old_dirty_big.png)


![apps/wsn old unconditional](img/wsn_old_unconditional.png)

![apps/wsn old unconditional](img/wsn_old_unconditional_big.png)


![apps/wsn new dirty only](img/wsn_new_dirty.png)

![apps/wsn new dirty only](img/wsn_new_dirty_big.png)


![apps/wsn new unconditional](img/wsn_new_unconditional.png)

![apps/wsn new unconditional](img/wsn_new_unconditional_big.png)


# Appendix

![microscopic effect: efficiency discretization](img/schlumpf_efficiency_steps.png)

The efficiency curve is actually discontinous at roughly `baseline / N` where N
is an integer. At each of these transitions, the total number of lifecycles for
an execution changes and for a fixed number of lifecycles, the efficiency is
constant.

This effect becomes best visible in the region approaching the baseline runtime.
In theory it should also be observable on the left side of the curve, but since
the intervals between two discontinuities decreases, it will look more or less
continous in the graphs obtained with Schlumpf.
