% Sytare sequence diagrams
%
% (this code is heavily based on pgf-umlsd.sty)
%
% TODO: 
% - automagically `\deactivate`  the last active lifeline  even if the
%   user forgets to do it
% - be smart when the same lifeline is `\deactivated` twice. So far,
%   each `\deactivate` overwrites the previous one, which silently
%   produces wrong but not obviously wrong results.
% - ensure that all our command names won't clash with other
%   packages (hard). 
% - also, decide whether we really want "graphical"-oriented command
%   names (e.g. bigcross) or "semantic"-oriented names (e.g. event)

\NeedsTeXFormat{LaTeX2e}[1999/12/01]
\ProvidesPackage{sytare-sd}[2017/06/30 Sytare sequence diagrams]

\RequirePackage{tikz}

\usetikzlibrary{backgrounds} % draw background after the foreground

\usetikzlibrary{arrows} % nicer arrows
% note 1: tikz 3.0 deprecates the `arrows` library
% note 2: loading `arrows.meta` changes the default arrow to `Computer Modern Rightarrow`

\newcounter{diagramtime} % tracks current time within diagram (in hardcoded units of .1em)
\newcounter{lifeline}    % counts lifelines in the diagram so that each one gets a unique number
\newcounter{poi}         % couts points of interest

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% User Interface

%%%%%%%%%%%%%%%%%%%%%
% add a lifeline to the diagram.
% usage:
% \lifeline[distance between labels]{tikz node name}{label text}
%
% lifeline declarations must come before any other content in the diagram
\newcommand{\lifeline}[3][1em]{
  \stepcounter{lifeline}
  % (the numexpr trick is from https://tex.stackexchange.com/a/210452/1769 )
  \path (lifeline\the\numexpr\value{lifeline}-1\relax.east)+(#1,0)
  node [rectangle,draw,anchor=west,text height=1.25ex,text depth=.25ex] (lifeline\thelifeline) {#3};
  % add an anchor at the start of the actual lifeline
  \path (lifeline\thelifeline.south) coordinate (#2) {};
}

%%%%%%%%%%%%%%%%%%%%%
% advance (or rewind) the vertical position in the diagram
% usage:
% \advancetime{10} or \advancetime{-100}
\newcommand{\advancetime}[1]{  \addtocounter{diagramtime}{#1} }

%%%%%%%%%%%%%%%%%%%%%
% draw a horizontal arrow between two lifelines, optionally with some label text above
% usage:
% \arrow[label text]{source tikz name}{destination tikz name}
\newcommand{\arrow}[3][]{
  \path (#2) + (0,-\thediagramtime*.1em) node (async from) {};
  \path (#3) + (0,-\thediagramtime*.1em) node (async to) {};

  \draw[->] (async from) -- (async to) node[midway, above, inner sep=1pt] {#1}; % IMO the default inner sep of .33em is too large
}

%%%%%%%%%%%%%%%%%%%%%
% draw a lightning arrow on the left of a lifeline, optionally with some label text above
% usage:
% \arrow{lifeline tikz name}
% \arrow[label text]{lifeline tikz name}
\newcommand{\event}[2][]{
  \path (#2) + (0,-\thediagramtime*.1em) node (event to) {};
  
 \draw [->] (event to) ++ (-3em,1ex)
  -- ++(1.5em,0) node [midway,above,inner sep=1pt] { #1 } % IMO the default inner sep of .33em is too large
  -- ++(-1ex,-1ex)
  -- (event to)  ;
  }

%%%%%%%%%%%%%%%%%%%%%
% draw a big cross to represent a crash (power failure)
% usage:
% \deactivate{lifeline tikz name} followed by \bigcross{lifeline tikz name}
\newcommand{\bigcross}[1]{
  \path (#1) +(0, -\thediagramtime*.1em) node [coordinate] (bigcross center) {};
  \draw [thick] (bigcross center) + (-.66em,-.66em) -- +(.66em,.66em) ;
  \draw [thick] (bigcross center) + (-.66em,.66em) -- +(.66em,-.66em) ;
}

%%%%%%%%%%%%%%%%%%%%%
% draw a small marker (dot) to identify a POI in the diagram
% usage:
% \marker{lifeline tikz name}
\newcommand{\marker}[1]{
  \path (#1)+(0, -\thediagramtime*.1em) node [draw,circle,fill,minimum size=4pt,inner sep=0pt] (now x) {}; }

%%%%%%%%%%%%%%%%%%%%%
% numbered referencable counters 
% usage:
% \noteleft{lifeline tikz name}{\numberedmarker}
\newcommand{\numberedmarker}{
  \stepcounter{poi}
  \textcircled{\tiny \thepoi}
}


%%%%%%%%%%%%%%%%%%%%%
% add a short note near (left or right of) a lifeline
% usage:
% \noteleft[hspace]{lifeline tikz name}{note text}
%
% `hspace` specifies  how much  space to leave  between note  and (the
% dotted line of) the lifeline (default=.66em)
\newcommand{\noteleft}[3][.66em]{
  \path (#2)+(0,-\thediagramtime*.1em) node [inner xsep=#1,anchor=east] {#3};
}
\newcommand{\noteright}[3][.66em]{
  \path (#2)+(0,-\thediagramtime*.1em) node [inner xsep=#1,anchor=west] {#3};
}

%%%%%%%%%%%%%%%%%%%%%
% draw a horizontal dashed line spanning several lifelines
\newcommand{\horizontalline}[2]{
  \path (#1)+(-2em,-\thediagramtime*.1em) node [coordinate] (hline left end) {} ;
  \path (#2)+(+2em,-\thediagramtime*.1em) node [coordinate] (hline right end) {} ;
  \draw[dashed] (hline left end) -- (hline right end) ;
}

%%%%%%%%%%%%%%%%%%%%%
% draw a grey rectangle over a lifeline to represent activity
%
% usage:
% \activate{lifeline tikz name}
% \advancetime{30} % or some other content
% \deactivate{lifeline tikz name}
%
% Notes:
% - `activate` and `deactivate`  must come in well-formed pairs
%    otherwise activity boxes will be drawn incorrectly !
% -  the rectangle  is about  .66em  in width  (twice "inner  sep")
%    because it is built around an empty (square) node

% top half
\newcommand{\activate}[1]{
  \path (#1)+(0, -\thediagramtime*.1em) node (activate #1) {};}

% bottom half
\newcommand{\deactivate}[1]{
  \path (#1)+(0, -\thediagramtime*.1em) node (deactivate #1) {};
  \begin{pgfonlayer}{sytaresd@activity}
  \draw [fill=gray!20] (activate #1.west) rectangle (deactivate #1.east) ;
  \end{pgfonlayer}
}

%%%%%%%%%%%%%%%%%%%%%
% the environment for a sequence diagram. really just a tikzpicture and some boilerplate
\newenvironment{sequencediagram}{

  \pgfdeclarelayer{sytaresd@background}
  \pgfdeclarelayer{sytaresd@activity}
  \pgfsetlayers{sytaresd@background,sytaresd@activity,main}

  \begin{tikzpicture}
    % reset counters
    \setcounter{diagramtime}{0}
    \setcounter{lifeline}{0}
    diagram origin (top left)
    \node [coordinate] (lifeline0) {};
  }
  {
    % finally draw the dotted lifelines themselves, below everything else 
      \begin{pgfonlayer}{sytaresd@background}
        \foreach \i [evaluate=\i] in {1,...,\thelifeline}{
          \draw[dotted] (lifeline\i.south) -- ++ (0, -\thediagramtime*.1em) ; 
        }
      \end{pgfonlayer}
    \end{tikzpicture}
}
