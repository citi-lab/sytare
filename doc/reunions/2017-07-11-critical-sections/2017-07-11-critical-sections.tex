\documentclass[a4paper,11pt, article]{memoir}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonts

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[final]{microtype}
\usepackage{lmodern}                      % vector version of default fonts
\usepackage{textcomp}                     % for textbullet



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Layout

\usepackage[margin=2cm]{geometry}

\usepackage{enumitem}
\setlist{nosep}

\setlength{\parindent}{0pt}

\setsecnumdepth{subsubsection}                   % default: section
\settocdepth{subsubsection}                      % default: section
\renewcommand*{\thesection}{\arabic{section}}
\renewcommand*{\thesubsection}{\thesection.\arabic{subsection}}

\setsecheadstyle{\Large\bfseries}                % default: \Large\bfseries
\setbeforesecskip{2ex plus 1ex minus 1ex}        % default: 3.5+1-.2 ex
\setaftersecskip{2ex plus 1ex minus 1ex}         % default: 2.3+.2
\setsubsecheadstyle{\large\bfseries}             % default: \large\bfseries
\setbeforesubsecskip{2ex plus 1ex minus 1ex}     % default: 3.25+1-.2 ex
\setaftersubsecskip{1ex plus .5ex minus .5ex}    % default: 1.5+.2

\setsubparaheadstyle{\itshape}          % default: \bfseries
\setsubparaindent{0pt}                           % default: \parindent

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Utilities and misc

\usepackage{url}
\usepackage{sytare-sd}

\usepackage{listings}
\makeatletter

\begin{document}

{\Large\bfseries Ensuring user data consistency and peripheral state consistency with user event handlers}

\section{Hypothesis}

\begin{itemize}
    \item User can define user event handlers to react upon event occurrence.
    \item User event handlers cannot be nested, but they can call syscalls.
    \item Application is running under Transiently-Powered System conditions.
\end{itemize}

\section{Motivation}

While attempting to expose interrupts to the user, we found out two problems:

\begin{enumerate}
    \item User data consistency between ``normal'' user code and user event handlers.
    \item Peripheral state and driver data consistency issue due to the fact kernel ISRs are run with highest priority and can alter peripheral state and driver data.
\end{enumerate}

\subsection{Illustration of problem 1}

UEH: User event handler.

$x$ is a variable in user space.

\bigskip
\begin{center}
\begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{ueh}{UEH}
    \lifeline{isr}{Kernel ISR}

    \advancetime{10}
    \activate{app}
    \advancetime{10}
    \noteleft{app}{$x=0$}
    \advancetime{20}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{isr}
    {
        \activate{isr}
        \advancetime{10}
        \deactivate{isr}
        \arrow{isr}{ueh}
        {
            \activate{ueh}
            \advancetime{10}
            \noteleft{ueh}{$x=1$}
            \advancetime{10}
            \deactivate{ueh}
            \arrow{ueh}{app}
        }
    }
    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \noteleft{app}{Assumes $x=0$ but it is no longer true}
\end{sequencediagram}
\end{center}

\subsection{Illustration of problem 2}

\subsubsection{Peripheral altered by ISR}
\begin{center}
\begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{ueh}{UEH}
    \lifeline{isr}{Kernel ISR}

    \advancetime{10}
    \activate{app}
    \advancetime{10}
    \noteleft{app}{\texttt{syt\_radio\_mode\_tx()}}
    \advancetime{20}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{isr}
    {
        \activate{isr}
        \advancetime{10}
        \noteleft{isr}{\texttt{radio\_mode\_rx()}}
        \advancetime{10}
        \noteleft{isr}{\texttt{radio\_sink\_packet()}}
        \advancetime{10}
        \deactivate{isr}
        \arrow{isr}{ueh}
        {
            \activate{ueh}
            \advancetime{10}
            \advancetime{10}
            \deactivate{ueh}
            \arrow{ueh}{app}
        }
    }
    \activate{app}
    \advancetime{10}
    \noteleft{app}{Assumes that radio is in TX mode but it is no longer true}
    \advancetime{15}
    \noteleft{app}{\texttt{syt\_radio\_send\_packet(...)}}
    \advancetime{10}
    \deactivate{app}
\end{sequencediagram}
\end{center}

\subsubsection{Peripheral altered by user event handler}
\begin{center}
\begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{ueh}{UEH}
    \lifeline{isr}{Kernel ISR}

    \advancetime{10}
    \activate{app}
    \advancetime{10}
    \noteleft{app}{\texttt{syt\_radio\_mode\_tx()}}
    \advancetime{20}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{isr}
    {
        \activate{isr}
        \advancetime{30}
        \deactivate{isr}
        \arrow{isr}{ueh}
        {
            \activate{ueh}
            \advancetime{10}
            \noteleft{ueh}{\texttt{syt\_radio\_mode\_sleep()}}
            \advancetime{10}
            \deactivate{ueh}
            \arrow{ueh}{app}
        }
    }
    \activate{app}
    \advancetime{10}
    \noteleft{app}{Assumes that radio is in TX mode but it is no longer true}
    \advancetime{15}
    \noteleft{app}{\texttt{syt\_radio\_send\_packet(...)}}
    \advancetime{10}
    \deactivate{app}
\end{sequencediagram}
\end{center}

\section{Solution to problem 1 - user data consistency}

This problem is a concern only for user data, a memory area that is used by ``normal'' user code and user event handlers.
Hence neither the kernel nor the device drivers can corrupt the contents of user data memory.
As far as user data is concerned, there is no race condition between userland and kernel/drivers.
There is no need to disable interrupts when accessing user data.

However, in order to remove concurrency issues between ``normal'' user code and user event handlers when accessing user data, a solution could be to prevent user event handlers from running when the application developer needs a code section to be data-consistent.
Since only the application developer knows when a code section needs to be protected or not, she is in charge of telling Sytare about such protected sections.
As user event handlers cannot be preempted by ``normal'' user code, this problem is not bidirectional, which means that the solution does not need to be bidirectional.

In this document, these protected sections are called \textbf{event-protected sections}.

\subsection{Semantics}

An event-protected section depicts a portion of user code where all hardware IRQs are enabled but user event handlers cannot run.
User event handlers are queued by kernel on event occurrence instead.

If user event handlers are \textit{not nested}, event-protected sections are not to be used inside user event handlers since within this hypothesis user event handlers cannot be preempted by code that alters user data.

\subsection{In TPS}

As IRQs are still enabled while running an event-protected section, usual checkpointing mechanisms can be used.
There is no specific constraint.

\subsection{Drawbacks}

This solution needs a user event handler storage stored in kernel memory to reduce user event handler loss (\textit{e.g.}, FIFO).
We have to define a behavior when the storage overflows (\textit{e.g.}, ignore event).

In addition, a policy must be defined when the same event occurs several times before the queue is emptied.
Since the same user event handler is to be called, should the queued events be merged into one single event?

\subsection{Example}

\begin{verbatim}
...
syt_events_disable();
...
syt_events_enable(); // Here kernel preempts user code and runs its
                     // user event handler FIFO until completion
...
\end{verbatim}

UEH: User event handler.

$x$ is a variable in user space.

\bigskip
\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{ueh}{UEH}
    \lifeline{ker}{Kernel}
    \lifeline{isr}{ISR}

    \advancetime{10}
    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \arrow[\scriptsize syt\_events\_disable()]{app}{ker}
    {
      \activate{ker}
      \advancetime{20}
      \deactivate{ker}
      \arrow[\scriptsize $return$]{ker}{app}
    }
    \activate{app}
    \advancetime{20}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{isr}
    {
      \activate{isr}
      \advancetime{10}
      \noteright{isr}{enqueue\_event()}
      \advancetime{10}
      \deactivate{isr}
      \arrow{isr}{app}
    }
    \activate{app}
    \advancetime{10}
    \noteleft{app}{$x=0$}
    \advancetime{10}
    \deactivate{app}
    \arrow[\scriptsize syt\_events\_enable()]{app}{ker}
    {
      \activate{ker}
      \advancetime{20}
      \deactivate{ker}
      \arrow[\scriptsize $schedule$]{ker}{ueh}
      {
        \activate{ueh}
        \advancetime{20}
        \noteleft{ueh}{$x=1$}
        \advancetime{20}
        \deactivate{ueh}
        \arrow{ueh}{app}
      }
    }    
    \activate{app}
    \advancetime{10}
    \deactivate{app}
\end{sequencediagram}
\end{center}

\section{Solution to problem 2 - peripheral state consistency}

This problem is a concern for all actors that have access to peripherals, \textit{i.e.}, ``normal'' user code, kernel ISRs and potentially user event handlers if we allow them to use peripherals.

The worst case of race conditions for peripheral state is when ``normal'' user code is using a peripheral through a sequence of syscalls and a hardware IRQ occurs, leading to the preemption of the code section in order to run driver code that alters the state of the same peripheral.
When the kernel ISR returns, the peripheral might not be in the state the user expected during the preempted code section.

\begin{lstlisting}[language=C,frame=l]
  spi_config(RADIO);
  radio_change_mode(TX);
  radio_send_packet(packet);
\end{lstlisting}


A solution to this problem could be to prevent hardware interrupts from occurring when the application developers runs code that must be peripheral-consistent.
As with problem 1, the application developer knows the dependencies between a given code section and peripherals, and she knows whether two calls to syscalls must be in a peripheral-consistent section or not.
Then she must tell Sytare which code sections are peripheral-consistent.

In this document, such peripheral-consistent sections are called \textbf{IRQ-protected} sections.

\subsection{Semantics}

IRQ-protected sections depict portions of code where IRQs are totally disabled.
As a consequence user event handlers are also disabled since they run upon event occurrence.

There are two options \textit{inside user event handlers}:

\begin{itemize}
    \item IRQs are disallowed in the semantics of user event handlers, thus they do not need IRQ-protected sections.
    \item IRQs are allowed in the semantics of user event handlers, then they may or may not use IRQ-protected sections.
\end{itemize}

\subsection{In TPS}
\label{in-tps}

Interrupts are disabled, which means that powerloss cannot be detected.
Depending on the contents of the IRQ-protected sections provided by the application developer, there might be lifecycles very difficult to overcome to the extent that they are likely to fail.
Since this solution was designed for situations where the application developer wanted to run several syscalls, potentially long and energy-consuming syscalls, the lifecycles that contain IRQ-protected sections will likely fail.

\subsection{Drawbacks}

See section~\ref{in-tps}.

\subsection{Example}

\begin{verbatim}
...
syt_irq_disable();
...
syt_irq_enable();
...
\end{verbatim}

\subsection{Possible implementation}

\begin{enumerate}
    \item \texttt{syt\_irq\_[dis|en]able() =} MSP430 macros \texttt{irq\_[dis|en]able()}
    \item \texttt{syt\_irq\_disable() = irq\_disable(), checkpoint()}\\
\texttt{syt\_irq\_enable() = irq\_enable()}
\end{enumerate}

    Implementation 2 does a normal checkpoint as if a powerloss detection occurred. Then, if a powerloss occurs when interrupts are disabled, on next boot the application will resume at the beginning of the IRQ-protected section instead of the beginning of the previous lifecycle. Hence chances to overcome the IRQ-protected section increase but it is still not guaranteed that the application eventually completes.

\section{Runtime priority}

Ordered from highest priority to lowest priority:

\begin{enumerate}
    \item User IRQ-protected sections + syscalls
    \item Kernel ISRs: save volatile peripheral data into device contexts
    \item User event-protected sections + syscalls if any
    \item User event handlers + syscalls if any and allowed
    \item "Normal" user code + syscalls if any
\end{enumerate}

All transitions between any of these modes must respect the priority constraints stated above to ensure both user data consistency and peripheral state consistency throughout execution.

\section{Conclusion}

A solution to user data consistency problem is to let the application developer decide when user event handlers must be disabled.
A solution to peripheral state consistency problem is to let the application developer decide when hardware IRQs must be disabled.

Basically IRQ-protected sections are a superset of event-protected sections, which means that we could use IRQ-protected sections to answer both formerly stated problems.
However within TPS, even more than with steadily-supplied devices, we want to avoid situations where IRQs are disabled as much as possible.
Hence the event-protected sections are more like optimizations that enable the system to react upon powerloss detection.
In addition event-protected sections improves interrupt latency as the hardware needs are still satisfied by the kernel, with respect to critical sections with interrupt disabled that cannot satisfy hardware needs.

The solutions proposed here have an impact on application code.
However it is the price to pay to give the application developer better control and correctness over her application.
Another argument in favor of such solutions is the fact that baremetal applications already have user-specified critical sections where hardware interrupts are manually disabled.
Here the application developer only has to determine whether the critical section should be either IRQ-protected or event-protected.

\section{Notes}
\label{sec:notes}

\begin{itemize}
\item Préciser la configuration attendue des drivers à chaque syscall
\item 
\end{itemize}

\end{document}
