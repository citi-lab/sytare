\documentclass[a4paper,11pt, article]{memoir}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonts

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[final]{microtype}
\usepackage{lmodern}                      % vector version of default fonts
\usepackage{textcomp}                     % for textbullet



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Layout

\usepackage[margin=2cm]{geometry}

\usepackage{enumitem}
\setlist{nosep}

\setlength{\parindent}{0pt}

\setsecnumdepth{subsubsection}                   % default: section
\settocdepth{subsubsection}                      % default: section
\renewcommand*{\thesection}{\arabic{section}}
\renewcommand*{\thesubsection}{\thesection.\arabic{subsection}}

\setsecheadstyle{\Large\bfseries}                % default: \Large\bfseries
\setbeforesecskip{2ex plus 1ex minus 1ex}        % default: 3.5+1-.2 ex
\setaftersecskip{2ex plus 1ex minus 1ex}         % default: 2.3+.2
\setsubsecheadstyle{\large\bfseries}             % default: \large\bfseries
\setbeforesubsecskip{2ex plus 1ex minus 1ex}     % default: 3.25+1-.2 ex
\setaftersubsecskip{1ex plus .5ex minus .5ex}    % default: 1.5+.2

\setsubparaheadstyle{\itshape}          % default: \bfseries
\setsubparaindent{0pt}                           % default: \parindent

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Utilities and misc

\usepackage{url}
\usepackage{sytare-sd}

\usepackage{listings}
\makeatletter

\begin{document}

{\Large\bfseries New solution to peripheral state and driver data consistency between user code and kernel ISRs}

While document from July 11th describes two problems and one solution for each, this document focuses on the second one and provides another solution, less contraining than the first one.

To begin with, the two identified problems were:
\begin{enumerate}
    \item User data consistency between ``normal'' user code and user event handlers.
    \item \textbf{Peripheral state and driver data consistency issue due to the fact kernel ISRs are run with highest priority and can alter peripheral state and driver data.}
\end{enumerate}

Here we define two \textbf{orthogonal} approaches, meaning that they can be combined, to solve problem 2.

\section{Approach 1: tuning syscall granularity}

\subsection{Description of the problem}

Problem 2 exists whenever the user needs to run a certain sequence of syscalls.
Indeed, within a given sequence of syscalls, the user makes the assumption that the peripherals states have not changed between two consecutive syscalls.
With interrupts enabled and no specific protection, this assertion is no longer true when the \textit{top half} (kernel-side, linux definition) part of any ISR calls driver functions.

For instance, if there are two SPI peripherals, any access to each SPI peripheral assumes that the SPI bus is properly configured.
In other terms, there is a precedence constraint between any syscall involving an SPI peripheral and a former call to \texttt{syt\_spi\_config()}.

\subsection{Is it avoidable?}

In the former example, precedence constraint between SPI-dependent syscall and SPI configuration actual call, a solution could be to add the SPI configuration as a parameter to every SPI-dependent syscall.

On the other hand, syscalls may also be dependent due to the device state machine like in the following code:
\begin{lstlisting}[language=C,frame=l]
syt_radio_set_mode(TX);
syt_radio_send_packet(packet);
\end{lstlisting}
In this case, since sending a packet makes no sense when the radio is not in transfer mode, the syscall that sends packets could also set the radio mode to transmission mode.
Then no syscall sequence would be required as one could send a packet with the radio being initially in any mode using a single syscall.

\subsection{Drawbacks}

Depending on the implementation of such a higher level syscall set, syscall execution time could increase substantially.
Within the SPI example, if the configuration is reset at the beginning of every SPI-dependent syscall, all SPI-dependent syscalls could be heavily impacted.
If there is a way of checking whether the current SPI configuration already matches the desired configuration, the full SPI configuration reset can be avoided, but the checking function itself adds its overhead.
Longer syscalls mean increasing the risks of not being able to complete syscall execution.

Another drawback, which is only implementation-dependent, is that in Sytare 2016 parameters are passed from user code to syscalls using registers and storing parameters in stack is prohibited by Sytare's contract (the compiler would not yield any error however).
If a pointer to a device configuration must be provided by the user for every single syscall that relies on some other peripheral, there would be one fewer parameter slot.

\section{Approach 2: cooperative critical sections}

This approach is closer to the IRQ-protected solution proposed in the document from July 11th.
It aims at reducing the constraints induced by the use of critical sections.

Instead of completely disabling IRQs, we propose here to keep them enabled but the user can be granted exclusive access to peripherals she requested.
The user can acquire exclusive access to one or more peripherals through a lock system, and then release the lock.

The only pieces of code within the kernel that could interfere with user code as far as peripherals are concerned are ISRs top halves.
As the kernel knows which driver functions must be called during the top halves, it simply \textbf{ignores the top halves that need access to locked peripherals}.
The corresponding bottom halves (user event handlers) are also discarded (they are not queued).
However \textbf{the top halves that do not rely on locked peripherals are run}, and their bottom halves are queued as usual.
This mechanisms removes peripheral access concurrency between user code and kernel, but does not address peripherall access concurrency between user code and user event handlers that can occur during the execution of a peripheral-locked section if no additional mechanism is added.

In order to maintain consistency between user peripheral-locked sections and user event handlers, \textbf{all user event handlers are postponed to the releasing of all peripheral locks}.
Hence they can no longer interfere with user code.
Since bottom halves do not have strong time constraints, postponing user event handlers execution could be accecptable.

Let's take the following code with precedence dependences:
\begin{lstlisting}[language=C,frame=l]
syt_spi_config(radio_config);
syt_radio_set_mode(TX);
syt_radio_send_packet(packet);
\end{lstlisting}

Using the peripheral lock system, it would become:
\begin{lstlisting}[language=C,frame=l]
syt_exclusive_access_begin(SPI|RADIO); // Acquire locks of SPI and radio

syt_spi_config(radio_config);
syt_radio_set_mode(TX);
syt_radio_send_packet(packet);

syt_exclusive_access_end(SPI|RADIO); // Release locks of SPI and radio
\end{lstlisting}

The following sequence diagram shows how the proposed solution works with user code above.
The system is equipped with a built-in timer, an SPI bus and two SPI slaves (radio and external ADC).

\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{ueh}{Timer UEH}
    \lifeline{spi}{Drv SPI}
    \lifeline{rad}{Drv SPI radio}
    \lifeline{tim}{Drv timer}
    \lifeline{adc}{Drv SPI ADC}
    \lifeline{ker}{Kernel}

    \advancetime{10}
    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \arrow[\scriptsize syt\_exclusive\_access\_begin(...)]{app}{ker}
    {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize $return$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \arrow[\scriptsize syt\_spi\_config(...)]{app}{ker}
    {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize spi\_config(...)]{ker}{spi}
      {
          \activate{spi}
          \advancetime{10}
          \deactivate{spi}
          \arrow[\scriptsize $return$]{spi}{ker}
      }
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize $return$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}

    \event[\scriptsize IRQ SPI\_ADC]{app}
    \arrow[\scriptsize ISR]{app}{ker}
    {
      \activate{ker}
      \advancetime{5}
      \noteright{ker}{\scriptsize ignore}
      \advancetime{5}
      \deactivate{ker}
      \arrow[\scriptsize $reti$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}

    \event[\scriptsize IRQ Timer]{app}
    \arrow[\scriptsize ISR]{app}{ker}
    {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}

      \arrow[\scriptsize timer\_on\_overflow()]{ker}{tim}
      {
          \activate{tim}
          \advancetime{10}
          \deactivate{tim}
          \arrow[\scriptsize $return$]{tim}{ker}
      }

      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize $reti$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}

    \arrow[\scriptsize syt\_radio\_set\_mode(TX)]{app}{ker}
    {
        \activate{ker}
        \advancetime{10}
        \deactivate{ker}
        \arrow[\scriptsize radio\_set\_mode(TX)]{ker}{rad}
        {
          \activate{rad}
          \advancetime{10}
          \deactivate{rad}

          \arrow[\scriptsize spi\_write(...)]{rad}{spi}
          {
              \activate{spi}
              \advancetime{10}
              \deactivate{spi}
              \arrow[\scriptsize $return$]{spi}{rad}
          }

          \activate{rad}
          \advancetime{10}
          \deactivate{rad}
          \arrow[\scriptsize $return$]{rad}{ker}
        }
        \activate{ker}
        \advancetime{10}
        \deactivate{ker}
        \arrow[\scriptsize $return$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}

    \arrow[\scriptsize syt\_radio\_send(...)]{app}{ker}
    {
        \activate{ker}
        \advancetime{10}
        \deactivate{ker}
        \arrow[\scriptsize radio\_send(...)]{ker}{rad}
        {
          \activate{rad}
          \advancetime{10}
          \deactivate{rad}

          \arrow[\scriptsize spi\_write(...)]{rad}{spi}
          {
              \activate{spi}
              \advancetime{10}
              \deactivate{spi}
              \arrow[\scriptsize $return$]{spi}{rad}
          }

          \activate{rad}
          \advancetime{10}
          \deactivate{rad}
          \arrow[\scriptsize $return$]{rad}{ker}
        }
        \activate{ker}
        \advancetime{10}
        \deactivate{ker}
        \arrow[\scriptsize $return$]{ker}{app}
    }

    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \arrow[\scriptsize syt\_exclusive\_access\_end(...)]{app}{ker}
    {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize run postponed user event handlers]{ker}{ueh}
    }

    \activate{ueh}
    \advancetime{10}
    \deactivate{ueh}
    \arrow[\scriptsize $return$]{ueh}{app}

    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \advancetime{10}
\end{sequencediagram}
\end{center}

User application first acquires SPI and radio locks.
When the system receives SPI ADC hardware interrupt, the kernel discards both top and bottom halves as the SPI lock is currently taken by user.
However, when the system receives timer hardware interrupt, the kernel runs the corresponding top half and queues the bottom half since timer top half does not interfere with neither SPI bus nor radio.
But the timer user event handler is not yet fired since all peripherals are not unlocked.
When the user releases SPI, the ADC interrupt is not fired again since it was discarded by kernel during peripheral-locked section execution.
When all peripherals are released, the user event handler corresponding to the bottom half of the timer interrupt is fired.

\section{Conclusion}

The need for a complex mechanism to prevent peripheral coherence issues between application code and ISRs top halves could be decreased by providing a higher level syscall set.
It would not solve the problem completely but would actually decrease the amount of cases where a critical section would be required.

Critical sections that disable IRQs would make it hard for the application to complete.
Worst case scenario is when a critical section is so large that the whole energy budget of the current lifecycle is consumed entirely before the critical section ends, even if the critical section starts at the earliest moment possible.
Then the system would loop on the cycle \texttt{(boot, critical section start, actual powerloss, off)}.
Since IRQs are disabled, powerloss detection cannot occur.

An alternative solution is to keep IRQs enabled and to provide locks for each peripheral.
Locks are acquired and released by the user.
When a peripheral is locked, all kernel interrupt top halves that depend on this peripheral are disabled, and their corresponding bottom halves are discarded as well.
In addition all user event handlers are postponed to the unlocking of all peripherals.
This approach is far less constraining than the former IRQ-disabling approach as IRQs remain enabled.
This means that powerloss detection can occur and any progress during the locked section will eventually be saved for next lifecycle.
In addition this approach enables independent top halves to be run, resulting in a better interrupt latency for all peripherals that do not rely on currently locked peripherals.

\end{document}
