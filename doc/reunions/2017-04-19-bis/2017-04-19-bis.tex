\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{listings}
%\usepackage[margin=1in, paperwidth=9in, paperheight=4in]{geometry}

\usepackage{color}
 
\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolor}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolor},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\scriptsize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}
 
\lstset{style=mystyle}

\usepgfplotslibrary{statistics}

\title{Simulating Energy Harvesting at a Hardware Level}

\begin{document}
\maketitle
\section{Purpose}
In former works, the target microcontroller and peripherals were powered by a signal generator that drove the voltage risings and the voltage fallings.
Using this setup, the observed voltage drop was thus not due to the powered board energy consumption, but due to the generator that drives the voltage based on the requested user parameters instead.
Hence reading the supply voltage is absolutely irrelevant for energy evaluation purposes.

To enable investigation in energy evaluation from the microcontroller point of view, a new setup where only the board draws current from the energy source is needed.

The aim of simulating energy harvesting is to enable development of energy-aware solutions, without having to build an actual energy harvester.
Since the supplied board does not care about how energy was acquired, it is reasonable to simulate energy harvesting through a device that can be, in a first phase supplied by an \textit{infinite} power source, and in a second phase that can supply the board without the infinite power source connected to the circuit.

\section{Energy model}
A system involving a capacitor was designed for simplicity, both on the electrical design aspect and the energy consumption evaluation on the target microcontroller.
The capacitor stores energy and uses it to supply the microcontroller.

\bigskip

Using such an energy model gives an estimation of the total remaining energy based on the supply voltage read by the microcontroller: $E(t) = \frac{1}{2} CV^2(t)$ where $E(t)$ is the total remaining energy, $C$ the capacitor value and $V(t)$ the supply voltage.
However total remaining energy does not really make sense since the microcontroller shuts down when the supply voltage drops below $V_{off} = 2V$.
Hence exploitable energy is defined so:
\begin{equation}
    E_{exp}(t) = \frac{1}{2} C (V^2(t) - V_{off}^2)
\end{equation}

This formula also enables the microcontroller to determine the energy consumption of some operation -- specific syscall execution for instance.
Let $t_{start}$ and $t_{end}$ respectively be the starting time and the ending time of the execution of some syscall.
Assuming that \emph{the capacitor is not given any energy from any external source when powering the microcontroller} (hypothesis \emph{H1}), energy consumption can be computed as follows:
\begin{equation}
    \Delta E = \frac{1}{2} C (V^2(t_{end}) - V^2(t_{start}))
\end{equation}
Note that $\Delta E$ is negative since the voltage is decreasing over time.

The actual value of energy consumption also depends on which peripherals are powered on and what their states are in the meantime, but this problematic is left to the application developer (hypothesis \emph{H2}).
To illustrate this problematic, the energy consumption of sending a radio packet when no LED is on would be different than that of sending a radio packet when four LEDs are on; despite the fact that in both cases the same syscall was called.

Another possible source of error in evaluating energy consumption is that the current that is drawn from the source is in general increasing over supply voltage, which means that depending on the supply voltage, the same syscall with the very same peripheral state might differ in energy consumption.
A more precise model must be defined here (define hypothesis \emph{H3}), for example considering the energy consumption voltage-independant, or linear with respect to supply voltage, \textit{etc.}. 

%\bigskip
%Before the capacitor powers the target board, it must be charged first.

\section{Solution description}
\subsection{Logic overview}
\begin{figure}
    \centering
    \includegraphics[]{logic.pdf}
    \caption{High level overview of the whole system.}
    \label{fig:logic}
\end{figure}

The energy harvesting simulator circuit is driven by three digital input pins: \textit{Ch} (charge), \textit{Pow} (power) and \textit{SC} (short-circuit).
Figure~\ref{fig:logic} shows how they interact within the whole system.
Its different working modes are depicted in table~\ref{tab:modes}.

\begin{table}
    \centering
    \begin{tabular}{l | c | c | c}
        Mode & Ch & Pow & SC \\
        \hline
        Stable & 0 & 0 & 0 \\
        \hline
        Charge & 1 & 0 & 0 \\
        \hline
        Power board & 0 & 1 & 0 \\
        \hline
        Short-circuit & 0 & 0 & 1 \\
        \hline
        Do not try & 0 & 1 & 1 \\
                   & 1 & 0 & 1 \\
                   & 1 & 1 & 0 \\
                   & 1 & 1 & 1 \\
    \end{tabular}
    \caption{Logical values of \textit{Ch}, \textit{Pow} and \textit{SC} pins and their corresponding modes.}
    \label{tab:modes}
\end{table}

\subsection{Circuit design}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{design.png}
    \caption{Electrical design}
\end{figure}

Here the 67$\Omega$ resistor represents the board to supply.
The PNP transistor is BC328 and all the NPN transistors are BC639.

This circuit was first designed to be controlled by a Raspberry Pi 3.
Its +5V potential was preferred over its +3.3V counterpart because the +5V pins can safely deliver 1 A in total (300 mA for Raspberry Pi mod B) whereas the +3.3V pins can only deliver 50 mA in total.
In addition using a +5V potential enables the capacitor to be charged at higher voltages than 3.3V such as 3.6V that is supported by MSP430FR5739 microcontroller.

\subsubsection{Stable mode}
This mode has two purposes: on boot when the GPIOs of the controller are not fully initialized, when connected to pull-down resistors the circuit is guaranteed to be in stable mode where no harm can occur on the energy source or on the target board.
It is also useful in waiting phases, debugging phases or measurement phases since the capacitor voltage remains the same during stable mode.
However as the capacitor is not ideal, it will nonetheless discharge slowly.

In stable mode, all transistor act like open switches.
Very little current is drawn from the energy source and the capacitor stays at the same voltage.

\subsubsection{Charge mode}
This mode enables the capacitor to accumulate energy and increase its voltage, while not powering the target board.

The PNP transistor and the NPN transistor connected to \textit{Ch} act like closed switches. The other transistors are open.

The circuit part that involves the capacitor is a simple RC circuit where $R_c = 470 \Omega$ and $C = 220 \mu F$.
A high resistor value enables the capacitor charge more slowly and thus enables finer timing configurations to raise the capacitor voltage to a certain value without precise timers.
Let $\alpha$ be the ratio between desired capacitor voltage and supply voltage $E$ (5V), then the charge time $t_c$ needed to reach that desired capacitor voltage is:
\begin{equation}
    t_c = - R_c C \ln(1 - \alpha)
\end{equation}

As a result, if $V_C(0) = 0V$ and the capacitor is charged during $t_c$ seconds, its final value will be $V_C(t_c) = \alpha E$.

\subsubsection{Power mode}
This mode connects the capacitor with the target board while having the infinite energy source disabled.

The NPN transistor connected to \textit{Pow} acts like a closed switch.
The other transistors are open.

In this mode, the current is driven by the target board.
The 33 $\Omega$ resistor protects the circuit against potential short-circuits that might occur on the target board. 
However, the resistor voltage is subtracted from the capacitor voltage and as a result the target board supply voltage is less than expected.
For exemple, if the board draws 20 mA from the capacitor, the 33 $\Omega$ resistor voltage will be 660 mV, which is not negligible.

Let $I_{avg}$, $t_d$ and $\Delta V$ respectively be the average current consumption of the target board \emph{during next lifecycle}, the duration of next lifecycle and the voltage drop of the capacitor until it is below the threshold voltage to keep the board on. $\Delta V = V_C(0) - V_{off}$.
Then we have:
\begin{equation}
    I_{avg} t_d = C \Delta V
\end{equation}

Note that $I_{avg} t_d$ can be noted as $Q_{budget}$, being the charge budget that is left to the board.

\subsubsection{Short-circuit mode}
This mode enables the capacitor to be discharged fast to nearly 0 V.
Since the charge time depends on both the initial capacitor voltage and the desired capacitor voltage, using short-circuit mode sets the initial capacitor voltage to 0 V.
Hence there is no need for the controller to read the actual value of the capacitor voltage to base its computation on.
Reading the value of the capacitor voltage is unwanted here since it would draw some current and modify the value as it is read.

The NPN transistor connected to \textit{SC} acts like a closed switch.
The other transistors are open.

\subsection{Maximum ratings}
See table~\ref{tab:max}.

\begin{table}
    \centering
    \begin{tabular}{l|r}
        Phase & $I_{max}$ (mA) \\
        \hline
        Charge -- capacitor & 11 \\
        Charge -- leak      & 2 \\
        \hline
        Short-circuit -- $V_C >$ 3.6V \protect\footnotemark& 151 \\
        Short-circuit -- $V_C <$ 3.6V & 110 \\
        \hline
        GPIO at logical 1 \protect\footnotemark & 3.3\\
    \end{tabular}
    \caption{Maximum current values, drawn from the energy source.}
    \label{tab:max}
\end{table}
\footnotetext{Short-circuit mode draws current from the capacitor, not from the infinite energy source.} 
\footnotetext{This value is for a single GPIO, but they are all mutually exclusive so they do not sum up.} 

\section{Implementation}
\subsection{Hardware and software}
The circuit was tested with a Raspberry Pi 3 as controller and a 2A power supply.

Control pins \textit{Ch}, \textit{Pow} and \textit{SC} were wired to pulled-down pins so that on boot the circuit is in stable state.
On a Raspberry Pi, GPIOs 16, 20 and 21 are examples of such pulled-down pins.

The circuit is controlled by a Python 3 script (see section~\ref{sec:code}) on top of a Raspbian distribution, using \texttt{wiringpi} module.
This choice was made for simplicity.
However it is questionnable as Linux does not provide real time, nor does Python.
Thus capacitor voltage is rather imprecise.

\subsection{Results}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{wsn.png}
    \caption{Measured capacitor voltage over a whole simulated lifecycle, running \texttt{demo\_wsn}. X: time (s). Y: capacitor voltage (V). Desired capacitor voltage was 2.5V.}
    \label{fig:graph_wsn}
\end{figure}

Figure~\ref{fig:graph_wsn} shows the evolution of the capacitor voltage over a whole lifecycle.
The target board was flashed with \texttt{demo\_wsn} (with support for multiprocessing).

Before 0 ms, the energy harvesting simulator circuit is in stable mode, the capacitor being formerly short-circuited to force it to 0V.
From 0 to 76 ms, the circuit is in charge mode and at the end the capacitor reaches 2.5V as requested by the user.
From 76 to 126 ms, the circuit is held in stable mode for visual purposes.
The capacitor voltage drops slightly in the meantime.
From 126 to 142 ms, the circuit is held in power mode and the board is powered on. Two slopes are visible in this part, the most abrupt being while sending a radio packet and the other one being user and kernel code running.
From 142 ms till the end of the record, the board is in short-circuit mode and the capacitor completely discharges.

\section{User guide}
\subsection{Expected usage}
There is no hardware protection that prevents the user from powering on several transistors at a time, so it is left to the user to make sure that at most one transistor is on at a time.

\subsection{Minimum working example -- Python 3}
\label{sec:code}
\lstinputlisting[language=python]{mspharvest.py}
\end{document}

