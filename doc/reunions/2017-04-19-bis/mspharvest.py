from math import log
import wiringpi

class HWConfig:
    def __init__(self, Rc, C, E, Voff):
        self.Rc   = Rc
        self.C    = C
        self.E    = E
        self.Voff = Voff

    def voltage_difference(self, I, power_duration):
        return I * power_duration / self.C

    def get_charge_duration(self, vdiff):
        return -self.Rc * self.C * log(1 - vdiff/self.E - self.Voff/self.E)

class RPIConfig:
    def __init__(self, gpio_charge, gpio_power, gpio_short_circuit):
        self.charge        = gpio_charge
        self.power     = gpio_power
        self.short_circuit = gpio_short_circuit

        wiringpi.pinMode(self.charge, 1)        # output
        wiringpi.pinMode(self.power, 1)         # output
        wiringpi.pinMode(self.short_circuit, 1) # output

        wiringpi.digitalWrite(self.charge, 0)
        wiringpi.digitalWrite(self.power, 0)
        wiringpi.digitalWrite(self.short_circuit, 0)

    def capacitor_charge(self, charge_duration):
        wiringpi.digitalWrite(self.charge, 1)
        wiringpi.delay(int(charge_duration * 1e3))
        wiringpi.digitalWrite(self.charge, 0)

    def capacitor_power(self, power_duration):
        wiringpi.digitalWrite(self.power, 1)
        wiringpi.delay(int(power_duration * 2 * 1e3))
        wiringpi.digitalWrite(self.power, 0)

    def capacitor_short_circuit(self):
        wiringpi.digitalWrite(self.short_circuit, 1)
        wiringpi.delay(150)
        wiringpi.digitalWrite(self.short_circuit, 0)
        
wiringpi.wiringPiSetupGpio()

VOFF  = 2
VCRIT = 3.6

hw_config = HWConfig(470, 220e-6, 5, VOFF)
pi_config = RPIConfig(16, 20, 21)
print("System now ready to go")

I                  = 20e-3
power_duration     = 9.7e-3

vdiff           = hw_config.voltage_difference(I, power_duration)
print("Vdiff =", vdiff, "V")
if vdiff > VCRIT - VOFF:
    raise Exception("Supply voltage would be too high for MSP430")

charge_duration = hw_config.get_charge_duration(vdiff)

for i in range(4):
    pi_config.capacitor_short_circuit()
    wiringpi.delay(50)
    pi_config.capacitor_charge(charge_duration)
    wiringpi.delay(50)
    pi_config.capacitor_power(power_duration)
pi_config.capacitor_short_circuit()
