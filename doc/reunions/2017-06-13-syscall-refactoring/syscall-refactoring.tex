\documentclass[a4paper,11pt, article]{memoir}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}


\usepackage[margin=2cm]{geometry}
\setlength{\parindent}{0pt}

\usepackage{url}
\usepackage{sytare-sd}

\begin{document}

{\Large\bfseries On  Syscall Refactoring} \hfill (G. Salagnac, july 6th)
\bigskip

On june 13th Daniel and I had a long conversation about the state of our issue \#5 AKA ``syscall refactoring'' (\url{https://gitlab.inria.fr/citi-lab/sytare/issues/5}).
%
The idea is to move away from the current architecture of sytare (with \texttt{restore}, \texttt{signal} and \texttt{commit} methods) and propose a simpler, more streamlined architecture (with just \texttt{restore} and \texttt{save}).

Also, the proposal makes it possible to optimize certain drivers for execution speed by omitting the syscall wrapper layer (hence the title of the issue).

\bigskip

In this document I reproduce a few scenarios that we discussed over the whiteboard and I identify potential problems in terms of program semantics.

\bigskip
For more details please see issue \#5 on gitlab, and in particular these two summary notes:
\begin{itemize}
\item \url{https://gitlab.inria.fr/citi-lab/sytare/issues/5#note_29445}
\item \url{https://gitlab.inria.fr/citi-lab/sytare/issues/5#note_33178}
\end{itemize}



\subsection*{Sytare 2016 recap}

\begin{itemize}
\item Application must call driver functions through \texttt{syt\_...} syntax only
\item Driver code must call functions in other drivers directly
\item Driver functions must update a RAM object called a \emph{device context}
  \begin{itemize}
  \item each driver X must call \texttt{signal(X)} to notify the OS of a change in device context X.
  \end{itemize}
\item On syscall exit, OS \texttt{commit()}s all dirty device contexts to NVRAM.
  \begin{itemize}
  \item marks them as clean
  \end{itemize}
\item Drivers must export a \texttt{restore(void *from)} method
  \begin{itemize}
  \item responsible for bringing back the hardware in said state
    \begin{itemize}
    \item TODO: clarify what happens with dependents drivers: what does
    it mean  if driver  A depends  on driver B,  but also  restoring A
    would change state of B ?
    \end{itemize}

  \item Note: in the 2016 implementation, the \emph{from} argument is actually passed via a global variable: the OS restores the device context in RAM and then invokes \texttt{restore()}.
  \end{itemize}
\end{itemize}


\subsection*{Proposed refactoring}

\begin{itemize}
\item Application must call driver functions through \texttt{syt\_...} syntax only
\item Driver code must call functions in other drivers directly
\item Drivers must export a \texttt{save(void *where)} method
  \begin{itemize}
  \item \texttt{save()} is responsible for writing a \emph{restore}-able state at location pointed by argument \emph{where}
  \item not required: concepts of \emph{device context} and dirtyness, methods \texttt{signal()} and \texttt{commit()}
  \end{itemize}
\item Drivers must export a \texttt{restore(void *from)} method
  \begin{itemize}
  \item responsible for bringing back the hardware in said state
  \item TODO: clarify what happens  with dependents drivers
  \end{itemize}
\item At certain specific moments, the OS calls \texttt{save()} on all drivers
  \begin{itemize}

  \item if power failures happen only during application code (e.g. scenario 1 below) then
    calling \texttt{save()} at the last moment (i.e. upon receiving an IRQ) will be enough.

  \item but with this approach (which we will refer to as ``optimistic
    mode''), if a  power failure happens during a syscall  then the OS
    cannot save  a consistent  checkpoing image  and must  discard all
    progress made in this lifetime.

  \item  we can  imagine that  the  OS now  switches to  ``pessimistic
    mode'', in which  we \texttt{save()} driver state  on all possible
    occasions (i.e. syscall entry and/or syscall exit).
  \item this strategy should ensure correctness ?
  \item but more scenarios must be considered
    \begin{itemize}
    \item clarifying all of that is the main purpose of this document
    \end{itemize}
  \end{itemize}
\end{itemize}

\subsubsection*{Bonus: inlined drivers}

Some selected drivers (e.g. GPIO) can now be \emph{inlined}. 

\begin{itemize}
\item at the API level the driver follows the same contract:
  \begin{itemize}
  \item application must call driver functions through \texttt{syt\_...} syntax only
  \item driver guarantees atomicity: execution is never resumed in the middle of a call
    \begin{itemize}
    \item Question:  expressed like that, impossible  without changing
      stacks. Even with a very simple action, a power failure may very
      well  happen \emph{during}  the action.  Under which  conditions
      and/or hypotheses is it safe to resume such an action ?
      TODO: clarify contracts (app vs core+drivers, and core vs drivers) to ensure correctness.
    \end{itemize}
  \end{itemize}
\item at the implementation level the driver code is completely inlined in the caller
  \begin{itemize}
  \item the \texttt{syt\_...} wrapper exists but does nothing (and as such, should optimised out by the compiler)
  \item the wrapper really does nothing: no stack change, no entry callbacks (i.e. no save), no exit callbacks (no commit)
  \end{itemize}
\item typically an inlined driver has no device context in RAM
  \begin{itemize}
  \item no \texttt{signal()/commit()} mechanism, only a \texttt{save()} method
  \item the \texttt{save()} method reads the hardware directly (e.g. MMIO) 
  \end{itemize}
\end{itemize}

\bigskip

Rationale: the goal is to reduce execution time overhead for simple drivers like GPIO. 
%
Bare-metal applications access hardware registers all the time, and for simpler devices the don't use an explicit driver.
%
Sytare 2016 requires an explicit API to switch a GPIO, which adds a significant overhead to execution time and code complexity. 
%
The syscall layer between application code and this API makes execution time even worse.
%
This proposal aims at providing the same level of persistence but without the overhead.  

\section*{Scenarios discussed on 2017-06-13}


\subsection*{Scenario 1}

This scenario illustrates the classical ``peripheral state volatility problem'' with two dependent drivers: application calls driver A which in turn calls driver B.
%
Both drivers modify their state, which we denote ``rw'' (for read/write) in the diagram below.

\begin{center}
 \begin{sequencediagram}
 \lifeline{app}{App}
 \lifeline{os}{OS}
 \lifeline{da}{Drv A}
 \lifeline{db}{Drv B}
 \activate{app}
 \advancetime{20}
 \deactivate{app}  
 \arrow{app}{os}  
 {
   \activate{os}
   \advancetime{5}
   {
     \advancetime{5}
     \deactivate{os}  
     \arrow{os}{da}   
     {
       \activate{da}
       \advancetime{10}
       \noteright{da}{rw}
       \advancetime{10}
       \deactivate{da}   
       \arrow{da}{db}   
       {
         \activate{db}
         \advancetime{10}
         \noteright{db}{rw}
         \advancetime{10}
         \deactivate{db}   
         \arrow{db}{da}   
       }
       \activate{da}
       \marker{db}
       
       \advancetime{10}
       \noteright{da}{rw}
       \advancetime{10}
       \deactivate{da}   
       \arrow{da}{os}
     }
     \activate{os}
     \marker{da} 
   }
   \advancetime{5}
   \noteright{os}{\textcircled{\tiny 1}}
   \advancetime{5}
   \deactivate{os}   
   \arrow{os}{app}  
 }
\activate{app}
 \advancetime{20}
 \deactivate{app}
 \bigcross{app}
 \horizontalline{app}{db}
 \end{sequencediagram}
\end{center}

 In this scenario,  both Driver A and Driver B  change their state, so
 when  the crash  happens, what  we want  saved is  the
 driver state at both bullets.

 \begin{itemize}
 \item Sytare 2016: A and B are marked dirty via \texttt{signal()}, so their device context is \texttt{commit()}ed on syscall exit \textcircled{\tiny 1}.
 \item Proposed refactoring: A and B are \texttt{save()}d at the last moment 
   \begin{itemize}
   \item Bonus: works for all combinations of inlineness of A and B
   \end{itemize}

 \end{itemize}


\subsection*{Scenarios 2, 3}

Similar to  Scenario 1,  but here we  have enough time  to begin  a second
hardware action. The crash happens somewhere in the middle of this
second call.

 \begin{center}
   \begin{sequencediagram}
     \lifeline{app}{App}
     \lifeline{os}{OS}
     \lifeline{da}{Drv A}
     \lifeline{db}{Drv B}
     \activate{app}
     \advancetime{20}
     \deactivate{app}  \arrow{app}{os}  \activate{os}
     \advancetime{5}
     {
       \advancetime{5}
       \deactivate{os}   \arrow{os}{da}   \activate{da}
       \advancetime{10}
       \noteright{da}{rw}
       \advancetime{10}
       {
         \deactivate{da}   \arrow{da}{db}   \activate{db}
         \advancetime{10}
         \noteright{db}{rw}
         \advancetime{10} 

         \noteright{db}{\textcircled{\tiny 1}}
         
         \deactivate{db}   \arrow{db}{da}   \activate{da}
       }
       \advancetime{10}
       \noteright{da}{rw}
       \advancetime{10}
       \noteright{da}{\textcircled{\tiny 2}}

       \deactivate{da}   \arrow{da}{os}   \activate{os}
     }
     \advancetime{5}
     \noteright{os}{\textcircled{\tiny 3}}
     \advancetime{5}
     \deactivate{os}   \arrow{os}{app}  \activate{app}
     \advancetime{20}
     % first call ends here
     \deactivate{app}  \arrow{app}{os}  
     \noteright{os}{\textcircled{\tiny 4}}
     \activate{os}
     \advancetime{5}
     {
       \advancetime{5}
       \deactivate{os}   \arrow{os}{da}   \activate{da} 
       \advancetime{10}
       \noteright{da}{rw}
       \advancetime{10}
       {
         \noteleft{da}{\textcircled{\tiny 5}}
         \deactivate{da}   \arrow{da}{db}   \activate{db}
         \advancetime{10}
         \noteright{db}{rw}
         \advancetime{10}
         \deactivate{db}
       }}
     \bigcross{db}
     \horizontalline{app}{db}
     \noteright[3em]{db}{\textcircled{\tiny 6}}
   \end{sequencediagram}
 \end{center}

 On next boot, we want the syscall retried from point \textcircled{\tiny 4}, so we have to save driver state as of \textcircled{\tiny 1}~and~\textcircled{\tiny 2}.
%
 But because of the  state changes around \textcircled{\tiny 5}, saving state at the last moment \textcircled{\tiny 6} would be incorrect.

\bigskip

Conclusion: in this scenario, saving driver state must be done either on syscall exit \textcircled{\tiny 3} or on the next syscall entry \textcircled{\tiny 4}.

\bigskip

\begin{itemize}
\item Sytare 2016: Driver B calls \texttt{signal()} at \textcircled{\tiny 1} and Driver A calls  \texttt{signal()} at \textcircled{\tiny 2}, so the OS knows that it must \texttt{commit()} A and B at \textcircled{\tiny 3}.
\item Proposed refactoring: 
  \begin{itemize}
  \item Optimistic mode: the OS is only allowed to call \texttt{save()} at the last moment, i.e. \textcircled{\tiny 6}.
    \begin{itemize}
    \item in this scenario, this would lead to incorrect behaviour. Instead, the OS should discard the entire lifecycle and start again in pessimistic mode. 
    \item Question: but how is the OS instructed to do so ? TODO: make the heuristic explicit.
    \end{itemize}
  \item Pessimistic mode: the OS calls \texttt{save()} (for all drivers) on each syscall exit, i.e. \textcircled{\tiny 3}.
    \begin{itemize}
    \item Note about performance: if the \texttt{save()} operation is costly, then a driver can implement its own private \emph{dirty} flag. 
%
In terms of execution time, letting each driver check their flag should be just as efficient as checking all dirty flags in the OS like is done in Sytare 2016.
    \item alternative: we could call \texttt{save()} on each syscall entry entry instead, e.g. \textcircled{\tiny 4}, and it would work just as well, because the device context is stable outside of driver calls (hypothesis).
    \end{itemize}
  \end{itemize}
\end{itemize}

\bigskip
Hypothesis: we always assumed that the state of a device (resp. the driver state AKA device context) only ever changes as a result of a driver function being executed, either because it is invoked through a syscall or because it is called from another driver. 
%
However this is incorrect if a driver has interrupts handlers, as we discussed at our july 5th meeting. (but this is another story)

\subsection*{Scenario 4}

Here driver B is \emph{inlined}.

\begin{center}
\begin{sequencediagram}
  \lifeline{app}{App}
  \lifeline{os}{OS}
  \lifeline{da}{Drv A}
  \lifeline{db}{Drv B}
  \activate{app}
  \advancetime{20}
  \deactivate{app}
  \arrow{app}{os}
  {
    \activate{os}
    \advancetime{10}
    \deactivate{os}
    \arrow{os}{da}
    {
      \activate{da}
      \advancetime{10}
      
      \foreach \i in {1,2,3}
      {
        \deactivate{da}
        \arrow{da}{db}
        \activate{db}
        \advancetime{2}
        \noteright{db}{\scriptsize rw}
        \advancetime{5}
        \deactivate{db}
        \arrow{db}{da}
        \activate{da}
        \advancetime{10} 
      }
      \deactivate{da}
      \arrow{da}{os}
    }
    \activate{os}
    \advancetime{10}
    \deactivate{os}
    \noteright{os}{\textcircled{\tiny 1}}
    \arrow{os}{app}
  }
  \activate{app}
  \advancetime{30}
  \deactivate{app}
  \bigcross{app}
  \horizontalline{app}{db}
\end{sequencediagram}
\end{center}


\begin{itemize}
\item This is like scenario 1:  the crash happens outside any driver function, so calling \texttt{save()} at the last moment is perfectly fine.
\item Sytare 2016: driver B would have to call \texttt{signal()} so that the OS does a \texttt{commit()} on syscall exit \textcircled{\tiny 1} (Remember that in Sytare 2016 there is no such thing as an inlined driver)
\end{itemize}


\subsection*{Scenario 5}


Again, driver B is inlined. In this scenario, B is first called from the application, and then from a syscall. The power failure happens during this syscall.

\begin{center}
\begin{sequencediagram}
  \lifeline{app}{App}
  \lifeline{os}{OS}
  \lifeline{da}{Drv A}
  \lifeline{db}{Drv B}
  \activate{app}
  \advancetime{20}
  \deactivate{app}
  \arrow{app}{db}
  {
    \activate{db}
    \advancetime{2}
    \noteright{db}{\scriptsize rw}
    \advancetime{5}
    \deactivate{db}
    \arrow{db}{app}
  }
  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \arrow{app}{os}
  {
    \activate{os}
    \advancetime{5}
    \noteright{os}{\textcircled{\tiny 1}}
    \advancetime{5}
    \deactivate{os}
    \arrow{os}{da}
    {
      \activate{da}
      \advancetime{10}
      
      \deactivate{da}
      \arrow{da}{db}
      \activate{db}
      \advancetime{2}
      \noteright{db}{\scriptsize rw}
      \advancetime{5}
      \deactivate{db}
      \arrow{db}{da}
    }
    \activate{da}
    \advancetime{10}
    \deactivate{da}
    \bigcross{da}
    \horizontalline{app}{db}
    }
\end{sequencediagram}
\end{center}

\begin{itemize}
\item Optimistic mode: nothing gets \texttt{save}d ; the OS will discard the entire lifecycle and start again in pessimistic mode. 
\item Pessimistic mode: all drivers get \texttt{save}d on syscall entry i.e. \textcircled{\tiny 1}. When the IRQ arrives the OS can still save application state (unchanged since \textcircled{\tiny 1}) and obtain a complete checkpoint image. 
\end{itemize}



\end{document}
