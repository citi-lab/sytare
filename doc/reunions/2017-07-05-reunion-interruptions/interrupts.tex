\documentclass[a4paper,11pt, article]{memoir}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fonts

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[final]{microtype}
\usepackage{lmodern}                      % vector version of default fonts
\usepackage{textcomp}                     % for textbullet



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Layout

\usepackage[margin=2cm]{geometry}

\usepackage{enumitem}
\setlist{nosep}

\setlength{\parindent}{0pt}

\setsecnumdepth{subsubsection}                   % default: section
\settocdepth{subsubsection}                      % default: section
\renewcommand*{\thesection}{\arabic{section}}
\renewcommand*{\thesubsection}{\thesection.\arabic{subsection}}

\setsecheadstyle{\Large\bfseries}                % default: \Large\bfseries
\setbeforesecskip{2ex plus 1ex minus 1ex}        % default: 3.5+1-.2 ex
\setaftersecskip{2ex plus 1ex minus 1ex}         % default: 2.3+.2
\setsubsecheadstyle{\large\bfseries}             % default: \large\bfseries
\setbeforesubsecskip{2ex plus 1ex minus 1ex}     % default: 3.25+1-.2 ex
\setaftersubsecskip{1ex plus .5ex minus .5ex}    % default: 1.5+.2

\setsubparaheadstyle{\itshape}          % default: \bfseries
\setsubparaindent{0pt}                           % default: \parindent

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Utilities and misc

\usepackage{url}
\usepackage{sytare-sd}

\usepackage{listings}
\makeatletter
\lstset{
  language={},
  frame=none,
  basicstyle=\lst@ifdisplaystyle\footnotesize\fi\ttfamily,
  columns=fullflexible,
  keepspaces=true,
}
\makeatother

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% Begin document

\begin{document}

{\Large\bfseries On the semantics of interrupts in Sytare} \hfill (G. Salagnac, july 7th)
\bigskip

A few days ago we had a Sytare meeting about exposing interrupts to application code, for instance through deferred-execution ``user event handlers'' and/or other similar features.
%
In the course of the discussion we realized that even in the current Sytare model (i.e. IoENT, ComPAS, research report) we have no clear idea about the status of interrupts.

This document summarizes our exchanges.
%
In a few places I remark that I'm not convinced that what we're doing is correct.

\section{Sytare 2016 vanilla}

The Sytare 2016 vanilla implementation does not support interrupts, neither in application code nor in drivers. 
%
In other words, writing an interrupt service routine is forbidden.
%
The main reason for that is that we make the hypothesis that \emph{the state of a peripheral device never changes outside of an explicit driver function.}

\bigskip
In practice, the kernel does use interrupts from the voltage comparator module to detect imminent power failures. 
%
However this does not pose any problems of semantics because once a failure is detected, execution jumps in the the kernel and never goes back to user code (and we rely on correct semantics in the checkpointing and syscall retrying mechanisms).

\bigskip

Also, interrupts are disabled during syscall entry and syscall exit, so that stack switching and argument backup are not disturbed.


\section{Sytare 2016 + Interrupts}

In this section we discuss what it would mean to add interrupt support to Sytare. 
%
For lack of better names the ``Sytare 2016 vanilla'' architecture will be denoted S/V and the one with interrupts S+I

\paragraph{Interrupt dispatch} When an interrupt request (IRQ) arrives at the CPU, the hardware automatically saves a few registers of context on the stack and then jumps to the associated interrupt service routine (ISR). 

Unlike in a bare-metal application where each device driver implements their own interrupt routines, in S+I all interrupt vectors point to ISRs implemented in the kernel.
%
Each such ``kernel ISR'' has to determine the actual cause of the interrupt and then dispatch the call to the right function in the device driver (i.e. to a ``driver ISR'').

\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{os}{OS}
    \lifeline{drv}{Driver}
    \activate{app}
    \advancetime{15}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow[\scriptsize func()]{os}{drv}
      {
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow[\scriptsize \texttt{RET}]{drv}{os}
      }
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow[\scriptsize \texttt{RETI}]{os}{app}
    }
    \activate{app}
    \advancetime{10}
    \deactivate{app}
  \end{sequencediagram}
\end{center}


The idea is to allow several distinct device drivers to be associated with a single interrupt vector.
%
Still the constraints imposed on a ``driver ISR'' are similar to that of an ordinary ISR (no long-running computation, no arguments, etc.)



\paragraph{Volatility} In S+I, interrupts cannot be nested, i.e. all ISRs run with interrupts disabled.
%
This means that even a ``power failure detection'' event cannot interrupt the execution of an ISR.
%
Instead, the ``driver ISR'' runs to completion, then returns (\texttt{RET}) into the ``kernel ISR'' which itself returns (\texttt{RETI}) into interrupted code (either application code like illustrated in the diagram above, or driver code in case we were in a syscall).

\medskip

As a result, a power failure ISR would be invoked in the same circumstances in S+I that it is in S/V: either during a syscall, or during application execution.


\bigskip

Remark: Are there problems in all that ? It's not obvious to me so I'm still very much not convinced that everything would work smoothly.


\subsection{Atomicity} Because interrupts cannot be nested, each ISR is executed atomically: either entirely or not at all.
%
In case a power failure happens \emph{during} an ISR, no state will be saved so the lifetime will be discarded.

However if a power failure is detected outside of an ISR, then several cases must be discussed.

% because for some reason (lmodern.sty ?) circled numbers look bad today
\newcommand{\circled}[1]{\textcircled{\raisebox{.1em}{\resizebox{.35em}{!}{#1}}}}


\paragraph{Scenario 1} at \circled{1} all device contexts are supposed to be ``clean''.


\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{os}{OS}
    \lifeline{drv}{Driver A}
    \lifeline{drvb}{Driver B}
    \activate{app}
    \advancetime{15}
    \horizontalline{app}{drvb}
    \noteright{drvb}{~~~~~\circled{1}}
    \advancetime{15}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{drv}
      {
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{drvb}
        {
          \activate{drvb}
          \advancetime{5}
          \noteleft{drvb}{\circled{2}}
          \noteright{drvb}{rw}
          \advancetime{5}
          \deactivate{drvb}
          \arrow{drvb}{drv}
        }
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{os}
      }
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{app}
    }
    \activate{app}
    \advancetime{20}
    \event[\scriptsize failure detected~~~~~~~~]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \noteright{os}{\circled{3}}
      \advancetime{5}
      \deactivate{os}
      \bigcross{os}
    }
    \horizontalline{app}{drvb}

  \end{sequencediagram}
\end{center}

Because driver B calls \texttt{signal()} at \circled{2}, the OS knows that it must save B's state at \circled{3}.



\paragraph{Scenario 2} at \circled{1} all device contexts are supposed to be ``clean''.


\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{os}{OS}
    \lifeline{drv}{Driver A}
    \lifeline{drvb}{Driver B}
    \activate{app}
    \advancetime{15}
    \horizontalline{app}{drvb}
    \noteright{drvb}{~~~~~\circled{1}}
    \advancetime{15}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{drv}
      {
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{drvb}
        {
          \activate{drvb}
          \advancetime{5}
          \noteleft{drvb}{\circled{2}}
          \noteright{drvb}{rw}
          \advancetime{5}
          \deactivate{drvb}
          \arrow{drvb}{drv}
        }
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{os}
      }
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{app}
    }
    \activate{app}
    \advancetime{10}
    \deactivate{app}
    \arrow{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \arrow{os}{drv}
      \deactivate{os}
      {
        \activate{drv}
        \advancetime{5}
        \noteright{drv}{rw}
        \noteleft{drv}{\circled{3}}
        \advancetime{10}
        \arrow{drv}{os}
        \deactivate{drv}
      }
      \activate{os}
      \advancetime{5}
      \noteright{os}{\circled{4}}
      \arrow{os}{app}
      \deactivate{os}
    }
    \activate{app}
    \advancetime{20}

    \event[\scriptsize failure detected~~~~~~~~]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \noteright{os}{\circled{5}}
      \advancetime{5}
      \deactivate{os}
      \bigcross{os}
    }
    \horizontalline{app}{drvb}

  \end{sequencediagram}
\end{center}

In this scenario, Driver B is signaled dirty at \circled{2} and Driver A is signaled dirty at \circled{3}.
%
On syscall exit \circled{4} the OS saves both device contexts to non-volatile memory, so when the failure happens at \circled{5} only application state needs to be saved and no progress will be lost.


S+I design proposal: 
\begin{itemize}
\item on checkpoint  creation, if we were  executing application code, then commit all dirty device contexts to NVRAM (in addition to saving application state)
\end{itemize}


\needspace{20em}
\paragraph{Scenario 3.} This is a variant of the previous diagram. Here the power failure happens during the syscall.

\begin{center}
  \begin{sequencediagram}
    \lifeline{app}{App}
    \lifeline{os}{OS}
    \lifeline{drv}{Driver A}
    \lifeline{drvb}{Driver B}
    \activate{app}
    \advancetime{15}
    \horizontalline{app}{drvb}
    \noteright{drvb}{~~~~~\circled{1}}
    \advancetime{15}
    \event[\scriptsize IRQ]{app}
    \deactivate{app}
    \arrow[\scriptsize ISR]{app}{os}
    {
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{drv}
      {
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{drvb}
        {
          \activate{drvb}
          \advancetime{5}
          \noteleft{drvb}{\circled{2}}
          \noteright{drvb}{rw}
          \advancetime{5}
          \deactivate{drvb}
          \arrow{drvb}{drv}
        }
        \activate{drv}
        \advancetime{10}
        \deactivate{drv}
        \arrow{drv}{os}
      }
      \activate{os}
      \advancetime{5}
      \deactivate{os}
      \arrow{os}{app}
    }
    \activate{app}
    \advancetime{10}
    \noteleft{app}{\circled{3}}
    \deactivate{app}
    \arrow{app}{os}
    {
      \activate{os}
      \noteright{os}{\circled{4}}
      \advancetime{5}
      \arrow{os}{drv}
      \deactivate{os}
      {
        \activate{drv}
        \advancetime{7}
        \noteright{drv}{rw}
        \noteleft{drv}{\circled{5}}
        \advancetime{7}
        \horizontalline{os}{drv}
        \deactivate{drv}
      }}
    \event[\scriptsize failure detected~~~~~~~~]{os}
    \activate{os}
    \advancetime{7}
    \noteright{os}{\circled{6}}
    \advancetime{5}
    \deactivate{os}
    \bigcross{os}
    \horizontalline{app}{drvb}
  \end{sequencediagram}
\end{center}

In this scenario, we want the syscall retried at next boot, starting from wrapper entry \circled{4}.
%
For this, the state change at \circled{2} must have been saved (e.g. at \circled{6}) but not the state change at \circled{5}.

S+I design proposal: 
\begin{itemize}
\item on syscall entry  \circled{4}, commit all dirty device contexts to NVRAM (in addition to backuping syscall arguments and id)
\item on checkpoint creation \circled{6}, if we were executing driver code (i.e. if we were in a syscall) then only save application state (which should be intact since \circled{3}).
\end{itemize}

\bigskip

Remark: all of that depends on application state (i.e. globals+stack) to remain untouched during ISRs and syscalls.
%
This is fine as long as everyone communicates via simple function arguments and return values, but everything breaks if driver code touches application state (e.g. writes in a shared buffer).
%
We should probably think about utility functions like \lstinline{copy_to_user} and \lstinline{copy_from_user} to help us understand these cases better.

\bigskip

Remark 2: Are there problems in all that ? It's not obvious to me so I'm still very much not convinced that everything would work smoothly.




\section{Race conditions}
\label{sec:race-conditions}

(This is not specific to sytare but applies more generally to bare-metal microcontroller programming)

Interrupt service routines are executed concurrently with the rest of the system, thus may cause synchronization problems.
%
This section illustrates such a problem and solution.

\paragraph{Hardware}

The MSP430 serial port has a one-byte buffer for received data (\texttt{UCA0RXBUF} cf e.g. slau049 §13.1)
%
Each time a new character is received, an interrupt is issued so that software can read the buffer and make room for the next character:

\begin{lstlisting}[language=C,frame=l]
volatile char received;
INTERRUPT uart_isr()
{
   received = UCA0RXBUF;
}
\end{lstlisting}

\paragraph{Software} On the other hand, application logic typically has a completely different control structure and  wants to read several characters at once:

\begin{lstlisting}[language=C,frame=l]
main()
{
   char line[50];
   ...
   while(1)
   {
       uart_gets(&line, 50);
       ... // do stuff with `line'
   }
}
\end{lstlisting}

\paragraph{Ring buffer} The ISR and application code form a classical producer/consumer architecture.
%
They can communicate through a shared circular buffer:

\setlength{\overfullrule}{1em}

\begin{minipage}{.49\linewidth}
\begin{lstlisting}[language=C,frame=l]
char buffer[N];
int  wptr=0; // write pointer (used by producer)
int  rptr=0; // read pointer  (used by consumer)

INTERRUPT uart_isr()
{
   // omitted: abort if buffer full
   // else:
   wptr = (wptr+1) % N;
   buffer [ wptr ] = UCA0RXBUF;
}
\end{lstlisting}
\end{minipage}
%
\hfill
%
\begin{minipage}{.45\linewidth}
\begin{lstlisting}[language=C,frame=l]
int uart_getc(char *data)
{
   // omitted: abort if buffer empty
   // else:
   rptr = (rptr +1) % N;
   *data = buffer [ rptr ] ;
}

// implementing gets on top of getc is 
// left as an exercise to the reader
\end{lstlisting}
\end{minipage}

\paragraph{Race condition}

Just like in the classical producer/consumer scenario, concurrent execution of these two functions may put the circular buffer in an inconsistent state.
%
The solution is to ensure mutual exclusion between the two critical sections, for instance by adding a mutex lock. 
%
However we cannot grab a lock in an interrupt handler. 
%
Instead, we should disable interrupts completely in \lstinline{uart_getc}.

\begin{center}
\fbox{
  \begin{minipage}{.8\linewidth}
    \setlength{\leftmargini}{0pt}
\begin{quote}\sffamily
  First  rule  of  writing   correct  interrupt-driven  code: 
  \begin{itemize}
  \item  Disable   interrupts at all times when interrupt cannot be handled properly
  \end{itemize}
  When can an interrupt not be handled properly?
  \begin{itemize}
  \item When manipulating data that the interrupt handler touches
  \end{itemize}
\end{quote}
\flushright
John Regher, Advanced Embedded Software lecture 10 page 5.\\
\url{http://www.eng.utah.edu/~cs5785/slides/10.pdf#page=5}
\end{minipage}
}
\end{center}

\paragraph{Implementation in the CITI ez430 BSP}

This synchronization pattern is present in the CITI ez430 BSP by Antoine Fraboulet and others.
%
Below are a few relevant excerpts.

\subparagraph{Interrupt handler}


In the ez430 BSP, the serial port driver does not implement the ring buffer.
%
Instead it invokes a user-specified callback function:

\begin{lstlisting}[language=C,frame=l]
interrupt (USCIAB0RX_VECTOR) usart0isr(void)
{
    volatile unsigned char dummy;
    /* Check status register for receive errors. */
    if (UCA0STAT & UCRXERR) {
        /* Clear error flags by forcing a dummy read. */
        dummy = UCA0RXBUF;
        dummy += 1; /* warning gcc otherwise! */
    } else {
        if (uart_cb(UCA0RXBUF) != 0) {
            LPM_OFF_ON_EXIT;
        }
    }
}
\end{lstlisting}

\subparagraph{Circular buffer} The callback function is implemented in application code:

\begin{lstlisting}[language=C,frame=l]

#define SERIAL_RX_FIFO_SIZE 8

volatile uint8_t serial_rx_buffer[SERIAL_RX_FIFO_SIZE];
volatile uint8_t serial_rx_rptr=0;
volatile uint8_t serial_rx_wptr=0;
volatile uint8_t serial_rx_size=0;

void serial_ring_put(uint8_t data)
{
    serial_rx_buffer[serial_rx_wptr] = data;
    serial_rx_wptr = (serial_rx_wptr + 1) % SERIAL_RX_FIFO_SIZE;
    if (serial_rx_size < SERIAL_RX_FIFO_SIZE)
    {
        serial_rx_size ++;
    }
    else
    {
        /* 
         * if (serial_rx_size == SERIAL_RX_FIFO_SIZE) 
         * we get a rx_overflow 
        */
    }
}
\end{lstlisting}



\subparagraph{Race condition avoidance} The synchronization problem between producer and consumer is indeed prevented by disabling interrupts during the critical section of the consumer:

\begin{lstlisting}[language=C,frame=l]
int serial_ring_get(uint8_t *data)
{
  int ret = 0;
  _disable_interrupts();
  if (serial_rx_size > 0)
  {
      *data = serial_rx_buffer[serial_rx_rptr];
      serial_rx_rptr = (serial_rx_rptr + 1) % SERIAL_RX_FIFO_SIZE;
      serial_rx_size --;
      ret = 1;
  }
  _enable_interrupts(); 
  return ret;
}
\end{lstlisting}


\end{document}
