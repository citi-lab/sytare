# script R pour generer les element de base des graphiques du rapport de recherche
# 14-sept-2016 @TDk

# to run : 
# se placer dans le dossier du script
# lancer R avec la commande éponyme "R"
# installer ggplot: install.packages("ggplot2")
# execution du script : source(rscript.r)
# 
# note : les  packages ne sont à installer qu'une fois.


# open installed library
library(ggplot2)

# colors palette with 5 values
syt_colors_0=c(	"#cc5500", 
				"#ffcccc", 
				"#cc5500", 
				"#ffcccc",
				"#cc5500")

# colors palette with 4 values
syt_colors_1=c(	"#ffcccc", 
				"#cc5500", 
				"#ffcccc",
				"#cc5500") 

# color palette with 3 value
syt_colors_2=c(	"#cc5500", 
				"#ffcccc", 
				"#cc5500")


# jeu donnée : temps de boot général
boot_sys = c(1240, 45, 27, 1170, 30)
dataframe_boot_sys = as.data.frame(boot_sys)

# temps boot drv (rf clk adc spi port)
boot_drv = c(11, 37, 103, 80, 940)
dataframe_boot_drv = as.data.frame(boot_drv)



# syscalls : 
syt_led_toggle = c(3.4,1.6,17.8)
syt_send_packet = c(2.8,3400, 20.6)
syt_radio_sleep = c(2.4, 23, 29.2)
syt_radio_wakeup = c(2.4, 428, 31.4)
syt_sense_temp = c(2.4, 75.2, 17.6)

dataframe_1 = as.data.frame(syt_led_toggle)
dataframe_2 = as.data.frame(syt_send_packet)
dataframe_3 = as.data.frame(syt_radio_sleep)
dataframe_4 = as.data.frame(syt_radio_wakeup)
dataframe_5 = as.data.frame(syt_sense_temp)






# set de l'export png/pdf/...
png("pie_boot_sys.png")
# diagrame tartesque
pie(boot_sys, labels='', col=syt_colors_1)
dev.off()

# diagrame stack sur une barre, (je comprends pas tout à fait mais bon)
ggplot(dataframe_boot_sys, aes(1, boot_sys, fill=c("fuck0","fuck1","fuck2","fuck3", "fuck4"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_0) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("bar_boot_sys.png", width=7, height=1)




# set de l'export png/pdf/...
png("pie_boot_drv.png")
# diagrame tartesque
pie(boot_drv, labels='', col=syt_colors_0)
dev.off()


ggplot(dataframe_boot_drv, aes(1, boot_drv, fill=c("fuck0","fuck1","fuck2","fuck3","fuck4"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_0) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("bar_boot_drv.png", width=7, height=1)






ggplot(dataframe_1, aes(1, syt_led_toggle, fill=c(	"fuck0","fuck1","fuck2"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_2) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("syt_led_toggle.png", width=7, height=1)

ggplot(dataframe_2, aes(1, syt_send_packet, fill=c(	"fuck0","fuck1","fuck2"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_2) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("syt_send_packet.png", width=7, height=1)

ggplot(dataframe_3, aes(1, syt_radio_sleep, fill=c(	"fuck0","fuck1","fuck2"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_2) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("syt_radio_sleep.png", width=7, height=1)

ggplot(dataframe_4, aes(1, syt_radio_wakeup, fill=c(	"fuck0","fuck1","fuck2"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_2) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("syt_radio_wakeup.png", width=7, height=1)

ggplot(dataframe_5, aes(1, syt_sense_temp, fill=c(	"fuck0","fuck1","fuck2"))) + 
	theme_bw() +
	geom_bar(stat = "identity") + 
	scale_fill_manual(values=syt_colors_2) +
	coord_flip() +
	theme(panel.border = element_blank(), panel.grid.major = element_blank(),legend.position="none",
	panel.grid.minor = element_blank(), axis.line = element_line(colour = "black"), 
	axis.title.x=element_blank(), axis.text.x=element_blank(), axis.ticks.x=element_blank(),
	axis.title.y=element_blank(), axis.text.y=element_blank(), axis.ticks.y=element_blank())
ggsave("syt_sense_temp.png", width=7, height=1)

