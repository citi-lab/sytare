\documentclass[conference,9pt,a4paper]{IEEEtran}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{microtype}

\usepackage{graphicx}
\usepackage{tikz}  
\usetikzlibrary{calc, backgrounds, arrows, positioning, patterns, fit}
\graphicspath{{figs/}{logos/}{imgs/}}

\usepackage[colorlinks,linkcolor=blue,citecolor=blue,urlcolor=blue]{hyperref}

\usepackage{listings}

\makeatletter
\lstset{
  language={},
  frame=none,
  basicstyle=\lst@ifdisplaystyle\footnotesize\fi\ttfamily,
  columns=fullflexible,
  keepspaces=true,
}
\makeatother

\usepackage[
backend=bibtex,
maxnames=4,
minnames=3,
maxbibnames=10,
bibencoding=inputenc,
style=numeric-comp,
sorting=anyt, % Sort by alphabetic label, name, year, title
]{biblatex}
\bibliography{nvmw.bib}

\newcommand{\todo}[1]{\textbf{\color{blue}TODO: #1}}

\begin{document}
\title{Peripheral State Persistence and Interrupt Management For Transiently Powered Systems}

\renewcommand\IEEEauthorrefmark[1]{\raisebox{0pt}[0pt][0pt]{\textsuperscript{\footnotesize #1}}}

\author{%
  \IEEEauthorblockN{
    Gautier Berthou\IEEEauthorrefmark{1},
    Tristan Delizy\IEEEauthorrefmark{1},
    Kevin Marquet\IEEEauthorrefmark{1},
    Tanguy Risset\IEEEauthorrefmark{1},
    Guillaume Salagnac\IEEEauthorrefmark{1}
  }
  \IEEEauthorblockA{
    \IEEEauthorrefmark{1}Univ. Lyon, INSA-Lyon, Inria -- Villeurbanne France
    \qquad
  }
}

\maketitle
\begin{abstract}
Recently has emerged the concept of {\em transiently powered systems} powered by harvesting power and being able to retain information between power failures using non-volatile RAM. While existing solutions focus on purely computing systems, this article presents {\em Sytare}, a software layer designed to allow the use of non-trivial peripherals such as timers, serial interface or radio devices in transiently powered systems.
\end{abstract}


\begin{keywords}
  NVRAM, internet of things, energy harvesting, sensors, ultra-low
  power, micro-architecture, compilers, instant on-off and transient
  computing systems
\end{keywords}

\section{Introduction and problem statement}

Using a battery in tiny embedded systems can be undesirable or even impractical~\cite{JLLx14}. In such cases, the system must harvest energy from its environment but it must then cope with an unreliable power supply. One obvious nuisance of \textit{transient} power is that the system will lose every volatile state at each power failure. Recent advances in non-volatile memories allow to envision tiny systems that do not lose their data in case of power outage. However, naively replacing RAM with NVRAM has undesirable side-effects. To remedy this problem, recent works propose to detect when a power failure is about to happen, and then save processor state to NVRAM before halting execution. This mechanism is called checkpointing, and data structures that contain such data are called checkpoints. However, these studies tend to focus on the computational angle and ignore peripheral accesses.

The problem we address is to make hardware peripherals \emph{persistent} across reboots so that the application does not notice power failures.

% This problem holds two generic aspects, \emph{state volatility} and \emph{access atomicity}.

The first issue is to cope with the volatility of peripheral state. Capturing and restoring the internal state of peripherals require more complex techniques than doing so for application state.
%
Existing works on transiently powered systems either ignore peripherals completely, or use hard-coded workarounds~\cite{LR15} to configure the hardware before restoring application state.
%
We propose a technique to address this problem in the general case. Our approach is completely transparent for the application, and requires little modification of driver code.

The second issue is to cope with power failures occurring in the middle of a hardware request being serviced.
%
Even if the state of a peripheral is non-volatile (either using non-volatile memory, or some software technique) a power failure may not be transparent for the user program.
%
For instance, consider an application sending a radio packet using a \lstinline{send_message()} function call.
%
Now a power failure happens, in the middle of the transmission.
%
At next boot, it would not make sense to ``send half of the packet''.
%
Not only because the receiver may be gone, but also because the hardware has no concept of ``half packet''.
%
This kind of problem arises even with simple peripherals such as an ADC (\textit{Analog-to-digital converter}).
%
If the power failure is to get unnoticed by application code, then the whole hardware access must be \emph{retried}.
%
We will refer to this issue as the \emph{peripheral access atomicity problem}.
%
The peripheral access atomicity problem has also been referred to as the {\em Broken Time Machine} problem~\cite{RL14}.
%

The third issue is to deal with interrupts. When an interrupt signals that an event has occurred, the software stack must handle it, since interrupt handling may require to modify data and peripherals state shared with the (interrupted) application. Thus, interrupts must be handled carefully or they can lead the application to consistency issues.

Existing works do not address these three problems in a satisfactory fashion.

\section{The Sytare kernel}

In this Section, we first consider that the execution flow can only be interrupted by powerloss detection interrupt, then we describe how we deal with other interrupts, finally the complete execution flow of Sytare is described.

\subsection{Sequential execution}

Our approach to provide peripheral state persistence revolves around the interface between \emph{application code} and \emph{driver code}. We interpose a so-called \emph{kernel} layer between the two, so as to intercept requests and responses. The Sytare \emph{kernel} is responsible for persistence management, which includes saving and restoring application state to and from non-volatile memory. \emph{Driver code} contains all functions which provide access to hardware features. In Sytare, if an application needs to invoke a driver function, it can only do so via \emph{system call} (or \emph{syscall}) mechanism implemented in the kernel. We used this denomination by analogy with the homonymous concept in classical kernels. In practice, a syscall is a thin wrapper around a driver function, adding the necessary features to address the atomicity problem.

%
The Sytare kernel ensures that, if the application is interrupted by a power failure, its state is saved and then at next boot it will restored. In the same manner, the Sytare kernel ensures that, if a syscall is interrupted by a power failure, then at next boot it will be re-invoked in the same conditions (arguments, hardware state, etc.). This piece of information is stored in non-volatile memory in a data structure we refer to as a \emph{device context}.
%
The device contexts are saved to persistent memory not upon power failures, but upon entering/exiting syscalls.
%
Also, system calls are executed in a volatile fashion, i.e. nothing a syscall does is made persistent until execution returns to the application.
%
For instance, we forbid application code to directly use memory-mapped registers to communicate with a hardware device.
%
Instead, we require this service to be encapsulated in a driver function and invoked explicitly from the application.
%
A driver may call primitives from other drivers, for instance our radio chip driver is built on top of the SPI driver, which itself requires digital I/O.
%
%

Restoring the state of a hardware device typically requires non-trivial operations like configuring some I/O pins, communicating over a serial bus (which itself should be initialized first), respecting certain timing constraints etc.
%
While it may be conceivable for a persistence kernel to perform all these operations transparently, in Sytare we require some cooperation from the drivers developer: storing state in a \emph{device context}, implementing a \lstinline{restore()} function and a \lstinline{save()} function for each driver.

A complete state machine of Sytare's control flow is given in the next Section.

% \begin{figure}[btp]
% \centering
% \includegraphics[width=\textwidth]{sequential-state-machine.pdf}
% \caption{State machine of Sytare for sequential flow.}
% \label{fig:sequential-state-machine}
% \end{figure}


\subsection{Interrupt handling}
\label{sec:problem-statement}


A standard approach for interrupt handling in operating systems consists in splitting interrupt handlers into two parts~\cite{senspire}.
When an interrupt occurs, it is immediately handled by the operating system that executes a so-called \textit{top-half} routine with IRQs disabled.
This routine typically acknowledges the interrupt, and registers a deferrable \textit{bottom-half} in charge of handling the lengthy operations associated with the interrupt. In Sytare, bottom halves are at the moment designed to be non-nestable, \textit{i.e.}, no other bottom half can be run when a bottom half is interrupted, but interrupts are left enabled in order to be able to react upon powerloss occurrence.

While an interrupt top-half takes care of very low-level operations, the application developer is likely to request peripheral access from a bottom half. For instance if the application developer wants to read a radio packet upon radio reception interrupt, it might be natural to call relevant syscalls in the dedicated user-written interrupt handling procedures.
To this extent bottom halves are allowed to use syscalls in the exact same way as application code would.

\subsection{Sytare's complete control flow}

Figure~\ref{fig:complete-state-machine} depicts as a state machine Sytare's control flow. Sytare always starts in \textit{Boot} state, runs the corresponding code and switches to either \textit{Init} state or \textit{Restore} state depending on the presence of a former valid checkpoint.
\textit{Init} state initializes kernel variables and user application environment, then switches to \textit{App} state.
\textit{Restore} state restores user context and device contexts and switches to a state depending on the powerloss detection that caused the checkpoint being restored happened respectively during the \textit{App} state or the \textit{Syscall} state.
Whenever the application calls a syscall, control flow switches from \textit{App} state to \textit{Syscall} state.
When a syscall returns, control flow switches back into \textit{App} state.
When powerloss detection occurs, control flow switches to \textit{Checkpoint} state which performs the necessary memory transfers and waits for the hardware to shutdown.

\begin{figure}[h!]
\centering
\includegraphics[width=9cm]{img/complete-state-machine.pdf}
\caption{Complete state machine of Sytare.}
\label{fig:complete-state-machine}
\end{figure}


When an interrupt occurs in either state \textit{App} or \textit{Syscall}, the control flow transitions to state \textit{Kernel Interrupt top half} in charge of performing interrupt acknowledgment and scheduling the corresponding bottom half.
When a bottom half is scheduled, the control flow goes to state \textit{Bottom half}.
The state machine formed by the subset of states \textit{Bottom half} and \textit{Bottom half syscall} behaves exactly the same as the state machine formed by states \textit{App} and \textit{Syscall}.

When an interrupt other than powerloss detection occurs during \textit{App} or \textit{Syscall} state, code goes to state \textit{Kernel Interrupt top half}.
If the user has registered a bottom-half for the given interrupt, the top half schedules the bottom-half to be run before resuming the interrupted application or syscall.
So when the top-half is done, code goes to state \textit{Bottom half}.
When the bottom-half is done, code goes back to \textit{Kernel Interrupt top half} and then back to either \textit{App} or \textit{Syscall} depending on the state when the interrupt occurred.
Interrupts are enabled in \textit{Bottom half} and \textit{Bottom half syscall}.

\section{Experimental results}
\label{sec:exp}

We use a handful of benchmark applications with various levels of interaction with peripheral devices.

Basically, our experimental results show that:
\begin{itemize}
\item Sytare successfully saves and restores peripherals states even if powerloss happened during the execution of complex hardware services involving several drivers (radio frontend accessed through SPI bus).
\item Sytare has low impact on performance: the execution time of application on top of Sytare is increased by 1-3\% depending on applications. These results do not take into account the time spent saving/restoring contexts.
\item The time spent to restore contexts because of powerlosses is heavily dependent on the devices used.
\item Latency between interrupt occurrence and bottom half execution is rather small and acceptable even if the platform is powered only during a few milliseconds. Examples of latency, or top half execution time, are given in Figure~\ref{tab:interrupt-perf}.
\end{itemize}

\begin{figure}
  \centering
  \begin{tabular}{l | l}
    Interrupt source & Top half execution time ($\mu$s)
    \\\hline
    GPIO & 20 \\
    Radio packet reception & 1603
  \end{tabular}
  \caption{Median top half execution times for common interrupt sources.}
  \label{tab:interrupt-perf}
  \vspace{-0.2cm}
\end{figure}

\section{Conclusion}
\label{sec:conclusion}

We described Sytare, an operating system kernel that allows an application to execute without noticing power failures on a tiny embedded systems harvesting energy from its environment. We addressed especially the problem of making hardware peripherals \emph{persistent} across reboots.

\printbibliography

\end{document}
