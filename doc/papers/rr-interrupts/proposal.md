# Discussion
## Interrupt model
Peripherals communicate with the processor through control registers and interrupts.

Power failures also impact interrupt handling.
Most of the time they bring data, such as the index of the GPIO pin that received a rising edge, or a signal that a radio packet is received and available to be read.
Since power failure can occur at any moment, we want the interrupt data to be saved as well.

In order for the user to write code similar to a bare-metal version (cf. figure 1), it is essential to expose interrupts to them.
However, to make power failures transparent to the user, it is necessary to let the kernel take care of the relevant operations.
Therefore interrupt handling must be split into 2 parts, kernel-managed first half and user-managed second half [references to systems that use 2-part interrupts].

In addition we want to expose a higher level of interrupts than actual hardware interrupts.
For instance, on many microcontrollers, interrupts caused by different GPIO pins are mapped onto the same hardware interrupt. But they are as many different events in our model.

**Est-ce qu'on peut se passer de la notion d'event dans ce papier ? C'est sans doute plus proche de l'implementation que du design et les notions decrites dans ce papier peuvent etre comprises sans pour autant faire la distinction entre event et interrupt.**

Such interrupt sources are referred to as "events" and the user-defined routines that are called upon events are called "user event handlers".

# 1.6.3: Interrupt-related problems
Interrupts add concurrency to the application control flow.
Code is run at unexpected moments, which can cause memory inconsistency if the interrupt handlers have side effects.

Problems:
 - When the user event handlers have side effects, data inconsistency can occur if the interrupted code needed access to the concerned memory regions. [Refs?] -> Similar situation to multithreaded applications [Refs].
 - Side effects impact not only main memory, but also peripheral state. When a hardware interrupt occurs during a given sequence of peripheral accesses, the kernel-managed first half might change the state of either the peripheral being accessed or the device. (`2017-08-03-critical-sections.pdf`, section 1.1)
 - Peripheral access atomicity. Interrupts can occur in the middle of a hardware routine, when a specific sequence of operations is needed.

The most common approach to fix these interrupt-related issues is to use synchronization mechanisms such as critical sections to prevent interrupt handlers from corrupting memory and peripherals. [Refs]

# Contribution 2/2: Interrupt management

## 1. Design choice (cf. `2017-06-15.pdf`)
 - Two axes: nestedness of UEH and ability of UEH to make peripheral accesses.
 - Design we finally chose: non-nested UEH with peripheral access allowed.
 - New figure: global architecture. Requires a figure describing the simplified architecture (without interrupts) in section contribution 1/2.

## 2. Solution to race conditions within non-peripheral memory (cf. `2017-07-11/critical_sections.pdf`, section 3)
 - (No race condition between userland and kernel)
 - Solution => user-defined criticals sections where all *events* are disabled. Hardware interrupts are still enabled. The first half of each interrupt is maintained enabled, but UEH are delayed until the end of the event-protected code section. When the user enables events again, queued UEH are run before the application code actually resumes, to keep the lowest latency between hardware interrupt and UEH execution.

## 3. Solution to peripheral-related race condition (cf. `2017-08-03-critical-sections.pdf`, section 2)
 - User-defined critical sections
 - Disabling interrupts would be too harsh and prevent the powerloss detection mechanism from working.
 - Solution => for a given application code section, let the user inform the kernel which peripherals are locked. On hardware interrupt occurence, do not run the first and the second halves if the concerned peripheral is locked. Otherwise, run the first half of the interrupt but delay the execution of the second half to a point in time when no peripherals are locked. This is necessary since the kernel does not know what peripherals are used by the UEH.
 - Semantic code example.
 - Sequence diagram.

## 4. Solution to peripheral access atomicity
 - Similar situation to power failure occurring during a syscall => same solution: restore drivers + restart syscall
 - Rather pessimistic. "Often" UEH work on another peripheral than the application currently uses, in such cases it is unnecessary to restart the syscall.

