import sys
import math
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

#### Directory of gesnerated figures ####
figspath='generated_figs/'

#### Constant ####

Word_size = 2

### Cool definitions here

E_restore = 0

# Energy consumed by saving all RAM into NVRAM (CPU off)
E_all = lambda S_words, f_dma, P_dma, P_platform: S_words * (P_dma+P_platform) / f_dma

# Energy consumed by saving a single block of RAM into NVRAM (CPU off)
B_words = lambda S_words, N_blocks : math.ceil(S_words / N_blocks)
E_block = lambda S_words, N_blocks, f_dma, P_dma, P_platform: E_all(B_words(S_words, N_blocks), f_dma, P_dma, P_platform)

# Energy of detecting a dirty block
E_detect = lambda t_interrupt, E_syscall, P_cpu, P_platform: t_interrupt * (P_cpu + P_platform) + E_syscall

N_dirty = lambda alpha, N_blocks : math.ceil(alpha * N_blocks)

# Energy of checkpointing only dirty blocks
E_dirty = lambda alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform, t_overhead: N_dirty(alpha, N_blocks) * E_block(S_words, N_blocks, f_dma, P_dma, P_platform) + t_overhead * (P_cpu + P_platform)

### Scenarios start here

E_lifecycle_full_dma  = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform: E_restore + E_app + E_all(S_words, f_dma, P_dma, P_platform)
E_lifecycle_dirty_mpu = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall: E_restore + E_app + E_dirty(alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform+P_mpu, t_overhead) + N_dirty(alpha, N_blocks) * E_detect(t_interrupt, E_syscall, P_cpu, P_platform+P_mpu)

### Utils

def J2uJ(y):
    return list(i*1e6 for i in y)

def plot1D(x, y1, y2, xlabel, ylabel, label1, label2, filename):
    y1 = J2uJ(y1)
    y2 = J2uJ(y2)
    plt.clf()
    plt.plot(x, y1, "r", label=label1)
    plt.plot(x, y2, "g", label=label2)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    #    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(loc='upper left')

    tikzplotlib.clean_figure()
    tikzplotlib.save(filename, axis_width='4.5cm', extra_axis_parameters=["xlabel near ticks", "ylabel near ticks"])
    print(filename + '  done')
#    plt.show()

### Runs

def defaults():
    Vcc = 3.3
    E_restore = 0
    E_app = 0
    S_words = pow(2, 15)//Word_size
    f_dma = 8e6
    P_dma = 0
    P_platform = 50e-3*Vcc
    P_cpu = 1.3e-3*Vcc
    P_mpu = 0
    alpha = 0.3
    N_blocks = 8
    t_overhead = 3e-6
    t_interrupt = 5e-6
    E_syscall = 0
    return (E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

def run_S_words():

#    t_S_words = list(range(256*1024//Word_size+1))
    t_S_words = list(range(32*1024//Word_size+1))
    t_dma = list()
    t_mpu = list()

    for S_words in t_S_words:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)


#    filename=figspath + 'E_to_S_alpha' + str(alpha) + '_N' + str(N_blocks) + '.tex'
    filename=figspath + 'E_to_S_alpha0.8_N16_lowRAM.tex'
    plot1D(t_S_words, t_dma, t_mpu, 'S_words', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename)

def run_P_platform():

    P_max = 3.3*30e-3
    N_samples = 4095
    t_P_platform = list(i*P_max/N_samples for i in range(N_samples+1))
    t_dma = list()
    t_mpu = list()

    for P_platform in t_P_platform:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)
        
    filename=figspath + 'E_to_P_alpha' + str(alpha) + '_N' + str(N_blocks) + '.tex'
    plot1D(t_P_platform, t_dma, t_mpu, 'P_platform (W)', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename)

def run_N_blocks():

    t_N_blocks = list(range(2, 66, 2))
    t_dma = list()
    t_mpu = list()

    for N_blocks in t_N_blocks:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)

#    filename=figspath + 'E_to_N_alpha' + str(alpha) + '_' + str(S_words // 1024) + 'kWords.tex'
    filename='generated_figs/E_to_N_alpha0.3_16kWords_Plow_tlow.tex'
    plot1D(t_N_blocks, t_dma, t_mpu, 'N_blocks', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename)


##### TMP #####
E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()
alpha = 0.3
N_blocks = 16
P_platform = 50e-6*3.3
t_interrupt = 1e-6
S_words = pow(2, 15)//Word_size
run_N_blocks()
sys.exit(0)

# #####################################################
# ######## En fonction de la taille de la RAM #########
# #####################################################
E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()

##### alpha = 0.1 #####
alpha = 0.1
N_blocks = 8
run_S_words()

N_blocks = 16
run_S_words()

N_blocks = 32
run_S_words()

##### alpha = 0.2 #####
alpha = 0.2
N_blocks = 8
run_S_words()

N_blocks = 16
run_S_words()

N_blocks = 32
run_S_words()

##### alpha = 0.3 #####
alpha = 0.3
N_blocks = 8
run_S_words()

N_blocks = 16
run_S_words()

N_blocks = 32
run_S_words()

### En fonction de la puissance de la platforme
# E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()
# run_P_platform()

# ##########################################################
# ####### En fonction de la taille de la taille des blocs ##
# ##########################################################

E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()

##### alpha = 0.1 #####
alpha = 0.1
S_words = pow(2, 13)//Word_size
run_N_blocks()

S_words = pow(2, 14)//Word_size
run_N_blocks()

S_words = pow(2, 15)//Word_size
run_N_blocks()

S_words = pow(2, 16)//Word_size
run_N_blocks()

S_words = pow(2, 17)//Word_size
run_N_blocks()

##### alpha = 0.2 #####
alpha = 0.2

S_words = pow(2, 13)//Word_size
run_N_blocks()

S_words = pow(2, 14)//Word_size
run_N_blocks()

S_words = pow(2, 15)//Word_size
run_N_blocks()

S_words = pow(2, 16)//Word_size
run_N_blocks()

S_words = pow(2, 17)//Word_size
run_N_blocks()

# ##### alpha = 0.3 #####
alpha = 0.3

S_words = pow(2, 13)//Word_size
run_N_blocks()

S_words = pow(2, 14)//Word_size
run_N_blocks()

S_words = pow(2, 15)//Word_size
run_N_blocks()

S_words = pow(2, 16)//Word_size
run_N_blocks()

S_words = pow(2, 17)//Word_size
run_N_blocks()
