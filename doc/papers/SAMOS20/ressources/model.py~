import math
import matplotlib.pyplot as plt

### Cool definitions here

E_restore = 0

# Energy consumed by saving all RAM into NVRAM (CPU off)
E_all = lambda S_words, f_dma, P_dma, P_platform: S_words * (P_dma+P_platform) / f_dma

# Energy consumed by saving a single block of RAM into NVRAM (CPU off)
B_words = lambda S_words, N_blocks : math.ceil(S_words / N_blocks)
E_block = lambda S_words, N_blocks, f_dma, P_dma, P_platform: E_all(B_words(S_words, N_blocks), f_dma, P_dma, P_platform)

# Energy of detecting a dirty block
E_detect = lambda t_interrupt, E_syscall, P_cpu, P_platform: t_interrupt * (P_cpu + P_platform) + E_syscall

N_dirty = lambda alpha, N_blocks : math.ceil(alpha * N_blocks)

# Energy of checkpointing only dirty blocks
E_dirty = lambda alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform, t_overhead: N_dirty(alpha, N_blocks) * E_block(S_words, N_blocks, f_dma, P_dma, P_platform) + t_overhead * (P_cpu + P_platform)

### Scenarios start here

E_lifecycle_full_dma  = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform: E_restore + E_app + E_all(S_words, f_dma, P_dma, P_platform)
E_lifecycle_dirty_mpu = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall: E_restore + E_app + E_dirty(alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform+P_mpu, t_overhead) + N_dirty(alpha, N_blocks) * E_detect(t_interrupt, E_syscall, P_cpu, P_platform+P_mpu)

### Utils

def J2uJ(y):
    return list(i*1e6 for i in y)

def plot1D(x, y1, y2):
    y1 = J2uJ(y1)
    y2 = J2uJ(y2)
    plt.plot(x, y1, "r", x, y2, "g")
    plt.show()

### Runs

def defaults():
    Vcc = 3.3
    E_restore = 0
    E_app = 0
    S_words = 1024//2
    f_dma = 8e6
    P_dma = 0
    P_platform = 50e-6*Vcc
    P_cpu = 2.4e-3*Vcc
    P_mpu = 0
    alpha = 0.4
    N_blocks = 32
    t_overhead = 3e-6
    t_interrupt = 5e-6
    E_syscall = 0
    return (E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

def run_S_words():
    E_restore, E_app, _, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()

    t_S_words = list(range(256*1024//2+1))
    t_dma = list()
    t_mpu = list()

    for S_words in t_S_words:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)

    plot1D(t_S_words, t_dma, t_mpu)

def run_P_platform():
    E_restore, E_app, S_words, f_dma, P_dma, _, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()

    P_max = 3.3*30e-3
    N_samples = 4095
    t_P_platform = list(i*P_max/N_samples for i in range(N_samples+1))
    t_dma = list()
    t_mpu = list()

    for P_platform in t_P_platform:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)

    plot1D(t_P_platform, t_dma, t_mpu)

def run_N_blocks():
    E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, _, t_overhead, t_interrupt, E_syscall = defaults()

    alpha = 0.45
    t_N_blocks = list(range(2, 66, 2))
    t_dma = list()
    t_mpu = list()

    for N_blocks in t_N_blocks:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)

    plot1D(t_N_blocks, t_dma, t_mpu)

run_S_words()
run_P_platform()
run_N_blocks()

