%===========================================================
%                              Choix de track
%===========================================================
% Une des trois options 'parallelisme', 'architecture', 'systeme'
% doit être utilisée avec le style compas2017
\documentclass[systeme]{compas2017}
\usepackage{url}
\usepackage{listings}

\makeatletter
\lstset{
  language={},
  frame=none,
  basicstyle=\lst@ifdisplaystyle\footnotesize\fi\ttfamily,
  columns=fullflexible,
  keepspaces=true,
}
\makeatother

\usepackage{xcolor}
\usepackage[pdfborder={0 0 0}]{hyperref}
\graphicspath{{img/}}

%===========================================================
%                               Title
%===========================================================

\toappear{1} % Conserver cette ligne pour la version finale



\title{Sytare: Persistence de l'état des périphériques pour les systèmes à alimentation intermittente}

\author{Gautier Berthou, Tristan Delizy, Kevin Marquet, Guillaume Salagnac, Tanguy Risset}%

\address{
\hspace*{-1ex}  Univ Lyon, INSA Lyon, Inria, CITI,\\
  F-69621 Villeurbanne, France.\\
  \url{firstname.lastname@inria.fr}
}

\date{\today}


\begin{document}

\maketitle

%===========================================================         %
%R\'esum\'e
%===========================================================
\begin{abstract}
  Les systèmes dits \textit{à alimentation intermittente} sont de petits systèmes embarqués récupérant l'énergie dans leur environnement. À cause de contraintes de taille et de coût, ils subissent de fréquentes coupures de courant, mais sont néanmoins capables d'exécuter un programme logiciel, en sauvegardant les données nécessaires au calcul dans une mémoire non-volatile. Cet article présente une technique permettant à ces systèmes d'utiliser des périphériques non triviaux tels qu'un convertisseur analogique-numérique, une interface série ou une radio.
  \MotsCles{système embarqué, alimentation intermittente, périphériques matériels, gestion mémoire, checkpointing, système d'exploitation}
\end{abstract}


%=========================================================
\section{Introduction}
%=========================================================

% Grâce à la miniaturisation croissante des puces et à leur consommation énergétique de plus en plus basse, 

% de 

% plus en plus d'objets de notre quotidien se transforment en 

% les systèmes embarqués permettent de transformer une 

Les progrès technologiques permettent petit à petit d'embarquer par centaines de tout petits systèmes presque partout, rendant ainsi des services variés dans différents domaines tels que la santé, les transports ou les loisirs. Les  plus petits de ces systèmes utilisent un processeur lent (fréquence de l'ordre du MHz), peu de mémoire (quelques dizaines de kilo-octets), et des périphériques simples. Mais alimenter de tels systèmes est un problème car leur taille, leur prix, et le peu de maintenance qu'il peut leur être fourni interdisent l'usage d'une batterie.

Récupérer l'énergie dans l'environnement est un moyen prometteur de résoudre ce problème. À cause de leur taille, de leur prix, et souvent de la faible luminosité auquel est exposé le système, l'usage de panneaux solaires est interdit, mais d'autres moyens existent : récupération par ondes radio, par gradient de température\ldots. Cela permet l'apparition progressive de systèmes à alimentation intermittente (TPS --- \textit{Transiently-Powered Systems}), qui amassent l'énergie dans un condensateur et l'utilisent par cycles de quelques dizaines de millisecondes. Ces systèmes sont donc sujets à des coupures très fréquentes et imprédictibles. Ces coupures rendent impossible la programmation traditionnelle à base de processus s'exécutant sur des systèmes tels que FreeRTOS~\cite{GPPT16} ou Contiki~\cite{DGV04}. Ces derniers ne supportent pas les fréquentes coupures de courant, ils doivent décider quand s'arrêter et quand redémarrer.

La récente apparition de mémoires non-volatiles rapides (\textit{NVRAM} --- \textit{Non-Volatile RAM}) permet d'avancer vers des systèmes conservant leur état même lorsque leur exécution est entrecoupée de coupures de courant. Plusieurs travaux récents ont proposé de sauvegarder l'état du système avant une coupure en utilisant des mécanismes de \textit{checkpointing}. Si ces travaux permettent la savegarde et la restauration de l'état de la partie \textit{calcul} du système (registres du processeur, pile d'exécution,  variables globales), ils ne considèrent que des applications simples n'utilisant pas de périphériques complexes tels qu'un ADC, un contrôleur de bus ou un emetteur/récepteur radio. Ces périphériques ont pourtant un état à sauvegarder.

Cet article propose une nouvelle technique de checkpointing pour systèmes à alimentation intermittente incluant de la RAM non-volatile. Les mécanismes mis en \oe{}uvre permettent l'usage correct de périphériques complexes en présence de coupures de courant.

% \vspace*{-1ex}%kludge
%=========================================================
\section{Contexte et problématique}
%=========================================================

Cette partie détaille la problématique de cet article : comment permettre aux TPS d'utiliser des périphériques non triviaux.

\subsection{Les systèmes à alimentation intermittentes (TPS)}

Les systèmes embarqués tels que les téléphones ou les n\oe{}uds de réseaux de capteurs fonctionnent tous sur batterie. Dans certaines situations il est cependant impossible d'utiliser une batterie~\cite{JLLx14}. Dans ces cas là, le système doit récupérer l'énergie dans son environnement, de sources telles qu'un piezo électrique, un gradient de température ou d'ondes radio~\cite{MZLx15}. Les cartes à puce et les \textit{RFID} sont des exemples connus, mais d'autres plates-formes existent. Par exemple, la WISP (\textit{Intel's Wireless Identification and Sensing Platform}) d'Intel, ou encore la M$^3$ \cite{LBLx13} mesurant $1.0mm^3$, récupérant l'énergie dans l'environnement, et embarquant un Cortex-M0, quelques kilo-octets de SRAM et quelques kilo-octets de NVRAM.

Une caractéristique commune de ces systèmes est qu'ils doivent gérer une alimentation imprédictible. Même si de l'énergie est disponible, le niveau d'énergie récupérée est très bas~\cite{MZLx15} en comparaison de ce que le système consomme quand il est actif. Par exemple, les cartes à puce sans contact doivent effectuer un transaction complète en quelques centaines de millisecondes. Au-delà de la faisabilité, les contraintes imposées au programmeur de ces systèmes  sont très dures. Il est important de fournir à celui-ci un système d'exploitation séparé des applications, abstrayant pour lui la plus grande partie de ces contraintes.

\subsection{TPS et NVRAM}
\label{sec:program-persistence}

Un problème évident de l'alimentation intermittente est que le système va perdre ses données volatiles lors de chaque panne. Dans un TPS, l'état du processeur et le contenu de la RAM sont perdus, aussi bien que l'état des périphériques. Ces dernières années, des avancées ont été faites dans le domaine des mémoires non-volatiles. Certaines types NVRAM promettent de supprimer à terme la distinction entre mémoire non-volatile, lente, de \textit{stockage}, et mémoire rapide, volatile, de \textit{travail}~\cite{BCGL11}. Dans un TPS, remplacer naïvement la RAM par de la NVRAM a des effets indésirables. Puisque les pannes sont fréquentes, elles peuvent arriver alors qu'une structure de données non-volatile est en train d'être modifiée. Lorsque la plate-forme redémarrera, le programme reprendra avec un état incohérent, provoquant le problème dit de la {\em broken time machine}~\cite{RL14}.

 Une solution possible est d'avoir un processeur non-volatile~\cite{LLLx15}. Par exemple, Bartling et al. \cite{BKCx13} ont mis au point un tel micro-contrôleur non-volatile. Ce type d'approche est intéressant en termes d'architecture mais a des limitations majeures en termes de modèle de programmation. De plus, conserver une structure de données en NVRAM la rend persistente mais nécessite aussi que chaque accès peut être lent ou coûteux en énergie, en fonction de la technologie sous-jacente. D'un autre côté, stocker en RAM permet de bonnes performances mais pose le problème de la volatilité des données. Pour ces raisons, la plupart des systèmes utilisent maintenant une combinaison de RAM et NVRAM~\cite{BKCx13}.

Afin de conserver la cohérence entre les données en NVRAM et les données en RAM, une solution très utilisée est le \textit{checkpointing} : les coupures de courant sont détectées et avant l'arrêt complet en résultant, toutes les données volatiles nécessaires sont sauvegardées en NVRAM. Quand la plate-forme redémarre, les données sont restaurées afin que le programme reprenne son exécution d'une manière transparente.

% \vspace*{-1ex}%kludge
\subsection{Checkpointing}
\label{sec:problem-statement}

Sauvegarder l'état des données en NVRAM nécessite la détection des pannes de courant. Cela est souvent implémenté par un pont diviseur de tension, en utilisant un comparateur de tension intégré au microcontrolleur (voir~\cite{BWMx15} par exemple).

Dans presque toutes les techniques de checkpointing, deux structures sont utilisées: une image valide des données volatiles --- que nous appelons \textit{dernier checkpoint} ---, et le \textit{prochain checkpoint} (pas encore valide) en construction. Si une panne survient, toutes les données volatiles de l'exécution en cours seront copiées vers le checkpoint en construction (le \textit{prochain}) et si la copie se termine correctement les pointeurs \textit{prochain} et \textit{dernier} seront inversés. Au redémarrage de la plate-forme, le \textit{dernier} checkpoint sera restauré.

Cependant, ces travaux se concentrent sur les calculs et proposent de sauvegarder les registres du CPU, la pile d'exécution et les variables gloabales : ils ignorent les périphériques. Or, les péripériques non-triviaux tels que les ADCs, les liaisons série, ou les radios, ont une machine à états contrôlant leur exécution et qui ne peut être restaurée simplement. L'état interne du périphérique est le plus souvent accessible à travers un pilote de périphérique (que nous appellerons \textit{driver} par la suite).

Dans la suite de l'article, nous expliquons comment rendre l'état des périphériques persistant (résoudre le \textit{problème de la volatilité de l'état des périphériques}), et cohérent au fil des redémarrages de manière à ce que l'application soit insensible aux pannes de courant (résoudre le \textit{problème de l'atomicité des accès aux périphériques}).

\vspace*{-2ex}%kludge

\section{Checkpointing des périphériques}
\label{periph}

\vspace*{-2ex}%kludge

Nos mécanismes sont implémentés dans une couche logicielle entre code applicatif et code driver. Nous appelons cette couche logicielle le \textit{noyau}. De cette manière, nous les rendons plus transparents pour le programmeur. Cette solution fait l'hypothèse que l'état des périphériques ne peut changer que suite à l'exécution de code driver.

Le code noyau est donc en charge de sauvegarder et restaurer l'état des périphériques et des drivers. Ces états sont stockés en NVRAM dans une structure de données que nous appelons \textit{device context}, contenant les informations nécessaires au périphérique pour restaurer son état. Mais le noyau ne \textit{committera} en NVRAM les changements effectués sur un \textit{device context} que lorsque le péripéhérique associé aura atteint un état stable. Nous détaillons légèrement ce mécanisme ci-après mais pour plus de détails, veuillez vous référer au rappport de recherche~\cite{berthou:hal-01460699}.

\vspace*{-1ex}% kludge
\subsection{Non-volatilité de l'état des périphériques}

Restaurer l'état d'un périphérique nécessite des opérations non triviales telles que configurer certaines broches, respecter certains délais etc.
%
Dans notre proposition, une fonction de restauration est responsable de l'initialisation d'un périphérique et de le ramener dans l'état décrit dans son \textit{device context}.
%
Pour les périphériques complexes nécessitant des accès indirects (via bus SPI par exemple) ou imposant certaines contraintes particulières (délais, séquence d'instructions particulières\ldots), cette fonction peut être complexe.

\vspace*{-1ex}%kludge
\subsection{Atomicité des accès aux périphériques}

Restaurer le contexte d'un périphérique est différent de restaurer un contexte applicatif : un point d'exécution dans un code \textit{driver} ne reflète pas l'état courant d'un périphérique. Les appels au \textit{driver} doivent être protégés. Chaque fois qu'un tel appel survient, il est d'abord pris en charge par le noyau, nous appelons cet appel au noyau \textit{appel système (syscall)} par analogie avec le concept homonyme dans les systèmes d'exploitation classiques.

Le contrat entre l'application et le noyau spécifie qu'un syscall sera entièrement exécuté en un cycle de vie de la plate-forme. Si une panne survient au milieu de l'appel d'une fonction du \textit{driver}, alors au prochain démarrage, cette fonction sera re-exécutée depuis le début (au lieu de juste reprendre où elle avait été interrompue). Cela implique que le checkpointing du contexte applicatif et du contexte des \textit{drivers} sont gérés différemment.

Nous isolons donc le flot de contrôle des drivers et le flot de control applicatif en exécutant les appels aux pilotes dans une \textit{pile d'exécution séparée}. Cette pile, appelée \textit{pile noyau}, n'est jamais sauvegardée dans un checkpoint. Cela garantit qu'un progrès partiel au sein d'un code \textit{driver} sera volatile. Cette gestion de pile est effectuée par le noyau au début et à la fin d'un syscall.
% GB: Vu qu'on ne parle pas de wrapper ici, je commente cette partie pour gagner de la place. L'aspect timing du checkpointing est abordé dans la section "Exemple d'exécution avec Sytare".
%Au début d'un syscall, le noyau effectue également une copie des paramètres du syscall. Lorsqu'un syscall retourne avec succès, le noyau met à jour le \textit{prochain checkpoint} (\textit{e.g.} commite le \textit{driver context}) qui devra être sauvegardé en NVRAM lors du prochain checkpointing puis il change à nouveau la pile d'exécution avant de rendre la main au programme utilisateur.

Plusieurs détails d'importance variée sont à prendre en compte --- par exemple lorsqu'un driver en appelle un autre ---, le lecteur intéressé pourra lire le rapport de recherche~\cite{berthou:hal-01460699}.


% \vspace*{-1ex}%kludge
\subsection{Illustration}
% \vspace*{-1ex}%kludge

Le scénario simple décrit par la figure~\ref{fig:simple-syscall} illustre le mécanisme implémenté. Initialement, l'état de l'application est \textit{App\_0} et le périphérique \textit{A} a un état \textit{A\_0}. Le programme utilisateur demande l'accès au périphérique \textit{A}, ce qui doit être effectué via l'API Sytare (\textit{i.e.} la colonne \textit{kernel} à gauche de la figure).

Le \textit{last checkpoint} sur la droite du schéma représente l'état qui sera restauré si une panne survient et que le système n'a pas le temps de sauvegarder cet état. Le \textit{Next checkpoint} représente le checkpoint en cours de construction. Dans ces checkpoints, la colonne \textit{App} indique l'état de l'application à restaurer, et la colonne \textit{driver} indique l'état du driver \textit{A} à restaurer. La colonne {\em current driver call} indique la sauvegarde de l'appel au driver en cours d'exécution.%, comme expliqué en Section~\ref{periph}.

Le diagramme de séquence sur la gauche décrit un appel de la fonction du driver \lstinline{drvA_fn()} depuis le programme utilisateur. Il montre les syscalls du noyau (\lstinline{syt_drvA_fn()}) et les interactions entre le noyau et le driver. Après que le syscall a été effectué correctement, une panne est détectée, et un checkpointing est effectué. Juste avant la panne complète, le noyau fait pointer le \textit{last checkpoint} sur le \textit{next checkpoint}. L'état du programme est donc le suivant: l'application est dans l'état \textit{App\_2}, le driver \textit{A} est dans l'état \textit{A\_1}, aucun driver n'a été interrompu.

La primitive \lstinline{syt_signal()} est utilisée pour notifier le noyau que le périphérique \textit{A} a changé d'état. La promitive \lstinline{syt_commit()} rend persistent le nouvel état du périphérique dans le \textit{Next checkpoint} et supprime \textit{A} de la liste des périphériques modifiés.

\begin{figure}[hbt]

% \vspace*{-2ex}%kludge

\centering
\includegraphics[width=15cm]{simple_syscall_sequence_orig.pdf}
\caption{Diagramme de séquence d'un simple syscall, montrant le contenu des structures du noyau en RAM et en NVRAM.}
\label{fig:simple-syscall}
\vspace*{-4ex}%kludge
\end{figure}

\vspace*{-1ex}%kludge

\section{Validation expérimentale}

\subsection{Platforme matérielle}

Notre prototype est implémenté au-dessus de la carte d'évaluation Texas Instruments MSP-EXP430FR5739~\footnote{\url{http://www.ti.com/lit/ug/slau343b/slau343b.pdf}}. Le microcontrôleur MSP430FR5739 inclut 16ko de FRAM (i.e. NVRAM  ferroélectrique) et 1ko de RAM. %À notre connaissance, aucune autre gamme de microcontrôleurs n'est disponible commercialement aujourd'hui. XXX contresens
%
La limitation principale de la FRAM est son temps d'accès de 125ns, limitant la fréquence d'accès à cette mémoire à 8MHz.
%
Le reste de la plate-forme (CPU, RAM, périphériques) peut être cadencé à 24MHz, donc nos expériences sont faites à cette fréquence.
%
Nous ajoutons à cette plate-forme une carte fille comportant un émetteur-récepteur radio CC2500\footnote{\url{http://www.ti.com/lit/ds/swrs040c/swrs040c.pdf}}, connecté au microcontrôleur par un bus SPI.


\vspace*{-1ex}%kludge
\subsection{Alimentation et détection des pannes}

Dans un souci de reproductibilité, nous alimentons le système avec un générateur de tension programmable. Le signal est directement connecté aux broches \textit{Vcc} et \textit{Gnd} de la carte.

Le microcontrôleur msp430 comporte un comparateur de tension que nous utilisons pour détecter les pannes de courant. 
%
Comme dans les travaux comparables de l'état de l'art \cite{BWMx15,JRR14}
nous  ajoutons un pont diviseur de tension composé de deux résistances de 1M$\Omega$ chacune, pour que le comparateur mesure une tension différente de sa propre tension de fonctionnement.
%
La consommation supplémentaire due à ce montage (1.6$\mu$A) est négligeable devant la consommation de la plate-forme (qui peut atteindre plusieurs mA). 

% Un tel montage est courant (cf. Fig.~3~\cite{BWMx15}). Il consomme à peu près 1.6$\mu$A et nous permet de déclencher un checkpointing avec une précision théorique de 53mV. Étant donné que la tension théorique minimale pour notre plateforme est de 2.0V, nous avons fixé le seuil de checkpointing à 2.063V.

\vspace*{-1ex}%kludge

\subsection{Applications témoin}

% \vspace*{-1ex}%kludge


Nous avons évalué Sytare à l'aide de quatre applications utilisant des périphériques matériels plus ou moins complexes.
%
<<\emph{RSA}>> fait du chiffrement. Elle n'utilise pas de périphérique mais a une empreinte mémoire significative.
%
<<\emph{LEDs}>> affiche un simple compteur logiciel sur les diodes de la plate-forme.
%
<<\emph{ADC Sense}>> mesure la température ambiante avec le convertisseur analogique-numérique et stocke les valeurs dans un tableau.
%
Enfin, <<\emph{WSN}>> mesure la température et transmet les valeurs par radio vers un récepteur (qui est alimenté continûment).

Le choix de ces applications couvre plusieurs types de périphériques : simples comme les diodes, ou plus complexes comme la puce radio qui est accessible au travers d'un bus SPI.

%Nous avons utilisé quatre applications ayant des interactions variées avec les périphériques:
%\begin{description}
%\item[RSA] Cette application de calcul pur encode 128 bits de données à l'aide de l'algorithme RSA. Il n'utilise pas de périphérique mais a une empreinte mémoire significative.
%\item[Diode counter] Ce programme compte lentement de 0 à \texttt{max\_integer} et affiche la valeur du compteur grâce aux diodes de la plateforme. Il permet d'évaluer l'impact du support de la persistence sur l'utilisation des périphériques simples.
%\item[Sense and Aggregate] Cette application mesure la température 10 fois via l'ADC et stocke les valeurs dans un tableau . Un délai de 5 millisecondes est observé avant chaque nouvelle mesure.
%\item[WSN] Une application traditionnelle de réseau de capteur. Elle mesure la température en utilisant l'ADC et envoie la mesure en utilisant le périphérique radio (CC2500). Cette application montre l'usage de Sytare avec des périphériques complexes et des appels imbriqués (le chip radio est accédé via un bus SPI).
%\end{description}

\subsection{Évaluation des performances}

Pour chaque application, on mesure $T_{wired}$ comme le temps mis par l'application <<pure>> pour s'exécuter en entier, sous alimentation continue et sans le noyau Sytare. 
%
Pour les applications périodiques, le point d'arrivée est fixé arbitrairement, par exemple envoyer 10 messages, de façon à ce que la durée d'exécution soit assez grande.

Par contraste en mode intermittent, on note $T_{on}$ la durée pendant laquelle la plate-forme est alimentée entre deux périodes d'extinction.
%
Pour un $T_{on}$ donné, nous définissons $T_{transient}(T_{on})$ comme le temps requis par le système pour exécuter l'application jusqu'au point d'arrivée. 
%
Les durées $T_{transient}$ (et $T_{wired}$) n'incluent pas les moments où la plate-forme est éteinte, mais par contre le temps de démarrage du matériel est toujours comptabilisé.

% Pour une certaine valeur de $T_{on}$, nous définissons le \textit{effective yield} $Y$ comme le ratio suivant :
% %
% \begin{equation}
%   \label{eq-yield}
%   Y(T_{on}) = \frac{T_{wired}}{T_{transient}(T_{on})}
% \end{equation}

On définit ensuite le rendement $Y(T_{on})$ du système
%
comme le rapport $\frac{T_{wired}}{T_{transient}(T_{on})}$.
%
La figure~\ref{fig:yield-curves} illustre le rendement obtenu sur nos différentes applications témoin en fonction de $T_{on}$.

\begin{figure}[h]
\vspace*{-2ex}%kludge
\centering
% "trim" reduces the margins on each side. order is: left, bottom, right, top
\includegraphics[height=15ex,trim={9mm 7mm 3mm 4mm}]{img/yield-curve-rsa.png}
%
\includegraphics[height=15ex,trim={3mm 7mm 3mm 4mm}]{img/yield-curve-leds.png}
%
\includegraphics[height=15ex,trim={3mm 7mm 3mm 4mm}]{img/yield-curve-sense.png}
%
\includegraphics[height=15ex,trim={3mm 7mm 4mm 4mm}]{img/yield-curve-wsn.png}

\caption{Rendement Y obtenu pour une durée $T_{on}$ des périodes d'alimentation de 0 à $T_{wired}$.}
\label{fig:yield-curves}
\vspace*{-3ex}%kludge
\end{figure}


Pour des $T_{on}$ trop courtes, le système n'a jamais assez de temps entre deux coupures pour successivement démarrer, faire progresser l'application, et réussir un checkpoint. La métrique $T_{transient}$ est alors <<infinie>> et donc $Y$ est 0.
%
On mesure ainsi $T_{on}^{min}$ comme la plus petite durée d'alimentation $T_{on}$ qui produise un rendement non-nul.

À l'inverse, quand $T_{on}$ approche $T_{wired}$, alors l'application pourra s'exécuter en entier avant la première coupure. Mais $Y$ n'atteindra jamais 100\% à cause du temps passé à exécuter du code noyau (initialisation et syscalls). On note $Y^{max}$ le meilleur rendement ainsi observé.
%
La figure~\ref{fig:perfs} donne les valeurs de ces deux métriques globales pour nos différentes applications.

Le surcoût occasionné par Sytare sur le temps d'exécution des appels drivers est  très différent d'un driver à l'autre. 
%
La figure~\ref{fig:overhead} donne une évaluation de ces surcoûts.

\begin{figure}[bh]
  \vspace*{-4ex}%kludge
  % 
  \hfill
  %
    \begin{minipage}[t]{0.48\linewidth}
        \centering
        \begin{tabular}[t]{ r  | c  c  c  c}
                           & RSA    & LEDs   & Sense  & WSN    \\\hline
            $T_{on}^{min}$ & 2.79ms & 2.79ms & 2.90ms & 9.40ms \\
            $Y^{max}$      & 0.98   & 0.99   & 0.97   & 0.99   \\
        \end{tabular}
        \parbox{0.8\linewidth}{
          \caption{Performances globales: durée d'alimentation minimum supportable, et rendement maximum atteignable.}
          \label{fig:perfs}
        }
        
    \end{minipage}
    %
    \hfill
    %
    \begin{minipage}[t]{0.48\linewidth}
        \centering
        \begin{tabular}[t]{ r | l }
            Hardware action    & Time overhead \\\hline
            ADC sample         & +27\%         \\
            Radio sleep        & +137\%        \\
            Radio wake up      & +8\%          \\
            Radio message send & +1\%          \\
        \end{tabular}
        \parbox{0.8\linewidth}{
        \caption{Appels drivers: surcoût (en temps) occasionné par le noyau.}

        \label{fig:overhead}}
      \end{minipage}
      %
  \hfill
  %
  {}

 \vspace*{-3ex}%kludge
\end{figure}


% \begin{figure}[h!]
% \centering
% \includegraphics[width=\columnwidth]{syscall_diagram.pdf}
% \caption{Kernel temporal impact on drivers primitives calls}
% \label{fig-syscalls}
% \end{figure}

\vspace*{-5ex}%kludge

\section{Conclusion}

\vspace*{-1ex}%kludge

Cet article présente Sytare, une couche logicielle facilitant le développement d'applications embarquées sur des plates-formes à alimentation intermittente.
%
L'exécution de l'application est étalée automatiquement sur plusieurs périodes d'alimentation, sans intervention de la part du programmeur.
%
La contribution principale est un mécanisme garantissant l'accès correct aux périphériques matériels malgré les coupures de courant.
%
Les plates-formes visées sont par exemple les cartes à puces sans-contact, ou les RFID programmables, et plus généralement les petits systèmes embarqués destinés à l'Internet des objets.


Ce travail ouvre de nombreuses perspectives. 
%
Dans un premier temps, nous comptons développer des drivers pour davantage de périphériques, afin de mieux cerner les limitations imposées par nos hypothèses de travail.
%
Aussi, implémenter des applications supplémentaires nous permettrait de mieux comprendre comment l'intermitence modifie la sémantique offerte au programmeur.
%
Dans une optique à plus long terme, nous souhaitons travailler à l'intégration de Sytare dans un système d'exploitation embarqué comme Contiki ou Riot.

\bibliography{compas}

\end{document}



