
Since application will not access kernel variables and kernel will not modify application variables, there is no race condition between application and kernel as far as non-peripheral data is concerned. The only race conditions that can be observed with respect to non-peripheral data is between application code and user-defined bottom-halves of interrupt handlers, since they are likely to share data.

This situation is caused by bottom-halves being executable while user application is accessing common data. Hence Sytare provides a mechanism that enables the user to manually define critical sections where the bottom-halves cannot run.

A typical example of bottom-half scheduling with critical section is depicted in Figure~\ref{fig:disable-bottom-halves}.
The critical section starts when \lstinline{syt_disable_bottom_halves()} returns, and ends when \lstinline{syt_enable_bottom_halves()} returns.
Between these calls, the top-halves of all interrupt handlers are kept enabled, but the bottom-halves are delayed until the end of the user-defined critical section.
A sequence diagram corresponding to this scenario is given in Figure~\ref{fig:seq-critical-section}.
In the example, \lstinline{drv_A_on_interrupt()} runs but its matching bottom-half, named \lstinline{deviceA_bottom_half()}, is not run before returning to application code, which would have been the expected behaviour if the user had not specified any critical section.
On the contrary, the application is scheduled first and runs until \lstinline{syt_enable_bottom_halves} is called.
Since bottom-halves are now enabled again, the bottom-halves that were queued during the critical sections run before resuming application.
In the example, the driver A bottom-half was queued by kernel and its matching bottom-half is run before resuming application.

\begin{figure*}
    \begin{subfigure}[b]{0.45\linewidth}
        \begin{lstlisting}[language=C]
static volatile int x = 0;

deviceA_bottom_half()
{
  ++x;
}

main()
{
  while(...)
  {
    if(x == 0)
    {

      /* The code here may
         read and write x */

       ...

    }
  }
}
        \end{lstlisting}
        \vfill
        \caption{Code without critical section}
      \end{subfigure}
      \begin{subfigure}[b]{0.5\linewidth}
        \begin{lstlisting}[language=C]
static volatile int x = 0;

deviceA_bottom_half()
{
  ++x;
}

main()
{
  while(...)
  {
    if(x == 0)
    {
      syt_disable_bottom_halves();

      /* No bottom-half can run and
       * corrupt the value of x */

      ...

      syt_enable_bottom_halves();

      /* All queued bottom-halves will
       * be run before it returns */
    }
  }
}
        \end{lstlisting}
        \caption{Code with critical section}
      \end{subfigure}
      \caption{Not protected code vs. user-defined critical section.}
  \label{fig:disable-bottom-halves}
\end{figure*}

This mechanism aims at preventing data race conditions while keeping the latency between hardware interrupt occurrence and bottom-half execution very low.
%Figure~\ref{fig:seq-critical-section} illustrates the scheduling of the various pieces of code involved in this mechanism. It shows how the execution of a bottom-half is delayed in time when a critical section is used in the application code.

\begin{figure}[h]
\resizebox{\linewidth}{!}{
\begin{sequencediagram}
  \lifeline{app}{App}
  \lifeline{ueh}{A bottom-half}
  \lifeline{gpio}{Drv A}
  \lifeline{ker}{Kernel}

  \advancetime{10}
  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \arrow[\scriptsize syt\_disable\_bottom\_halves()]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \event[\scriptsize IRQ GPIO]{app}
  \arrow[\scriptsize ISR]{app}{ker}
  {
    \activate{ker}
    \advancetime{5}
    \advancetime{5}
    \deactivate{ker}
    \arrow[\scriptsize drv\_A\_on\_interrupt()]{ker}{gpio}
    {
        \activate{gpio}
        \advancetime{10}
        \deactivate{gpio}
        \arrow[\scriptsize $return$]{gpio}{ker}
    }
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $reti$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \arrow[\scriptsize syt\_enable\_bottom\_halves()]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize deviceA\_bottom\_half()]{ker}{ueh}
    {
        \activate{ueh}
        \advancetime{10}
        \deactivate{ueh}
        \arrow[\scriptsize $return$]{ueh}{ker}
    }
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \advancetime{10}
\end{sequencediagram}
}
\caption{Sequence diagram showing the critical section mechanism, with a platform equipped with device A. The application represented corresponds to the one of Figure~\ref{fig:disable-bottom-halves}.}
\label{fig:seq-critical-section}
\end{figure}

\subsection{Solution to peripheral race conditions}

Handling interrupts may have side effects not only in main memory, but also on peripherals' state. Indeed, when a hardware interrupt occurs during a given sequence of peripheral accesses, the (kernel-managed) top-half might change the state of either the peripheral being accessed or its device context. We explain wy and how hereafter.

\begin{figure*}
  \centering
    \begin{tabular}{l|l}
        \begin{minipage}{4cm}
            \begin{lstlisting}[language=C]
spi_config(radio_config);
radio_set_mode(TX);
radio_send_packet(packet);
            \end{lstlisting}
        \end{minipage}
        &
        \begin{minipage}{4cm}
            \begin{lstlisting}[language=C]
/* Acquire locks of SPI and radio */
syt_exclusive_access_begin(SPI|RADIO);

syt_spi_config(radio_config);
syt_radio_set_mode(TX);
syt_radio_send_packet(packet);

/* Release locks of SPI and radio */
syt_exclusive_access_end(SPI|RADIO);
            \end{lstlisting}
        \end{minipage}
\\
Example of sequence of accesses
&
(a) Sequence of accesses to peripherals protected
\\
to peripherals.
& (b) by peripheral locking mechanism.


    \end{tabular}
    \caption{Description of the peripheral locking mechanism from the application developer point of view.}
    \label{fig:periph-sequence}
\end{figure*}

In the example given in Figure~\ref{fig:periph-sequence}, if the platform is equipped with another indirect peripheral to be accessed through SPI bus, if an interrupt occurs during the radio call above, the kernel-defined top-half might access to the other SPI peripheral and alter SPI configuration.
After the interrupt handler returns, communication between the microcontroller and the radio peripheral might be broken if the SPI bus is not reset to its former configuration. 

As of non-peripheral data, peripherals face the same problems described in Section~\ref{sec:data-race-conditions}.
In addition, kernel-defined top-halves of interrupt handlers need access to peripherals in order to retrieve and store data in the device context.
Those peripheral accesses issued from kernel can interfere with peripheral accesses issued from application code. 

The problem is caused by first and bottom-halves of interrupt handlers being potentially run during a particular sequence of user-called system calls. An obvious solution to this problem would be to let the user disable interrupts when needed, but this would compromise the checkpointing mechanism since it is based on interrupts being enabled in order to know when to checkpoint.

To solve this problem, Sytare provides a peripheral locking system controllable on user initiative.
Interrupts are kept enabled, but all the kernel-defined top-halves check if the peripheral they try to access is locked or not.
If the peripheral is not locked, the rest of the top-half is run and the bottom-half is delayed to a point in time when no peripherals are locked.
This is necessary since the kernel does not know what peripherals are used by user-defined bottom-halves. If the peripheral is locked, both the remainder of the top-half and the bottom-half of the interrupt handler are discarded. This means that the interrupt signal is ignored.

%% TODO: expliquer les différentes solutions que l'on a envisagé, et laquelle on a implémenté

The actual sequence of operations induced by the code exposed in Figure~\ref{fig:periph-sequence} while interrupts occur is shown in Figure~\ref{fig:seq-ueh}.
The hardware platform is equipped with an internal timer, a SPI bus, a radio module accessible through SPI and an ADC module accessible through SPI.
When \lstinline{syt_exclusive_access_begin(SPI|RADIO)} returns, the kernel knows that both SPI and radio peripherals are locked.
Between the execution of \lstinline{syt_spi_config} and the execution of \lstinline{syt_radio_set_mode}, a SPI ADC interrupt occurs.
Since the SPI ADC top-half relies on the SPI driver and SPI is locked by user, both the top and the bottom-halves of the SPI ADC interrupt are discarded.
When the application resumes, a timer interrupt occurs.
Since the timer top-half uses neither the SPI driver nor the radio driver, the timer top-half is immediately run.
However the peripherals are still locked, which prevents the timer bottom-half from being run at that point.
Then application resumes and radio calls work as usual.
When \lstinline{syt_exclusive_access_begin(SPI|RADIO)} returns, the kernel knows that all peripherals are unlocked.
Before returning to application, the bottom-half of the timer interrupt, that was formerly scheduled by the corresponding top-half, is run.

\begin{figure*}[h]
\resizebox{\textwidth}{!}{
\begin{sequencediagram}
  \lifeline{app}{App}
  \lifeline{ueh}{Timer bot. half}
  \lifeline{spi}{Drv SPI}
  \lifeline{rad}{Drv SPI radio}
  \lifeline{tim}{Drv timer}
  \lifeline{adc}{Drv SPI ADC}
  \lifeline{ker}{Kernel}

  \advancetime{10}
  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \arrow[\scriptsize syt\_exclusive\_access\_begin(...)]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \arrow[\scriptsize syt\_spi\_config(...)]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize spi\_config(...)]{ker}{spi}
    {
        \activate{spi}
        \advancetime{10}
        \deactivate{spi}
        \arrow[\scriptsize $return$]{spi}{ker}
    }
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \event[\scriptsize IRQ SPI\_ADC]{app}
  \arrow[\scriptsize ISR]{app}{ker}
  {
    \activate{ker}
    \advancetime{5}
    \noteright{ker}{\scriptsize ignore}
    \advancetime{5}
    \deactivate{ker}
    \arrow[\scriptsize $reti$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \event[\scriptsize IRQ Timer]{app}
  \arrow[\scriptsize ISR]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}

    \arrow[\scriptsize timer\_on\_overflow()]{ker}{tim}
    {
        \activate{tim}
        \advancetime{10}
        \deactivate{tim}
        \arrow[\scriptsize $return$]{tim}{ker}
    }

    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize $reti$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \arrow[\scriptsize syt\_radio\_set\_mode(TX)]{app}{ker}
  {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize radio\_set\_mode(TX)]{ker}{rad}
      {
        \activate{rad}
        \advancetime{10}
        \deactivate{rad}

        \arrow[\scriptsize spi\_write(...)]{rad}{spi}
        {
            \activate{spi}
            \advancetime{10}
            \deactivate{spi}
            \arrow[\scriptsize $return$]{spi}{rad}
        }

        \activate{rad}
        \advancetime{10}
        \deactivate{rad}
        \arrow[\scriptsize $return$]{rad}{ker}
      }
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}

  \arrow[\scriptsize syt\_radio\_send(...)]{app}{ker}
  {
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize radio\_send(...)]{ker}{rad}
      {
        \activate{rad}
        \advancetime{10}
        \deactivate{rad}

        \arrow[\scriptsize spi\_write(...)]{rad}{spi}
        {
            \activate{spi}
            \advancetime{10}
            \deactivate{spi}
            \arrow[\scriptsize $return$]{spi}{rad}
        }

        \activate{rad}
        \advancetime{10}
        \deactivate{rad}
        \arrow[\scriptsize $return$]{rad}{ker}
      }
      \activate{ker}
      \advancetime{10}
      \deactivate{ker}
      \arrow[\scriptsize $return$]{ker}{app}
  }

  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \arrow[\scriptsize syt\_exclusive\_access\_end(...)]{app}{ker}
  {
    \activate{ker}
    \advancetime{10}
    \deactivate{ker}
    \arrow[\scriptsize run postponed user-defined bottom-halves]{ker}{ueh}
  }

  \activate{ueh}
  \advancetime{10}
  \deactivate{ueh}
  \arrow[\scriptsize $return$]{ueh}{app}

  \activate{app}
  \advancetime{10}
  \deactivate{app}
  \advancetime{10}
\end{sequencediagram}
}
\caption{Sequence diagram showing the peripheral locking mechanism, with an internel timer peripheral, an external radio module through SPI and an external ADC module through SPI. The application represented corresponds to the one of Figure~\ref{fig:periph-sequence}.}
\label{fig:seq-ueh}
\end{figure*}

\subsection{Solution to peripheral access atomicity}

Interrupts can occur in the middle of a hardware routine, when a specific sequence of operations is needed.
The most common approach to fix these interrupt-related issues is to use synchronization mechanisms such as critical sections to prevent interrupt handlers from corrupting memory and peripherals~\cite{Corbet:2005}. %% TODO: expliquer lesquels ici (mutex, spinlock...) ?

One could argue that adapting the driver API would be a solution, adding conditional guards and device context preservation to driver routines in order to entirely eliminate this problem.
However, driver routines are often kept as simple as possible and adding such mechanisms to driver routines would add overhead. Furthermore it would require more work from the driver developer. In addition if driver routines get larger in terms of execution time and energy consumption, they will become more likely to provoke power failure while running.
This would lead to situations where, on boot, Sytare reruns a given system call that would never finish before power failure occurs. Such a situation would prevent the application from making any progress. Hence driver routines are kept simple in Sytare.

%% TODO : ce paragraphe est pas ultra clair. Un chronogramme ou un exemple serait bienvenu je pense. Aussi, ça vaudrait le coup de donner les approches alternatives, ou dire qu'elles sont discutées + tard.
As a result, Sytare adopts a pessimistic approach towards system call interruption: peripherals and device contexts are restored to the state they had before the system call, and the system call itself will be rerun instead of resumed on interrupt handler completion. This solution considers non-powerloss interrupts the same as powerloss interrupt since it considers the worst case. However often the top and bottom-halves use other peripherals than the peripheral currently used by the application.
In such cases it is unnecessary to rerun the system call.

