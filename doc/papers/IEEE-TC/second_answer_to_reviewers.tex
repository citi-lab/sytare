\documentclass[10pt,journal,onecolumn]{IEEEtran}
%\documentclass[prodmode,acmtaco]{acmsmall}
\usepackage{listings}
%\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{textcomp}
\usepackage[textsize=footnotesize]{todonotes}
\lstset{
   breaklines=true,
   basicstyle=\ttfamily}

 \presetkeys%
 {todonotes}%
 {inline}{}

%\input{macro.tex}
\title{Second answers to reviewers for minors changes in  the paper: "Sytare: a Lightweight Kernel for NVRAM-Based Transiently-Powered Systems"}
\author{Gautier~Berthou, Tristan~Delizy, Kevin~Marquet, Tanguy~Risset, Guillaume~Salagnac}
\date{\today}
\begin{document}
\maketitle

    
\section{Introduction}
We are very happy to see that the major re-writing of the paper that we have processed, following reviewers comments,  has been appreciated by the reviewers.
At that stage, reviewer 4 is satisfied with the current version, reviewer 2 has only a small concern that we will address.
Hence the main changes that we propose in the document address the comments of reviewers 1 and 3. 

In this document, we answer to the second set of reviews.
These answers implied several small changes in the paper plus three important changes:
\begin{itemize}
    \item The first important change is asked by reviewer 3 who requires that Sections 3 (Peripheral state persistence) and 4 (Interrupt handling) should be merged because they correspond to the solution of the three problems presented in Section 2.5 (Problem statement).
    As we agree with that remark, we changed the paper accordingly.
    We hope that this change will also satisfy the reviewers that were already satisfied with the former structure.
    \item The second change is asked by reviewer 1 who requested to show a brief system/hardware architecture.
    We added a new Figure (Figure~2) as an overview for this purpose.
    \item The third change clarifies the position of this paper with respect to our former publication.
    We have now mentioned it explicitely in the introduction.
\end{itemize}

Through this review process, we improved other parts of the paper as well.
The most important changes, including the ones mentioned aboved, are highlit in yellow in the papers.

In the rest of this document, we provide precise answers to each review.
The reviews are exposed in {\tt courrier} font, while our answers are written in times font.
\section{Precise Answers to Reviewers }
\vskip 0.5cm
\subsection{Answers to Referee 1}

\begin{lstlisting}
This paper proposes a new kernel and some important mechanisms for keeping consistency and persistency of the application and system states in transiently-powered systems, which may experience frequent power shortages in real world. To retain the application states and system states (including peripheral states), they adopt NVRAM as their key system component and use it for context saving and recovery. They implemented their kernel on a micro-controller hardware and performed several experiments on it. Several analyses, which shows the characteristics of the proposed Sytare kernel, are presented in the paper. The revised version of the paper focuses on the major idea more clearly and has better-structured organization. But, there is still some issues to be addressed.
\end{lstlisting}
Thank you for pointing out the improvement of the paper.

\begin{lstlisting}
First of all, it is recommended to show brief system/hardware architecture which includes the CPU, memory(DRAM and NVRAM), storage, and other peripherals, in the paper. Overall software architecture of the Sytare kernel, including its key components and functions, is also recommended to be described in the paper, so as to help readers to easily understand which functionalities the Sytare has (and which ones does not have). It might help the readers to easily understand the differences between the Sytare kernel and the existing kernels such as other TPS kernels and Linux.
\end{lstlisting}

We added such a description as Figure~2.
It shows both hardware (from energy harvester to CPU and peripherals) and software architectures.

\begin{lstlisting}
The authors said that, in the Answers to the first review, side effects, which may occur due to power failure in the middle of a hardware request being serviced, will be managed at a higher level than in the kernel, and it is the role of the network layers. But, then, where the network layer should be placed in the software stack of the proposed system, if it is not supported by the kernel? (Generally, network layer should be in the kernel, because applications should access it via the systems calls.) Also, how about the side effects that occur due to power failures during the communication between the host and its peripherals?
\end{lstlisting}

Network and communication protocol layers should not be part of the kernel as we chose to build a kernel which is {\em as small as possible} (lightweight).
The sole purpose of Sytare is to provide the required mechanisms to achieve persistence across power outages.
Communication protocols are a separate concern.
However it would be interesting, in the future, to see how power outages could impact higher level layers.

Your question is important because it reminds us that many embedded operating systems provide the whole network stack in the operating system while this is not the case for Sytare. In order to remove this ambiguity, we have clarified this point by adding a sentence (Section~3.3).

Regarding the second part of your question: {\tt about the side effects that occur due to power failures during the communication between the host and its peripherals}, this situation cannot happen, because communication between host and external peripheral (\textit{e.g.}, radio) is performed by other peripherals.
SPI bus and UART bus {\em are} peripherals themselves.
Hence this ``communication state'' will be handled by Sytare as any other peripheral state, using the system call mechanism depicted in the paper.

\begin{lstlisting}
The authors describe the FR5739 microprocessor in Section 5.1, especially on the voltage comparison unit of the processor, which is used to implement power failure detection. But, it seems that the description is not in the main scope of the paper and may be removed from the paper. Instead, the overall CPU and memory architecture is recommended to be described in the paper, as aforementioned.
\end{lstlisting}
We agree with this remark, this part was rather technical and did not help understanding.
As a consequence, we have shortened the description of the voltage comparison unit and removed the Figure, only leaving the reference of other work using the same technique (Section~4.1).
Still, it is important that the reader understands that power outage is detected before it actually occurs, otherwise Sytare would be of no use.

\begin{lstlisting}
In Section 6, the authors evaluate the characteristics of the proposed system using 4 application benchmarks, including boot times, system call related times, memory occupations, kernel and user stack utilizations, and energy overheads. Additionally, how about evaluating the context saving or checkpointing overhead, also with recovery overhead?
\end{lstlisting}
The value of the checkpointing overhead was already present in the paper: 26~\textmu{}s (Section 6.4.1 in the former version, Section 5.4.1 in the new one).
However, it was not really visible.
With your remark, we decided to state it clearly earlier in the paper and we added a sentence about checkpointing time in Section 4.3.

\begin{lstlisting}
It may be evaluated according to the frequency of power failures, because the readers want to have information on how much is the checkpointing and recovery overhead and how those overheads change according to the frequency of power failures.
\end{lstlisting}

In our implementation, we use a straightforward DMA copy of the whole memory (1kB only) when saving checkpoint to NVRAM.
We did not mention the use of a DMA in the former versions of the paper, but your question shows that we should have done it.
We have added a brief mention of the DMA copy in Section~4.3.
As we copy the whole application memory at each checkpoint, the overhead of each checkpoint and recovery should be independent of the frequency of power failure.
This is why we do not talk about that in the paper.
The implementation of a more optimized checkpoint procedure has been formerly studied by us in~\cite{AAMS14}.
However given the use of a DMA and given the small size of the memory, selecting parts of the memory to be checkpointed would provide very little speedup (and might bring slowdown) to the checkpoint procedure.

\begin{lstlisting}
Finally, the paper might still be improved for readability and understandability. As commented in the review of the first version of the paper, it is important to give a clear description on the author's idea on proposed architectures and mechanisms in the paper. (e.g., in 3rd paragraph of Section 2.1, the sentences starting with "In particular," should be corrected; in several sections, "consists in" should be replaced with "consists of"; etc.)
\end{lstlisting}
We have performed the change.
Again, we are sorry about these errors due to the fact that we are not native English-speaking people.
We have asked again a native English-speaking person to read the paper, this provided many little changes. 

\vskip 0.5cm
\subsection{Answers to Referee 2}
\begin{lstlisting}
Sytare has been proposed before this journal submission, but the journal manuscript says ``we present an implementation of these principles: the Sytare lightweight kernel running on a microcontroller equipped with RAM and NVRAM.'' As a journal version, it has to clearly specify the difference between the new Sytare and the original Sytare. Otherwise, it might overclaim the contribution and novelty on designing and implementing the lightweight kernel Sytare. The other questions and suggestions of this reviewer have been addressed. 
\end{lstlisting}
We understand that this is a major concern for TCSI.
As we already mentioned several times, the Sytare kernel makes sense only if interrupt handling is available.
So we are convinced that the paper presented here is the only way to understand the principles of Sytare.
The former publication~\cite{ioent} is rather short (6 pages) and does not explain the mechanisms in detail.

Following your request, we have modified the introduction and clearly "specify the difference between the new Sytare and the original Sytare" with the following sentences:

"Compared to the former published implementation
~\cite{ioent}
, the work presented in this paper includes interrupt handling which is definitely mandatory for any embedded application.
The present paper also explains the solution to the problems mentioned above in a more detailed fashion."

We hope that it will be sufficiently convincing for TCSI editorial board.

\vskip 0.5cm
\subsection{Answers to Referee 3}

\begin{lstlisting}
The key contribution of the paper is a transactional checkpoint mechanism designed for power transient embed systems. It splits software into 2 domains (i.e., applications and drivers) and handles their running states differently. The state of the driver domain is volatile upon power failure, but the system automatically reproduces it by executing callback functions defined by driver developers. The state of the application domain is non-volatile. When an application executes a system call, the state is atomically saved in non-volatile memory. It contributes to presenting the design of system software for power transient devices.

Although the paper has been improved, the logic of the paper is still problematic. It is still tough to follow the story.

The structure of the paper is not good. For example, Section 2.5 explains 3 problems (i.e., state volatility, access atomicity and interrupt handling) in parallel. If so, Section 3 should have the corresponding solutions also in parallel. But, currently, Section 3 has the solution to state volatility, the solution to access atomicity and an example of complex system call. The third one should be the solution to interrupt handling. 
\end{lstlisting}
We agree with that change.
Given that we decided to present Sytare as a whole project (without omitting that a former version was published as mentioned in the answer to reviewer 3), it makes no sense to separate the three problems indentified: state volatility, peripheral access atomicity and interrupt handling.
Hence, we shortened Section 3 a little, merged Section 2 and 3, and named the resulting Section {\em Handling peripherals in a TPS}.

\begin{lstlisting}
Similarly, the phrases in Introduction, ``(i) the definition of a new driver API'' and ``(iii) an improved checkpointing mechanism'', should be replaced with better phrases that clearly mean they are the solutions to state volatility and interrupt handling problems, respectively.
\end{lstlisting}
Here also we agree that this part was not clear enough.
We have re-written the whole paragraph, in the introduction, hoping that it makes more sense now.

\begin{lstlisting}
But, to begin with, the true nature of the problem in interrupt handling is not clear. It may not be different from state volatility and access atomicity. If interrupt handling is rather an implementation issue, it should be moved to the implementation section or appendix, (or dropped). It is strange that interrupt handling is located at the same level as state volatility and access atomicity. 
\end{lstlisting}
Here we do not agree with you.
To some extent, you are right because the only real new element in TPS is the presence of non-volatile memory.
Interrupt handling has been studied in many other contexts and the solution proposed here is based on an existing interrupt handling strategy.
However, even though interrupt handling itself does not directly use non-volatile memory, it removes the assumption of sequential code which substantially simplified the process of ensuring data and peripheral consistency.
Interrupt handling must be designed with care in order to have a correct and deterministic way of ensuring consistency of the whole system after power loss.
Again, we insist that interrupt handling should be located at the same level as state volatility and access atomicity as far as kernel design for TPS is concerned.

\begin{lstlisting}
Also, it is not necessary to treat state volatility as a problem to be addressed. It is rather an assumption (which is normally explained in a former section such as the background section). 

So, the remaining, the essential problem is atomicity. Saying consistency might be better.
\end{lstlisting}
Here again, we do not {\em really} agree.
Yes, it is an assumption to have volatile state for our application in the context of TPS.
As we have explained in the introduction, there are other approaches (non-volatile processors, using NVRAM as RAM, \textit{etc.}).
However, most important works related to ultra-low power leverage the same assumption: computation in RAM and checkpointing when power loss occurs~\cite{JRR14, BKCx13,BWMx15}.
Hence, we believe it is the most realistic choice for TPS since fully non-volatile processors are not very promising today.

Volatility brings problems if we want to ensure coherent execution with transient power.
Hence we insist on keeping state volatility as a problem, even if it is an assumption.

The same situation occurs for works, such as~\cite{Ismail}, which use NVRAM as main computing memory, if not only memory, because of their better density: the state is non-volatile and it rises the problems that they chose to solve. 

Again, your question highlights the fact that our explanation on that point were not clear enough.
We have modified Section 2.3 to clarify that the most probable computation model for TPS will include hybrid memory: volatile and non-volatile. 

\begin{lstlisting}
After all, I think that the cause of the opaque logic of the paper may be failure in problem statement. It will be possible to write the paper more concisely and sharply.
\end{lstlisting}
We are sorry that the problem statement was not clear to you.
We understand your remarks and made some changes, but we really think that the three problems addressed will be encountered by each one who needs to handle TPS with peripherals and frequent unexpected reboots.

\subsection{Answers to Referee 4}

\begin{lstlisting}
I am satisfied with this version. Good job!
\end{lstlisting}
Thank you a lot for that remark! 

\bibliographystyle{IEEEtran}
\bibliography{second_answer_to_reviewers}

\end{document}
