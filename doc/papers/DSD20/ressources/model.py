import sys
import math
import matplotlib.pyplot as plt
import numpy as np
import tikzplotlib

#### Directory of generated figures ####
FIGSPATH = 'generated_figs/'

#### Constant ####

WORDSIZE = 2

### Cool definitions here

E_restore = 0

# Energy consumed by saving all RAM into NVRAM (CPU off)
E_all = lambda S_words, f_dma, P_dma, P_platform: S_words * (P_dma+P_platform) / f_dma

# Energy consumed by saving a single block of RAM into NVRAM (CPU off)
B_words = lambda S_words, N_blocks : math.ceil(S_words / N_blocks)
E_block = lambda S_words, N_blocks, f_dma, P_dma, P_platform: E_all(B_words(S_words, N_blocks), f_dma, P_dma, P_platform)

# Energy of detecting a dirty block
E_detect = lambda t_interrupt, E_syscall, P_cpu, P_platform: t_interrupt * (P_cpu + P_platform) + E_syscall

N_dirty = lambda alpha, N_blocks : math.ceil(alpha * N_blocks)

# Energy of checkpointing only dirty blocks
E_dirty = lambda alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform, t_overhead: N_dirty(alpha, N_blocks) * E_block(S_words, N_blocks, f_dma, P_dma, P_platform) + t_overhead * (P_cpu + P_platform)

### Scenarios start here

E_lifecycle_full_dma  = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform: E_restore + E_app + E_all(S_words, f_dma, P_dma, P_platform)
E_lifecycle_dirty_mpu = lambda E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall: E_restore + E_app + E_dirty(alpha, S_words, N_blocks, f_dma, P_dma, P_cpu, P_platform+P_mpu, t_overhead) + N_dirty(alpha, N_blocks) * E_detect(t_interrupt, E_syscall, P_cpu, P_platform+P_mpu)

### Utils

def J2uJ(y):
    return list(i*1e6 for i in y)

def plot1D_J2uJ(x, y1, y2, xlabel, ylabel, label1, label2, filename, constrainX):
    y1 = J2uJ(y1)
    y2 = J2uJ(y2)
    plt.clf()
    plt.plot(x, y1, "r", label=label1)
    plt.plot(x, y2, "g", label=label2)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    #    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(loc='upper left')

    tikzplotlib.clean_figure()

    if constrainX == 1:
        tikzplotlib.save(filename, axis_width='4.5cm', extra_axis_parameters=["xlabel near ticks", "ylabel near ticks",  "xtick distance=3000", "x tick label style={/pgf/number format/.cd, set thousands separator={},fixed}"])
    else:
        tikzplotlib.save(filename, axis_width='4.5cm', extra_axis_parameters=["xlabel near ticks", "ylabel near ticks", "x tick label style={/pgf/number format/.cd, set thousands separator={},fixed}"])

        
    print(filename + '  done')

def plot1D_solo(x, y, xlabel, ylabel, filename):
    plt.clf()
    plt.plot(x, y, "r")
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    #    plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(loc='upper left')

    tikzplotlib.clean_figure()
    tikzplotlib.save(filename, axis_width='4.5cm', extra_axis_parameters=["xlabel near ticks", "ylabel near ticks", "x tick label style={/pgf/number format/.cd, set thousands separator={},fixed}"])
    print(filename + '  done')

def cross(y1, y2):
    if len(y1) != len(y2):
        raise Exeption("cross: y1 and y2 must have the same length")

    d = None

    for i in range(len(y1)):
        new_d = y1[i] - y2[i]
        if d is not None:
            if d * new_d < 0:
                return (True, i)
        d = new_d
    return (False, d)

### Runs

class Parameters:
    def __init__(self):
        self.Vcc = 3.3
        self.E_restore = 0
        self.E_app = 0
        self.S_words = (1 << 13) // WORDSIZE
        self.f_dma = 8e6
        self.P_dma = 0
        self.P_platform = 5e-3*self.Vcc
        self.P_cpu = 1.2e-3*self.Vcc
        self.P_mpu = 0
        self.alpha = 0.1
        self.N_blocks = 16
        self.t_overhead = 3e-6
        self.t_interrupt = 5e-6
        self.E_syscall = 0

def run_S_words(p, maxwords):
    t_S_words = list(range(maxwords+1))
    t_dma = list()
    t_mpu = list()

    for S_words in t_S_words:
        dma = E_lifecycle_full_dma(p.E_restore, p.E_app, S_words, p.f_dma, p.P_dma, p.P_platform)
        mpu = E_lifecycle_dirty_mpu(p.E_restore, p.E_app, S_words, p.f_dma, p.P_dma, p.P_platform, p.P_cpu, p.P_mpu, p.alpha, p.N_blocks, p.t_overhead, p.t_interrupt, p.E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)
    return t_S_words, t_dma, t_mpu

def run_P_platform():

    P_max = 3.3*30e-3
    N_samples = 4095
    t_P_platform = list(i*P_max/N_samples for i in range(N_samples+1))
    t_dma = list()
    t_mpu = list()

    for P_platform in t_P_platform:
        dma = E_lifecycle_full_dma(E_restore, E_app, S_words, f_dma, P_dma, P_platform)
        mpu = E_lifecycle_dirty_mpu(E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)
        
    filename=figspath + 'E_to_P_alpha' + str(alpha) + '_N' + str(N_blocks) + '.tex'
    plot1D(t_P_platform, t_dma, t_mpu, 'P_platform (W)', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename)

def run_N_blocks(p, maxblocks):
    t_N_blocks = list(range(2, maxblocks+1, 2))
    t_dma = list()
    t_mpu = list()

    for N_blocks in t_N_blocks:
        dma = E_lifecycle_full_dma(p.E_restore, p.E_app, p.S_words, p.f_dma, p.P_dma, p.P_platform)
        mpu = E_lifecycle_dirty_mpu(p.E_restore, p.E_app, p.S_words, p.f_dma, p.P_dma, p.P_platform, p.P_cpu, p.P_mpu, p.alpha, N_blocks, p.t_overhead, p.t_interrupt, p.E_syscall)

        t_dma.append(dma)
        t_mpu.append(mpu)
    return t_N_blocks, t_dma, t_mpu

def fig_3(maxwords):
    for i, N in enumerate([16, 64]):
        letter = chr(ord('a')+i)
        name = "fig3" + letter + ".tex"

        p = Parameters()
        p.N_blocks = N
        t_S_words, t_dma, t_mpu = run_S_words(p, maxwords)
        
        crosses, index = cross(t_dma, t_mpu)
        if not crosses:
            raise Exception("fig3: curves don't cross!")
        S_min = t_S_words[index]
        print("fig3%c: S_min = %u" % (letter, S_min))

        filename = FIGSPATH + name
        plot1D_J2uJ(t_S_words, t_dma, t_mpu, '$S_{words}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 1)

def fig_4(maxregions):
    for i, S_words in enumerate([8*1024, 32*1024]):
        letter = chr(ord('a')+i)
        name = "fig4" + letter + ".tex"

        p = Parameters()
        p.S_words = S_words
        t_N_blocks, t_dma, t_mpu = run_N_blocks(p, maxregions)

        filename = FIGSPATH + name
        plot1D_J2uJ(t_N_blocks, t_dma, t_mpu, '$N_{reg}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 0)

def fig_5a(nalpha):
    middle = nalpha*2//3
    alphas = list(float(ialpha) / nalpha for ialpha in range(nalpha+1))
    x = list()
    y = list()
    for i, alpha in enumerate(alphas):
        p = Parameters()
        p.alpha = alpha
        t_S_words, t_dma, t_mpu = run_S_words(p, 8*1024)
        crosses, index = cross(t_dma, t_mpu)
        if crosses:
            S_words_cross = t_S_words[index]
            x.append(alpha)
            y.append(S_words_cross)
        else:
            print("[5a] Warning: no cross (alpha=%f, d=%f)" % (alpha, index))

        if i == middle:
            filename = FIGSPATH + "fig5a_example.tex"
            plot1D_J2uJ(t_S_words, t_dma, t_mpu, '$S_{words}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 0)
    filename = FIGSPATH + "fig5a.tex"
    plot1D_solo(x, y, "$\\alpha$", "$S_{min}$ (Words)", filename)

def fig_5b(np):
    MIN_I = 0
    MAX_I = 30e-3
    middle = np//10
    currents = list(float(i) * (MAX_I-MIN_I) / np + MIN_I for i in range(np+1))
    x = list()
    y = list()
    for i, current in enumerate(currents):
        p = Parameters()
        pplatform = current * p.Vcc
        p.P_platform = pplatform
        t_S_words, t_dma, t_mpu = run_S_words(p, 8*1024)
        crosses, index = cross(t_dma, t_mpu)
        if crosses:
            S_words_cross = t_S_words[index]
            x.append(pplatform)
            y.append(S_words_cross)
        else:
            print("[5b] Warning: no cross (p_platform=%f, d=%f)" % (pplatform, index))

        if i == middle:
            filename = FIGSPATH + "fig5b_example.tex"
            plot1D_J2uJ(t_S_words, t_dma, t_mpu, '$S_{words}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 0)
    filename = FIGSPATH + "fig5b.tex"
    x = list(i*1000 for i in x)
    plot1D_solo(x, y, "$P_{plat.}$ (mW)", "$S_{min}$ (Words)", filename)

def fig_5c(nt):
    MIN_T = 0
    MAX_T = 1e-3
    middle = nt//7
    t_interrupts = list(float(i) * (MAX_T-MIN_T) / nt + MIN_T for i in range(nt+1))
    x = list()
    y = list()
    for i, t_interrupt in enumerate(t_interrupts):
        p = Parameters()
        p.t_interrupt = t_interrupt
        t_S_words, t_dma, t_mpu = run_S_words(p, 8*1024)
        crosses, index = cross(t_dma, t_mpu)
        if crosses:
            S_words_cross = t_S_words[index]
            x.append(t_interrupt)
            y.append(S_words_cross)
        else:
            print("[5c] Warning: no cross (t_interrupt=%f, d=%f)" % (t_interrupt, index))

        if i == middle:
            filename = FIGSPATH + "fig5c_example.tex"
            plot1D_J2uJ(t_S_words, t_dma, t_mpu, '$S_{words}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 0)
    filename = FIGSPATH + "fig5c.tex"
    x = list(i*1e6 for i in x)
    plot1D_solo(x, y, "$t_{int.}$ ($\mu$s)", "$S_{min}$ (Words)", filename)

def fig_5d(ni):
    MIN_I = 0
    MAX_I = 1e-3

    middle = ni//2
    i_mpus = list(float(i) * (MAX_I-MIN_I) / ni + MIN_I for i in range(ni+1))
    x = list()
    y = list()
    for i, i_mpu in enumerate(i_mpus):
        p = Parameters()
        p_mpu = i_mpu * p.Vcc
        p.P_mpu = p_mpu
        t_S_words, t_dma, t_mpu = run_S_words(p, 8*1024)
        crosses, index = cross(t_dma, t_mpu)
        if crosses:
            S_words_cross = t_S_words[index]
            x.append(p_mpu)
            y.append(S_words_cross)
        else:
            print("[5d] Warning: no cross (p_mpu=%f, d=%f)" % (p_mpu, index))

        if i == middle:
            filename = FIGSPATH + "fig5d_example.tex"
            plot1D_J2uJ(t_S_words, t_dma, t_mpu, '$S_{words}$', 'Energy ($\mu{}$J)', 'Copy', 'Incremental', filename, 0)
    filename = FIGSPATH + "fig5d.tex"
    x = list(i*1000 for i in x)
    plot1D_solo(x, y, "$P_{MPU}$ (mW)", "$S_{min}$ (Words)", filename)

fig_3(8*1024)
fig_4(64)
fig_5a(64)
fig_5b(128)
fig_5c(128)
fig_5d(128)
sys.exit(0)

# #####################################################
# ######## En fonction de la taille de la RAM #########
# #####################################################

alphas = [0.1, 0.2, 0.3]
nblockss = [8, 16, 32]

for alpha in alphas:
    for nblocks in nblockss:
        p = Parameters()
        p.alpha = alpha
        p.N_blocks = nblocks
        run_S_words(p, 8*1024)

### En fonction de la puissance de la platforme
# E_restore, E_app, S_words, f_dma, P_dma, P_platform, P_cpu, P_mpu, alpha, N_blocks, t_overhead, t_interrupt, E_syscall = defaults()
# run_P_platform()

# ##########################################################
# ####### En fonction de la taille de la taille des blocs ##
# ##########################################################

exponents = [13, 14, 15, 16, 17]

for alpha in alphas:
    for exponent in exponents:
        p = Parameters()
        p.alpha = alpha
        p.S_words = pow(2, exponent) // WORDSIZE
        run_N_blocks(p, 64)
