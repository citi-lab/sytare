% Energy model for checkpointing
---
geometry: margin=2cm
output: pdf_document
---

# 1. Symbols

| Symbol | Description | Unit |
|--------|-------------|------|
| $S_{words}$ | RAM size | words |
| $f_{DMA}$ | DMA bandwidth | words/s |
| $P_{platform}$ | Platform power without CPU, DMA and MPU | W |
| $P_{DMA}$ | DMA power | W |
| $P_{MPU}$ | MPU power | W |
| $P_{CPU}$ | CPU power in active mode | W |
| $E_{restore}$ | Checkpoint restoration energy consumption | J |
| $E_{app}$ | Energy consumed by the application throughout a single lifecycle | J |
| $\alpha$ | Average ratio of dirty blocks amongst all blocks | |
| $N_{blocks}$ | Amount of blocks/MPU regions | |
| $t_{overhead}$ | Total overhead time while checking if a block must be copied | s |
| $t_{exception}$ | Execution time of the MPU exception | s |
| $E_{syscall}$ | Average amount of energy wasted due to syscall restart when the MPU exception occurred during syscall | J |

# 2. Hypotheses

 - $E_{restore}$ does not depend on the checkpointing policy since RAM and peripherals still need to be populated from their reset values.
 - $P_{i}$ are constant during the operation that is considered.
 - Low power modes consume 0W.
 - The MPU covers the whole RAM range.

# 3. Equations

## 3.1 Save all RAM to NVRAM (DMA, CPU off)

### 3.1.1 Checkpoint all RAM
\begin{equation} \label{eq:Eall}
E_{all} = \frac{S_{words}}{f_{DMA}} \times (P_{DMA} + P_{platform})
\end{equation}

### 3.1.2 Total lifecycle energy
\begin{equation} \label{eq:Elifecycleall}
E_{lifecycle, all} = E_{restore} + E_{app} + E_{all}
\end{equation}

Equations \ref{eq:Eall} and \ref{eq:Elifecycleall} give:
\begin{equation}
E_{lifecycle, all} = E_{restore} + E_{app} + \frac{S_{words}}{f_{DMA}} \times (P_{DMA} + P_{platform})
\end{equation}

## 3.2 Save only dirty blocks

### 3.2.1 Save a single block (DMA, CPU off)
Let $B_{words}$ be the amount of words in a block:
\begin{equation} \label{eq:Bwords}
B_{words} = \left \lceil \frac{S_{words}}{N_{blocks}} \right \rceil
\end{equation}

\begin{equation}
E_{block} = \frac{B_{words}}{f_{DMA}} \times (P_{DMA} + P_{MPU} + P_{platform})
\end{equation}

### 3.2.2 MPU-based block dirtiness detection
\begin{equation}
E_{detect} = t_{interrupt} \times (P_{CPU} + P_{MPU} + P_{platform}) + E_{syscall}
\end{equation}

### 3.2.3 Checkpoint only dirty blocks
Let $N_{dirty}$ be the average amount of dirty blocks:
\begin{equation}
N_{dirty} = \left \lceil \alpha \times N_{blocks} \right \rceil
\end{equation}

\begin{equation}
E_{dirty} = N_{dirty} \times E_{block} + t_{overhead} \times (P_{CPU} + P_{MPU} + P_{platform})
\end{equation}

### 3.2.4 Total lifecycle energy
\begin{equation} \label{eq:Elifecycledirty}
E_{lifecycle,dirty} = E_{restore} + E_{app} + E_{dirty} + N_{dirty} \times E_{detect}
\end{equation}

Equations \ref{eq:Bwords} through \ref{eq:Elifecycledirty} give:
\begin{multline}
E_{lifecycle,dirty} = E_{restore} + E_{app} \\
+ \left \lceil \alpha \times N_{blocks} \right \rceil \times
\left [\left \lceil \frac{S_{words}}{N_{blocks}} \right \rceil \times \frac{1}{f_{DMA}} \times (P_{DMA} + P_{MPU} + P_{platform}) + t_{interrupt} \times (P_{CPU} + P_{MPU} + P_{platform}) + E_{syscall} \right ] \\
+ t_{overhead} \times (P_{CPU} + P_{MPU} + P_{platform})
\end{multline}

# 4 Toy examples

## 4.1 Default parameter values

Since we only want to compare both policies here, $E_{restore}$ and $E_{app}$ are "dont'care values". Hence set to 0J.
We consider that syscalls are never retried when the MPU exception occurred, $E_{syscall}$ = 0J.
Most of the values are arbitrary, but some of them are based on the MSP430FR5739 datasheets.

| Symbol | Value | Unit |
|--------|-------------|------|
| $S_{words}$ | 512 | words |
| $f_{DMA}$ | 8e6 | words/s |
| $P_{platform}$ | 50e-6 | W |
| $P_{DMA}$ | 10e-6 | W |
| $P_{MPU}$ | 50e-6 | W |
| $P_{CPU}$ | 2.4e-3 | W |
| $E_{restore}$ | 0 | J |
| $E_{app}$ | 0 | J |
| $\alpha$ | 0.5 | |
| $N_{blocks}$ | 4 | |
| $t_{overhead}$ | 3e-6 | s |
| $t_{exception}$ | 50e-6 | s |
| $E_{syscall}$ | 0 | J |

In the following simulations and graphs, all the parameters are kept at the values defined above, except if explicitly mentioned.
The red curve is the full checkpointing policy and the green curve is the MPU-based checkpointing policy.

## 4.1 RAM size
Values shown are the energy consumption (uJ) of the checkpointing operation with respect to $S_{words}$.
With these parameters, the MPU-based approach consumes less energy when the chip embeds more than 107257 words, which is 214.5 kB.
\break
![RAM](ram.png)

## 4.2 Power consumption of the platform
Values shown are the energy consumption (uJ) of the checkpointing operation with respect to $P_{platform}$ (W).
Peripherals make it vary from very low values to rather high values (dozens of mA), hence slow operations may consume a high amount of energy.
\break
![power](power.png)

## 4.3 Amount of MPU regions
Alpha is set to $0.45$.
Values shown are the energy consumption (uJ) of the checkpointing operation with respect to $P_{platform}$.
\break
![MPU](regions.png)
