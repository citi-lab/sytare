Thèse Gautier
=============

## Energie

1. apprentissage de la quantité d'énergie nécessaire à l'exécution  de chacune des primitives OS/driver. Est-ce que celle-ci dépend de la valeur des paramètres passés ?

## Interruptions

1. gestion des interruptions en monoprocessus (en cours)
    + wait-until() + traitants d'irq sans syscall
    + avec K traitants imbriqués maximum ??

2. multi-processus (cf. section multi-processus)
    * gestion des interruptions:
        - wait_until() + chaque traitant d'irq dans un processus ?
        - les traitants d'irq prioritaires sur les autres processus ?
    * partage de la mémoire: comment ??
    * partage de l'énergie:
        - commencer avec un seul processus ? Puis un seul processus utilisateur mais des traitants d'irq ?
        - choix de l'OS en fonction de la quantité d'énergie restant : 1) ctx-switch 2) attente 3) LPM 4) checkpoint (+LPM ?)
        - même question en prenant en compte une énergie calculée au pire cas par le compilateur.
    * isolation entre applications : comment ? MPU ?

## Mémoire

1. sauvegarder uniquement les données nécessaire : utiliser la MPU comme dans NVRAM OS
    * comparer avec la copie brutale.
    * implique d'implémenter ça dans un simulateur adapté, cette fois. On peut peut-être aussi juste se servir de traces, ce qui serait _beaucoup_ plus simple, en tous cas en mono-processus. À plus long terme, on pourra se servir du boulot du CEA/CAIRN autour de L-IoT.

## Archi

1. supporter les mécanismes specifiques L-IoT
    * jeu d'instruction
    * always-on / on-demand
2. dans l'autre sens : quels mécansismes utiles à l'OS pourrait-on  ajouter à un micro-contrôleur ? e.g. MPU dédiée.

3. intérêt de l'intégration d'un cache dans le micro-contrôleur ? Compromis entre conso énergétique, espace physique et accélération réelle.

## Réseaux et protocoles

1. pile de communication dédiée aux systèmes intermittents
    * adapter une de celles de Contiki ? Il en existe une programmée de manière évènementielle.
    * couche MAC/protocole de communication adaptée ? Avec des paquets très petits par exemple ? Peut être fait en collaboration avec l'équipe Agora ?
    * distinction entre communication TPS/TPS et communication TPS/Système à alimentation stable. Dans le cas d'une communication bidirectionnelle asymétrique (second cas), faut-il utiliser le même moyen de communication pour les deux sens de communication ? Exemple du point de vue du TPS : photodiode pour la réception, radio pour l'émission.

## Systèmes existants

* Ajouter support pour l'intermittence à Contiki ?
* Ajouter support pour l'intermittence à RIOT ? Boulot déjà amorcé d'une certaine manière par Daniel.

## Maths

* Formaliser nos problèmes et notre modèle.
* Prouver le concept de Sytare.

# Annexe: retour sur le multiprocess

1. Intérêt du multiprocess dans le cadre des TPS. Juste une facilité de développement de l'utilisateur ? Ou peut-on envisager plusieurs processus ? Est-ce que les processus seront dynamiques ou au contraire statique (ex: toujours un processus kernel pour Sytare, un deuxième processus pour guetter les mises à jour externes, et un troisième processus dédié à l'application utilisateur sans qu'il puisse créer d'autres processus).

2. Une application par journee + ordonnancement Round Robin
    * Evaluer les couts (temps/energie) d'un context switch pour voir si ca vaut la peine de changer de contexte au milieu d'une journee, ou si au contraire on profite d'un checkpointing force par une perte d'energie pour passer a une autre application.
    * Limites du systeme : si les journées s'allongent (le systeme passe peu souvent en-dessous du seuil de checkpointing) les context switches seront peu frequents par rapport a un Round Robin gere par quantum de temps.

3. Sauvegarder la valeur de controle des resistances du comparateur dans le contexte de chaque application -> permet d'ajuster le comportement de Sytare en fonction de l'application qui a ete executee. Ajustement de la tension de seuil, cf. algo dans nvramos

    Pour l'instant la question ne se pose pas tant que Sytare sauvegarde les mêmes données et exécute les mêmes instructions quel que soit le processus. Donc il n'y aurait pas de vraie raison pour que les seuils de checkpointing soient différents. La seule chose qui pourrait justifier à l'heure actuelle une variation dans le seuil de checkpointing est le fait que les périphériques ne sont pas nécessairement dans le même état lorsque l'interruption comparateur est générée. Donc la consommation de la carte est différente et ca peut avoir un impact sur le seuil de checkpointing.
 
3. Constat : actuellement dans Sytare les valeurs de contrôle possibles pour la resistance ladder (contrôle de V_threshold) sont entre 20 et 31. Si l'intensité du pin relié au comparateur interne est constante, on peut placer une résistance en série pour rajouter un offset constant sur la tension lue par le comparateur. En étudiant la valeur de cette résistance et éventuellement en modifiant le ratio du pont diviseur, on peut arriver a un mapping avec un plus grand degré de liberté, où une valeur de contrôle dans [0, 31] correspond à un Vcc dans [Voff, Vmax].

4. Politique de gestion des priorites :

        - P0(priorite maximale,           seuil = 2.20V)
        - P1(priorite plus faible que P0, seuil = 2.08V)
        - P0 est en cours d'execution.

    Lorsque la tension passe sous 2.20V, faut-il executer P1 pour tirer un maximum de profit de la journee courante (strategie 1 [peut-on parler de priority inversion alors que P0 ne pourrait pas s'executer de toute maniere ?]), ou au contraire s'endormir et attendre que la tension remonte bien au-dessus de 2.20V pour executer P0 a nouveau (strategie 2) ?

    => Tant qu'on n'a pas la nécessité d'un seuil de checkpointing adaptatif, cette problématique est entre parenthèses.

5. Politique de gestion des peripheriques :

    * Chaque process a son propre contexte de peripheriques (prend beaucoup de memoire, mais sert dans le cas ou l'on voudrait que seuls les peripheriques du process courant soient sous tension par exemple)
    => solution choisie
    
    * Les process partagent les contextes de peripheriques (prend peu de memoire, mais peut poser des problemes si differents process essaient d'acceder au meme peripherique en ecriture. En revanche il permet de laisser la configuration des autres peripheriques en l'etat, par exemple si on ne veut pas avoir a reinitialiser un peripherique au prochain contexte switch quand il n'y a pas eu de power loss)
       => pas viable en multi-applicatif mais peut être acceptable en mono-applicatif multi-process.

    * Choix de design : main est un process en soi ou une "super application" qui cree tous les contextes puis lance le scheduler ?
       => Solution choisie : main est un process comme les autres qui crée d'autres processus.


6. Compromis à étudier:
     - pendant qu'un périphérique bosse, soit on met le CPU en LPM, soit on context-switch.
     - changer la fréquence du processeur
     - dépendances entre tâches ?

7. Analyse statique du code utilisateur pour déterminer les périphériques utilisées pour chaque process.
       * Optimisation temporelle : charger/restorer uniquement les périphériques correspondant au process élu
       * Optimisation spatiale dans le cas où chaque process a sa mémoire de périphériques
