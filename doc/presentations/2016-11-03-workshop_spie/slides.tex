%==============================================================================%

%"Internet of tiny things": vers des systèmes embarqués communicants sans batterie
%
% Les systèmes numériques du futur seront embarqués dans toutes sortes d'objets, y compris des objets de notre quotidien:
% vêtements, mobilier, livres, articles de supermarché, etc. Lorsque les contraintes de place et/ou de coût excluent 
% l'usage d'une batterie, le système doit récolter dans son environnement de quoi s'alimenter en énergie. En termes de 
% programmation logicielle, ce scénario d'alimentation intermittente pose des défis nouveaux et intéressants. Comment 
% fournir à l'utilisateur un service de qualité alors que l'alimentation est en pointillés imprévisibles ? Une piste 
% prometteuse est l'utilisation des nouvelles technologies de mémoire à accès direct dites non-volatiles. Dans cet atelier, 
% nous proposons un aperçu des différentes technologies de RAM non-volatiles suivit de la présentation d'un prototype de 
% système d'exploitation embarqué conçu spécifiquement pour ces conditions d’exécution intermittentes.


%==================================================================== packages %
\documentclass[slideopt,A4,hyperref={pdfpagelabels=false}]{beamer}
\usepackage{import}
\usepackage{soul}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{ragged2e}
\usepackage{wrapfig}
\usepackage{tcolorbox}
\usepackage{pgf}
\usepackage{tikz}
\usetikzlibrary{shapes, calc, arrows, positioning, fit, mindmap}



\newcommand{\todo}[1]{\bigskip \textcolor{red}{\textbf{todo: #1}}\\ \bigskip}


%======================================================= theme & customization %
\usetheme{CambridgeUS}
\useoutertheme{infolines}

% colors definition
\definecolor{main}{HTML}{34676D}
\definecolor{mybackground}{HTML}{82CAFA}
\definecolor{myforeground}{HTML}{0000A0}

% apply colors
\setbeamercolor{normal text}{fg=black,bg=white}
\setbeamercolor{alerted text}{fg=red}
\setbeamercolor{example text}{fg=black}
\setbeamercolor{title}{fg=main}
\setbeamercolor{frametitle}{fg=main}

\setbeamercolor{background canvas}{fg=myforeground, bg=white}
\setbeamercolor{background}{fg=myforeground, bg=mybackground}

\setbeamercolor{palette primary}{fg=black, bg=gray!30!white}
\setbeamercolor{palette secondary}{fg=black, bg=gray!20!white}
\setbeamercolor{palette tertiary}{fg=black, bg=main}

% remove the shadows around title
\setbeamertemplate{title page}[default][colsep=-4bp,rounded=false]

% hide subsections in table of contents
\setbeamertemplate{sections/subsections in toc}[default]

% custom itemize 
\setbeamertemplate{itemize items}[triangle]
\setbeamertemplate{itemize item}{\color{main}{$\blacktriangleright$}\hskip0.1em}
\setbeamertemplate{itemize subitem}{\color{main}{$\triangleright$}\hskip0.1em}


%================================================================== tikz + pgf %
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

\tikzset{
  cpu/.style={
    rectangle,
    line width=1pt,
    draw=black,
    minimum height=1cm,
    text centered}
}


\definecolor{hugebib}{rgb} {1.0, 0.20, 0.20}
\definecolor{bigbib}{rgb} {0.9, 0.3, 0.3}
\definecolor{smallbib}{rgb} {0.8, 0.4, 0.4}
\definecolor{tinybib}{rgb} {1.0, 1.0, 0.9}


%======================================================================== meta %
\title [] {Internet of Tiny Things: vers des systèmes embarqués communicants sans batterie}
\author {T. DELIZY, K. MARQUET}
\institute {INRIA}
\titlegraphic{\bigskip\includegraphics[width=10cm]{figures/logos.png}}
\date {Nov. 3\textsuperscript{rd} 2016}


%==============================================================================%
\begin{document}
%==============================================================================%

\titlepage

% \begin{frame}[plain]
%   \maketitle
% \end{frame}


\begin{frame}
  \frametitle{Outline}
  \tableofcontents%[currentsection,currentsubsection]  
\end{frame}

 % 1. NVRAM Technologies
 % 2. SYTRARE
 % 3. Demo

%==============================================================================%
\section{NVRAM and embedded systems}
%==============================================================================%
\begin{frame}
  \frametitle{A computer (View from very-far-away) }
  
  %% Schéma avec mémoire + disque dur 
    \begin{figure}
    \centering
    \scalebox{0.8} {
      \begin{tikzpicture}
        \node[cpu, text width=2cm, fill=gray!25, opacity=0.8] (cpu) {CPU};
        \node[cylinder, draw, minimum height=2cm, minimum width=1.5cm, shape border rotate=90, line width=1pt, anchor=north] (disk) at ($(cpu.south) +(0, -2)$) {Disk};
        
        \onslide<1-5, 7-8> {
          \node[cpu, text width=2cm, anchor=north, minimum height=1cm] (dram) at ($(cpu.south) +(0, -0.5)$) {DRAM};
          \draw[<-] (dram.north) -- (dram.north |- cpu.south);
          \draw[->] (dram.south) -- (disk.north);
        }
        
        \onslide<2-3> {
          \node[cpu, text width=2cm, anchor=north, minimum height=1cm] (dram) at ($(cpu.south) +(0, -0.5)$) {DRAM};

          \node[color=red] (ramc) at ($(dram.east) +(4, 0)$) {
            \begin{tabular}{l}
              \textbf{\Large Access time : 50ns}
              \\
              \textbf{\Large Loose data when not powered}
            \end{tabular}
          };
        }
        
        \onslide<3> {
          \node[color=red] (fsc) at ($(disk.east) +(4, 0)$) {
            \begin{tabular}{l}
              \textbf{\Large Access time : 100000 ns}
              \\
              \textbf{\Large Retains data when not powered}
            \end{tabular}
          };
        }
        
        \onslide<4> {What if it existed non-volatile RAM ?}
        
        \onslide<5> {
          \node[cpu, text width=2cm, anchor=north, minimum height=1cm] (dram) at ($(cpu.south) +(0, -0.5)$) {DRAM}; 
          \node[cpu, text width=3.5cm, anchor=north, minimum height=1.5cm, opacity=0.9, fill=blue!15] (nvram) at ($(dram.south) +(0, -0.8)$) {\textbf{NVRAM}}; 
          \draw[->] (dram.south) -- (nvram.north);
        }
        
        \onslide<6> {
          \node[cpu, text width=2cm, anchor=north, minimum height=1cm] (dram) at ($(cpu.south east) +(0, -0.5)$) {DRAM}; 
          \node[cpu, text width=2cm, anchor=north, minimum height=1cm, opacity=0.9, fill=blue!15] (nvram) at ($(cpu.south west) +(0, -0.5)$) {NVRAM}; 
          \draw[<-] (dram.130) -- (dram.130 |- cpu.south);
          \draw[<-] (nvram.50) -- (nvram.50 |- cpu.south);
          \draw[->] (nvram.320) -- (nvram.320 |- disk.north);
          \draw[->] (dram.220) -- (dram.220 |- disk.north);

        }

        \onslide<7> {
          \node[rectangle, minimum width=3cm, minimum height=2cm, fill=red!15, draw, line width=1pt, opacity=0.95] (newcpu) at ($(cpu) +(0, 0.2)$) {};
          \node[rectangle, minimum width=1cm, draw] (l1) at (newcpu) {\small L1 Cache};
          \node[rectangle, minimum width=1cm, anchor=north, draw, fill=blue!15] (l2) at ($(l1.south) +(0, -0.1)$) {\small L2 Cache};
          \node[rectangle, minimum width=0.2cm, minimum height=0.2cm, anchor=south west, draw, fill=blue!15] (r1) at ($(l1.north west) +(0, 0.2)$) {};
          \node[rectangle, minimum width=0.2cm, minimum height=0.2cm, anchor=west, draw, fill=blue!15] (r2) at ($(r1.east) +(0.2, 0)$) {};
          \node[rectangle, minimum width=0.2cm, minimum height=0.2cm, anchor=west, draw, fill=blue!15] (r3) at ($(r2.east) +(0.2, 0)$) {};

        }

        \onslide<8> {
          \node[fill=blue!15, draw, minimum height=4cm, minimum width=4cm, opacity=0.8] at ($(disk.north) +(0, -0.2) $) {\textbf{NVRAM}};
        }
        
        % \draw[->, line width=1pt, color=red] (ramc) -- (dram.east);
        % \draw[->, line width=1pt, color=red] (fsc) -- (disk.east);
        
      \end{tikzpicture}
    }
  \end{figure}  
\end{frame}  
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{Emerging memory technologies}
    \includegraphics[width=11cm]{figures/memory_types.png}
\end{frame} 


%==============================================================================%
\section{SYTARE}
%==============================================================================%
\begin{frame}
    \frametitle{Applications de l'IoT}

    \begin{center}
        \centering\includegraphics[width=.9\paperwidth]{figures/JLLx14-iot-applications.pdf}
    \end{center}
    {\footnotesize Jayakumar, Lee, Lee,  Raha, Kim, \& Raghunathan. \textbf{Powering the Internet of Things}. In \emph{ISPLED'14: International Symposium on Low Power Electronics and Design}, 2014\par}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{Différentes échelles de l'IoT}
    \vspace{-1cm}
    \begin{center}
      \centering\includegraphics[width=.5\paperwidth]{figures/JLLx14-iot-taxonomy.pdf}
    \end{center}
    {\scriptsize Jayakumar, Lee, Lee,  Raha, Kim, \& Raghunathan. \textbf{Powering the Internet of Things}. In \emph{ISPLED'14: International Symposium on Low Power Electronics and Design}, 2014\par}

    \bigskip
    \begin{center}
    faible conso $+$ beaucoup de devices $+$ sans batterie \\
    $\rightarrow$ opportunités d'utilisation des NVRAMs
    \end{center}

\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{Transiently Powered Systems}

    \begin{columns}
        \begin{column}{.333\textwidth}
            \begin{center}
                \includegraphics[width=4cm]{figures/nike_hyperadapt.jpg}\\
                \textit{\small{wearable computing}}
            \end{center}
        \end{column}
        \begin{column}{.333\textwidth}
            \begin{center}
                \includegraphics[width=3.65cm]{figures/carte_tcl.jpg}\\
                \textit{\small{smart cards}}
            \end{center}
        \end{column}
        \begin{column}{.333\textwidth}
            \begin{center}
                \includegraphics[width=4cm]{figures/tag_rfid.jpg}\\
                \textit{\small{RFID}}
            \end{center}
        \end{column}
    \end{columns}
    
    \bigskip
    Systèmes à alimentation interittente:
    \begin{itemize}
        \item Pas de batteries ou de source de courant stable
        \item récolte son énergie dans l'environnement
    \end{itemize}
    \bigskip
    \begin{center}
        \textbf{Services limités, développements spécifiques et couteux}
    \end{center}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{SYTARE : augmenter les capacités des TPS}

    Approche du problème au niveau système d'exploitation
    \begin{itemize}
        \item sauvegarde et restauration de l'exécution
        \item alimentation intermittente abstraite pour la couche applicative
        \item persistence de l'état des périhpériques
    \end{itemize}

    \bigskip

    "nouveaux trucs possibles sur les bestioles en question"
    \begin{itemize}
        \item possibilité d'executer des programmes complexes et/ou longs
        \item développement "transparent" des applications
        \item utilisation des périphériques robuste aux coupures
    \end{itemize}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{SYTARE : Fonctionnement}
    \begin{columns}
        \begin{column}{.43\paperwidth}
            \vspace{-1.8cm}\\
            Our approach:
            \begin{itemize}
                \item Anticipate power failures
                \item Save state to non-volatile memory
                \item Restore state at next boot
                \item Retry interrupted HW actions
            \end{itemize}
        \end{column}
        \begin{column}{.01\paperwidth}\end{column}
        \begin{column}{.48\paperwidth}
            \centering\includegraphics[width=.50\paperwidth]{figures/TPC-RAM-NVRAM-Comp-Peripherals.pdf}
        \end{column}
        \begin{column}{.01\paperwidth}\end{column}
    \end{columns}
    \vspace{-1.5cm}
    \includegraphics[width=.75\paperwidth]{figures/TPC-checkpointing.pdf}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
    \frametitle{SYTARE : Démo}
    \includegraphics[width=.75\paperwidth]{figures/sytare_one_discharge_legend.png}
\end{frame}



%==============================================================================%
% BACKUP SLIDES
%==============================================================================%
\begin{frame}
\frametitle{Bibliography}
  % \frametitle{Bibliography}
  \centering

  \scalebox{.85} {  
    \begin{tikzpicture}[small mindmap,concept color=white]
      \node [concept] {Bibliography}
      child[concept color=blue!15, grow=350] {
        node[concept] (archi) {Architecture}
        child{
          node[concept]{Cache}
        }
        child {
          node[concept]{Interconnect}
        }
        child {
          node[concept]{Other}          
        }
      }
      child[concept color=green!15, grow=200] {
        node[concept] {Hard + soft}
      }
      child[concept color=yellow!15, grow=270] {
        node[concept] (hardsoft) {Software}
        child{node[concept]{Mass storage}}
        child{node[concept]{Wear-levelling}}
        child{node[concept]{Orthogonal persistence}
          child[concept color=hugebib]{node[concept]{Check\-pointing}}
          child[concept color=bigbib]{node[concept]{NV heaps}}
          child[concept color=tinybib]{node[concept]{Whole System Persistence}}
        }
      };
%      \node[annotation] at (archi) {Essentially, modification of the memory hierarchy};
%      \node[annotation, right] at (hardsoft.south east) {XXXXX};      
    \end{tikzpicture}
  }
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}
\frametitle{Team project}
  % \frametitle{Team project}

\newcommand{\overview}{
  \scalebox{0.6} {

  \begin{tikzpicture}
    
    \tikzset{
      >=stealth',
      stdbox/.style={
        rectangle,
        line width=1pt,
        rounded corners,
        draw=black,
        text width=5em,
        minimum height=2em,
        text centered},
      partbox/.style={
        line width=1pt,
        text width=5em,
        color=gray!100,
        minimum height=2em,
        text centered},
      dbEdge/.style={
        ->,
        double,
        draw=black,
        line width=1pt,
        shorten <=2pt,
        shorten >=2pt},
      stdEdge/.style={
        ->,
        draw=black,
        line width=1pt,
        shorten <=2pt,
        shorten >=2pt},
      partEdge/.style={
        ->,
        dashed,
        draw=gray,
        line width=1pt,
        shorten <=2pt,
        shorten >=2pt}
    }
    
    %% ------------ MCU at the center ---------------
    \node[stdbox] (cpu) at (0, 0) {CPU};
    \node[stdbox, anchor=west] (mem) at ($(cpu.east) + (0.6, 0)$) {NVRAM};
    
    \node[anchor=north] (mcutext) at ($(cpu.330) + (0, -0.05)$) {NVRAM-based micro architecture};
    \begin{pgfonlayer}{background}
      \node[stdbox, draw=gray!50, fill=gray!14, inner sep=0.2cm, fit=(mem) (mcutext.west) (cpu)](mcu){};
    \end{pgfonlayer}

    %% ------------ Sensors on the right ---------------
    \node[stdbox, anchor=west] (radio) at ($(mcu.east) + (1.5, 0)$) {Radio};
    \node[stdbox, anchor=west] (sensors) at ($(mcu.east) + (1.5, -1)$) {Sensors};
    \node[stdbox, anchor=west] (camera) at ($(mcu.east) + (1.5, -2)$) {Low-res camera};
    
    %% ------------ Energy sources on the left ---------------
    \node[stdbox, anchor=east] (energy) at ($(mcu.west) + (-1.5, 0)$) {Energy buffer};
    
    \node[stdbox, align=left, anchor=east] (harv1) at ($(energy.west) + (-1.5, 0)$) {Harvester\\{\footnotesize Light}};
    \node[stdbox, align=left, anchor=north] (harv2) at ($(harv1.south) + (0.2, -0.2)$) {Harvester\\{\footnotesize Warmth}};
    \node[stdbox, align=left, anchor=north] (harv3) at ($(energy.south) + (-1, -1)$) {Harvester\\{\footnotesize Radio waves}};

    \node[anchor=north, align=left] (harvtext) at ($(energy.335) + (0, -0.04)$) {Energy\\harvesting};
    \begin{pgfonlayer}{background}
      \node[stdbox, draw=gray!50, fill=gray!14, inner sep=0.2cm, fit=(harv1) (harv2) (harv3) (energy) (harvtext)](energybox){};
    \end{pgfonlayer}

    %% ------------ OS above MCU ---------------
    \coordinate (oscenter) at ($(mcu.north) + (0,1)$);
    \coordinate (oscenterw) at ($(mcu.north west) + (0.2, 1)$);
    \coordinate (oscentere) at ($(mcu.north east) + (-0.2,1)$);
    \node[stdbox, anchor=south east] (mm) at ($(oscenter) + (-0.2, 0)$) {Memory Manager};
    \node[stdbox, anchor=south west] (em) at ($(oscenter.east) + (0.2, 0)$) {Energy Manager};

    \node[anchor=north] (ostext) at ($(mm.330) + (0, -0.05)$) {Operating system};
    \begin{pgfonlayer}{background}
      \node[stdbox, draw=gray!50, fill=gray!14, inner sep=0.2cm, fit=(oscenter) (oscenterw) (oscentere) (em) (mm) (ostext.west)](os){};
    \end{pgfonlayer}

    \coordinate (hwswl) at ($(harv1.north) + (0,0.5)$);
    \coordinate (hwswr) at ($(radio.north) + (0,0.5)$);
    \draw [dashed, line width=2pt] (hwswl) -- (hwswr);
    

    %% ------------ Compiler above OS ---------------
    \node[stdbox, anchor=south, draw=gray!50, fill=gray!14] (comp) at ($(os.north) + (0,0.5)$) {Compiler};

    %% ------------ Apps above compiler ---------------
    \coordinate (appcenter) at ($(comp.north) + (0,0.5)$);
    \node[stdbox, anchor=south] (sa) at (appcenter) {Sensing application};
    \node[stdbox, anchor=south west] (va) at ($(sa.south east) + (0.2, 0)$) {Video decoding application};
    \node[stdbox, anchor=south east] (ca) at ($(sa.south west) + (-0.2, 0)$) {Crypto application};

    
    %% ------------ Connecting boxes -------------
    \draw[stdEdge] (mcu.0) -- node[above=0.4cm](hwtext){HW} (radio.west);
    \draw[stdEdge] (mcu.357) -- (sensors.west);
    \draw[stdEdge] (camera.west) -- (mcu.352);
    \node at ($(hwtext) + (0, 0.7)$){SW};

    \draw[stdEdge, opacity=0.8] (appcenter) -- (os.north);

    \draw[dbEdge] (harv1.east) -- (energy.180);

    \draw[dbEdge] (harv2.20) -- (energy.200);
    \draw[dbEdge] (harv3.north) -- (energy.220);
    
    \draw[dbEdge] (energy.east) -- (mcu.west);
    
  \end{tikzpicture}
}
} 



\begin{figure}
  \centering
  \overview{}
  \caption{Overview of proposal}
  \label{fig:overview}
\end{figure}

\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}[noframenumbering]
\frametitle{prototype architecture}
    \vspace{-1cm}
    \includegraphics[width=\textwidth]{figures/TPC-architecture.pdf}
    \bigskip
    \begin{columns}
        \begin{column}{.40\paperwidth}
            Typical hardware :
            \begin{itemize}
                \item small microcontroller
                \item few kilobytes of RAM
                \item in place code execution
                \item serial port
            \end{itemize}
        \end{column}
        \begin{column}{.60\paperwidth}
            \includegraphics[width=0.95\textwidth]{figures/Sytare-exp-platform.jpg}
        \end{column}
    \end{columns}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}[noframenumbering]
\frametitle{system boot}
      \centering\includegraphics[width=.9\textwidth]{figures/boot_diagram.pdf}
\end{frame}
%------------------------------------------------------------------------------%
\begin{frame}[noframenumbering]
\frametitle{complex syscall sequence diagram}
      \centering\includegraphics[width=.9\textwidth]{figures/complex_syscall.pdf}
\end{frame}

%==============================================================================%
\end{document}
%==============================================================================%
%==============================================================================%
