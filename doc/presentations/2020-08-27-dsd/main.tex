\documentclass{beamer}

\mode<presentation>{
\usetheme{Madrid}
}

\usepackage{amsmath}
\usepackage{subfig}
\usepackage{listings}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{width=6.5cm,compat=1.12}

\definecolor{darkgreen}{rgb}{0, .5, 0}
\definecolor{grey}{rgb}{.7, .7, .7}

\lstset{
  language=C,
  keywordstyle=\color{blue}\bfseries,
  basicstyle=\tiny, % print whole listing small
%  keywordstyle=\color{black}\bfseries,% underlined bold black keywords
%  identifierstyle=, % nothing happens
  commentstyle=\color{red!50!black},
  stringstyle=\ttfamily, % typewriter type for strings
  showstringspaces=false, % no special string spaces
  emph={IS_DIRTY,REGION_OFFSET,CLEAR_DIRTY,SET_DIRTY,REGION_OF},
  emphstyle={\color{darkgreen}\bfseries\underbar}
}

\title[MPU-based Incremental Checkpointing]{MPU-based Incremental Checkpointing for Transiently-Powered Systems}

\author[Gautier Berthou]{\underline{Gautier~Berthou} \and Kevin~Marquet \and Tanguy~Risset \and Guillaume~Salagnac}
\date[DSD'20]{Digital System Design, August 27th, 2020}

\begin{document}

\defverbatim[colored]\snippetfullcopy{
    \begin{lstlisting}
void save(void) {
  dma_memcpy(nv_mem, v_mem, memory_size);
}

void restore(void) {
  dma_memcpy(v_mem, nv_mem, memory_size);
}
    \end{lstlisting}
}

\defverbatim[colored]\snippetincrementalA{
\begin{lstlisting}
static flags_t dirty;

void save(void) {
  for(size_t i = 0; i < N_reg; ++i)
    if(IS_DIRTY(dirty, i)) {
      size_t offset = REGION_OFFSET(i);
      dma_memcpy(nv_mem+offset, v_mem+offset,
        region_size);
    }
}

void restore(void) {
  dma_memcpy(v_mem, nv_mem, memory_size);
}
\end{lstlisting}
}
\defverbatim[colored]\snippetincrementalB{
    \begin{lstlisting}
void mpu_init(void) {
  for(size_t i = 0; i < N_reg; ++i)
    mpu_lock(i);
  CLEAR_DIRTY(dirty, ALL_REGIONS);
}

void interrupt mpu_interrupt_handler(void) {
  size_t address = FAULTING_ADDRESS;
  size_t region = REGION_OF(address);
  mpu_unlock(region);
  SET_DIRTY(dirty, region);
}
    \end{lstlisting}
}

\begin{frame}
    \titlepage
\end{frame}

\section{Introduction}
\begin{frame}
    \frametitle{Context: Checkpointing}

    \begin{overlayarea}{\textwidth}{2.5cm}
        \only<1>{Low-power platforms for \textbf{long-running} applications.
            \begin{itemize}
                \item From a few $\mu$A to dozens mA.
                \item Scarce energy (\textit{e.g.}, harvested from environment).
                \item Limited time and energy budgets.
            \end{itemize}}\only<2>{Checkpointing in a nutshell.
            \begin{itemize}
                \item {\setlength{\fboxsep}{2pt}\colorbox{blue!50!white}{Restore}} on boot.
                \item {\setlength{\fboxsep}{2pt}\colorbox{yellow!90!white}{Save}} upon power loss detection (might {\setlength{\fboxsep}{2pt}\colorbox{red!60!white}{fail}} as well).
                \item Copy big amounts of data: \texttt{.bss}, \texttt{.data}, \textit{etc.} from/into a \textbf{non-volatile} storage.
            \end{itemize}}
    \end{overlayarea}

    \begin{center}
        \only<1>{\input{figures/tps}}\only<2>{\input{figures/tps-checkpoint}}
    \end{center}

    \begin{block}{}
        Need \only<2>{\textbf{optimized} }checkpointing mechanisms.
    \end{block}
\end{frame}

\begin{frame}[noframenumbering]
    \frametitle{Context: Checkpointing}

    Checkpointing contents
    \begin{itemize}
        \item \texttt{.bss} and \texttt{.data}.
        \item stack.
        \item CPU registers.
    \end{itemize}

    \bigskip

    Out of scope
    \begin{itemize}
        \item peripherals.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Problem Statement}

    How to copy \textbf{only modified data}, \textbf{without any modification to application code} and \textbf{within existing, widespread architectures}?

    \bigskip

    Contributions
    \begin{itemize}
        \item Incremental checkpointing proposal.
        \item Models of full-copy and incremental checkpointing.
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Outline}
    \tableofcontents
\end{frame}

\section{Incremental Checkpointing}
\begin{frame}
    \frametitle{Full-Copy Checkpointing}
    \begin{center}
        \input{figures/full-checkpointing}
    \end{center}

    \begin{overlayarea}{\textwidth}{6cm}
        \begin{block}{}
            Copy large amounts of data, including data that \textbf{were not modified since the last checkpoint}.
        \end{block}
    \end{overlayarea}
\end{frame}

\begin{frame}
    \frametitle{Incremental Checkpointing}
    \begin{center}
        \input{figures/incremental-checkpointing}
    \end{center}


    \begin{overlayarea}{\textwidth}{6cm}
        \only<2>{Constraint: data alignment.

        \bigskip

        \begin{block}{}
            Increased \textbf{complexity} but decreased \textbf{copy length}.
        \end{block}

        Alternatives to MPU, out of scope
        \begin{itemize}
            \item Checksums.
            \item Dedicated hardware.
        \end{itemize}}\only<1>{\begin{columns}
                \begin{column}{.5\textwidth}
                    Incremental checkpointing
                    \begin{itemize}
                        \item Split data range into regions.
                        \item Non-volatile memory holds a full copy of the volatile memory.
                        \item Save only modified regions.
                    \end{itemize}
                \end{column}
                \begin{column}{.5\textwidth}
                    MPU-specific
                    \begin{itemize}
                        \item Map data regions onto MPU regions.
                        \item On boot, write-lock all regions after restoring data.
                        \item On write exception, unlock region and mark it to be saved.
                    \end{itemize}
                \end{column}
        \end{columns}}
    \end{overlayarea}
\end{frame}

\section{Checkpointing Models}
\begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}
    \frametitle{Model of Checkpointing by Full-Copy}
    \begin{overlayarea}{\textwidth}{2cm}
        \begin{block}{}
            Copy the \textbf{whole} data range (\texttt{.bss}, \texttt{.data}) using optimized DMA.
        \end{block}
    \end{overlayarea}

    \begin{overlayarea}{\textwidth}{6cm}
        \begin{table}
              \scriptsize
              \centering
              %\caption{Model parameters and their default values.}
              \begin{tabular}[h]{| p{2cm} | p{5.5cm} | p{1.5cm} | %p{2cm}|
                  }
                  \hline
                  \textbf{Symbol} & \textbf{Description}                                                                                  & \textbf{Unit} %& \textbf{Typical value}
                  \\\hline
                  $S_{words}$       & Size of RAM used by application                                                               & Word % & $2^{13}$
                  \\\hline
                  $f_{DMA}$         & DMA bandwidth                                                                                         & Word$\times$s$^{-1}$ % & $8 \times 10^{6}$ 
                  \\\hline
                  $P_{plat}$    & Power drawn by the whole platform                                           & W % & $1.65 \times 10^{-2}$
                  \\\hline
            \end{tabular}
        \end{table}

        \begin{equation}
              E_\text{copy} = \frac{S_{words}}{f_{DMA}} \times P_{plat}
        \end{equation}
    \end{overlayarea}
\end{frame}

\begin{frame}
    \frametitle{Model of MPU-based Incremental Checkpointing}
    \begin{overlayarea}{\textwidth}{2cm}
        \begin{block}{}
            Copy \textbf{only} regions formerly marked as \textbf{dirty} upon MPU \textbf{access mismatch exception}.
        \end{block}
    \end{overlayarea}

    \begin{overlayarea}{\textwidth}{6cm}
        \begin{table}
              \scriptsize
              \centering
              %\caption{Model parameters and their default values, continued.}
              \begin{tabular}[h]{| p{2cm} | p{5.5cm} | p{1.5cm} | %p{2cm}|
                  }
                  \hline
                  \textbf{Symbol} & \textbf{Description}                                                                                  & \textbf{Unit} %& \textbf{Typical value} 
                  \\\hline
                  $\overline{N_\text{dirty}}$          & Average amount of dirty regions                                    & - %& 0.1
                  \\\hline
                  $E_\text{once}$    & Energy to check regions dirtyness                                 & J %& $3 \times 10^{-6}$
                  \\\hline
                  $E_\text{per region}$   & MPU interrupt handler energy consumption                                                         & J %&   $5 \times 10^{-6}$
                  \\\hline
            \end{tabular}
        \end{table}

        \begin{equation}
            E_\text{incremental} = E_\text{once} + \overline{N_\text{dirty}} \times E_\text{per region}
        \end{equation}
    \end{overlayarea}
\end{frame}

\begin{frame}
    \frametitle{Required Architecture}

    MPU requirements
    \begin{itemize}
        \item Sufficient amount of regions.
        \item Covers the whole volatile RAM address range.
        \item Returns from interrupt \textit{at} the faulting instruction.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Comparison between both models}
    \only<1>{\begin{figure}
        \begin{columns}
            \begin{column}{.5\textwidth}
                \centering
                \input{figures/fig3b}
            \end{column}
            \begin{column}{.5\textwidth}
                \vspace{-2.8em}
                \centering
                \input{figures/fig4b}
            \end{column}
        \end{columns}
        %\caption{Theoretical values of $E_\text{copy}$ and $E_\text{incremental}$ wrt $S_{words}$ and $N_{reg}$.}
    \end{figure}}\only<2>{\begin{center}
        $\forall S_{words} > S_{min}, E_\text{incremental}(S_{words}) < E_\text{copy}(S_{words})$
    \end{center}
    \begin{figure}
        \subfloat{\centering\scalebox{0.6}{\input{figures/fig5a}}}
        \subfloat{\centering\scalebox{0.6}{\input{figures/fig5b}}}
        \subfloat{\centering\scalebox{0.6}{\input{figures/fig5c}}}
        \subfloat{\centering\scalebox{0.6}{\input{figures/fig5d}}}
        %\caption{Impact of $\alpha$, $P_{plat}$, $t_{int}$ and $P_{MPU}$ on the minimal amount of RAM words to make the incremental checkpointing worthwhile ($S_{min}$).}
    \end{figure}
    \begin{block}{}
        $S_{min}$ is always fairly low, $\le$ 16~kB.
    \end{block}}
\end{frame}

\begin{frame}
    \frametitle{Model Limitations}
    $\overline{N_\text{dirty}}$ is considered constant, but depends on
    \begin{itemize}
        \item Time/energy budget.
        \item Application properties.
    \end{itemize}

    \bigskip

    $P_{plat}$ is considered constant, but depends on
    \begin{itemize}
        \item The moment of the imminent power loss interrupt.
        \item Application properties.
    \end{itemize}

\end{frame}

\section{Implementation}
\begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}
    \frametitle{Implementation Setup}

    Cycle-accurate simulator\footnote{\url{https://github.com/gberthou/archc-msp430x/tree/wider-memory}} for MSP430FR MCU family
    \begin{itemize}
        \item Modified MSP430FR5739 architecture: \textbf{20~kB volatile RAM} and \textbf{40~kB non-volatile RAM}.
        \item Added support for an ARM-like MPU simulation.
        \item Up to 16 regions, dirtiness implemented as bitfield flags.
    \end{itemize}

    \bigskip

    Applications
    \begin{itemize}
        \item 3 applications with different values of $S_{words}$ and $N_{reg}$: 125 kernels.
        \item Quicksort / RSA / Wireless-Sensor-Network.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Simulation Results}
    \centering
    \begin{figure}
        \subfloat{\centering\scalebox{.6}{\input{figures/figure2_quicksort}}}
        \subfloat{\centering\scalebox{.6}{\input{figures/figure3_quicksort}}}
        \caption{Quicksort application.}
    \end{figure}
    \vspace{-2em}
    \begin{figure}
        \begin{columns}
            \begin{column}{.5\textwidth}
                \centering\scalebox{.6}{\input{figures/figure2_complete_scenario}}
            \end{column}
            \begin{column}{.5\textwidth}
                \centering\scalebox{.6}{\input{figures/figure3_complete_scenario}}
                \vspace{1em}
            \end{column}
        \end{columns}
        \caption{Wireless-Sensor-Network application.}
    \end{figure}
\end{frame}

\begin{frame}
    \frametitle{Other Implementation}

    This work is compatible with actual MPUs
    \begin{itemize}
        \item All ARM Cortex-M MPUs (M0+, M3, M4, M7, \textit{etc.}).
        \item 8 regions and 8 subregions per region.
        \item ARM's ABI makes returning from MPU fault easy.
    \end{itemize}

    \bigskip

    The MPU-based incremental checkpointing technique has been implemented within sytARM\footnote{\url{https://gitlab.inria.fr/gabertho/sytarm}} and runs on the ARMorik\footnote{\url{https://github.com/gberthou/armorik}} platform.
\end{frame}

\section{Conclusion}
\begin{frame}
    \frametitle{Outline}
    \tableofcontents[currentsection]
\end{frame}

\begin{frame}
    \frametitle{Conclusion}

    \begin{itemize}
        \setlength\itemsep{1.2em}
        \item On boot, restore the whole memory.
        \item On power loss, save only modified memory regions.
        \item Always better than a full copy.
        \item Theoretically more efficient than manually computing checksums.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Perspectives}

    Finer model
    \begin{itemize}
        \item Add energy budget to the model.
        \item Application-dependent heuristics ($\overline{N_\text{dirty}}$, $P_{plat}$).
    \end{itemize}

    \bigskip

    Further optimizations
    \begin{itemize}
        \item Read-lock regions on boot and \textbf{restore} regions from NVRAM \textbf{only when needed}.
    \end{itemize}
\end{frame}

\begin{frame}
    \Huge
    \centering
    Thanks!
\end{frame}

\begin{frame}[noframenumbering]
    \frametitle{Incremental Checkpointing equations}
    \begin{align}
        E_\text{once} &= t_{overhead} \times (P_{CPU} + P_{plat})
        \\
        E_\text{per region} &= P_{plat} \times \left[\lceil \frac{S_{words}}{N_{reg}} \rceil \times \frac{1}{f_{DMA}} + t_{int} \times (1 + \frac{P_{CPU}}{P_{plat}})\right]
    \end{align}
\end{frame}

\begin{frame}[noframenumbering]
    \frametitle{Code Insights: Full-copy}
    \begin{block}{}
        \snippetfullcopy
    \end{block}
\end{frame}

\begin{frame}[noframenumbering]
    \frametitle{Code Insights: MPU}
    \begin{block}{}
        \begin{columns}
            \begin{column}{.5\textwidth}
                \begin{overlayarea}{0.5\textwidth}{4cm}
                    \snippetincrementalA
                \end{overlayarea}
            \end{column}
            \begin{column}{.5\textwidth}
                \begin{overlayarea}{0.5\textwidth}{4cm}
                    \snippetincrementalB
                \end{overlayarea}
            \end{column}
        \end{columns}
    \end{block}
\end{frame}

\end{document}
