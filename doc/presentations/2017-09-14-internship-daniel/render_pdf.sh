#!/bin/bash

if [ -z "$(which npm)" ]; then
	echo "You need npm to render the slides"
	exit 1
fi

echo "== Checking dependencies"
if [ -z "$(npm list 2>/dev/null | grep -E 'backslide|decktape' )" ]; then
	echo "== Install backslide & decktape"
	npm install backslide decktape
fi

echo "== Start backslide"
`npm bin`/bs serve --skip-open &
BS_PID=$!

sleep 5

echo "== Render PDF"
`npm bin`/decktape remark http://localhost:4100/presentation.html presentation.pdf --no-sandbox

kill $BS_PID
