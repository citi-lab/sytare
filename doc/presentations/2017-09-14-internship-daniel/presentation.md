title:  Sytare
author: Daniel Krebs
date: 2017-09-14
class: animation-fade
layout: true

<!-- This slide will serve as the base layout for all your slides -->
.bottom-bar[
  .col-6[
  {{title}} | {{author}} | {{date}}]
  .col-6[
   .small[[Table of Contents](#toc)]
  ]
]

---
name: frontpage
class: impact

# {{title}}
## my contributions

---
name: toc

# My work on Sytare

.left-column[
## Done

- [CMake Build System](#cmake)
- [Continous Integration](#ci)
- [Schlumpf](#schlumpf)
- [Tools](#tools)
]

.right-column[
## Not yet merged / wip

- [register/save/restore Driver Interface](#driver-interface)
- [Linker Script Memory Allocation](#ld-script)
- [Inlinable GPIO driver](#gpio)
- [UART driver](#uart)
- [Multi-MCU support](#multi-mcu)
- [RIOT PoC](#riot)
]

---
name: cmake

# CMake

.col-9[
- typical usage:
  - `mkdir build ; cd build ; cmake .. ; make`
- compile *core* and *drivers* to static library
- combine both to `INTERFACE` library *sytare*
- reduce minimum application *CMakeLists.txt* to:

.col-11[
```cmake
include(../../cmake/Sytare-Application.cmake)
project(app_leds C)
add_executable(app_leds main.c)
```
]
]

.col-3[
```
*src/
├── apps
├── cmake
├── CMakeLists.txt
├── core
├── drivers
├── include
└── tests
```
]

---
name: cmake-details

# CMake details

.col-7[
- main script is *Sytare.cmake*
  - included everywhere in kernel
  - set global compiler/linker flags
  - set MCU
- *Sytare-Application.cmake* to include from apps
  - wrapper for *Sytare.cmake*
  - link application against Sytare
- *Sytare-Tests.cmake* for CI tests
- detect toolchain via *msp430-toolchain.cmake*
]

.col-5[
```
*src/cmake/
├── msp430-toolchain.cmake
├── Sytare-Application.cmake
├── Sytare.cmake
├── Sytare-Tests.cmake
└── Utils.cmake
```
]

---
name: ci

# CI - Continous integration

.col-9[
- run automatically via GitLab CI (see `.gitlab-ci.yml`)
- compile all *apps* and *tests*
- run *tests*
- software tests: `sections`
  - check if important symbols are present in the their section
- hardware tests: `dma`, `checkpoint`
  - `dma`: copy/set memory via DMA
  - `checkpoint`: check if `stats_success_count >= 2`
]
.col-3[
```
*src/tests/
├── checkpoint
├── dma
├── sections
└── run_tests.sh
```
]

---

name: ci-hw

# CI - hardware tests

.col-8[
- PC programs MSP430 via *mspdebug*
- STM32 generates .green[PWM] for transient power supply
- execute for 1 second
- check memory via *mspdebug*
- *mspdebug* works despite TPS (so lucky!)

<br/>
<br/>
<br/>
**Integration of Schlumpf should be quite straight-forward!**
]

.col-4[
.responsive[![CI hardware](img/ci-hw.png)]
]

---
name: ci-new-hw-test

# CI - new hardware test

- copy a simple one (e.g. `checkpoint`) and extend
- all tests in `src/tests/` will be run (see `run_tests.sh`)
- test memory content using `.expect` file (see `tools/check_test.sh`)
  - Python(-like) expressions evaluated to `Bool`
  - variable is resolved from ELF-file and fetched via *mspdebug*


.left-column[
*checkpoint.expect*:
```
stats_boot_count > 2
stats_success_count >= 2
```
]

.right-column[
*dma.expect*:
```
dst_memset == 424242424242424242424242..
dst_memcpy == 53797461726553797461726500
```
]

---
name: ci-new-sw-test

# CI - new software test

- `run_tests.sh` will just do `cmake .. ; make tests` for each test
- create a target `tests` and do whatever you want

<br/>
*sections/CMakeLists.txt*:
```cmake
add_custom_target(tests
    DEPENDS ${PROJECT_NAME}
    COMMAND
        ./check_sections.sh $<TARGET_FILE:${PROJECT_NAME}>
    WORKING_DIRECTORY
        ${CMAKE_CURRENT_SOURCE_DIR}
    )
```

---
name: schlumpf

# Schlumpf

.col-9[
- application terminates when `main()` returns
- raise GPIO pin to signalize termination
- measure baseline execution time of application
- sweep T_on parameter space: [0, a * baseline], a > 1
- Lines of code (C++):
  - **pc**: 671 lines
  - **stm32**: 692 lines
  - **common**: 326 lines
]

.col-3.top[
.responsive[![Schlumpf](img/schlumpf.png)]
]

---
name: schlumpf-results

# Schlumpf - Example results

.left-column[
```
== Analysis


Execution baseline: 996.36 ms
Shortest lifecycle: 4.02 ms @ 49.9%


Linear interpolation:
     50% @ 4.05 ms
     60% @ 5.11 ms
     70% @ 6.87 ms
     80% @ 10.36 ms
     90% @ 21.27 ms
  99.32% @ Baseline
```
]

.right-column[
```
Hyperbola fitting:
  Curvature:     -201.92  (stdev -0.2%)
  Max. efficiency: 99.60% (stdev  0.0%)
     25% @ 2.71 ms
     50% @ 4.07 ms
     60% @ 5.10 ms
     70% @ 6.82 ms
     80% @ 10.30 ms
     90% @ 21.03 ms
  99.40% @ Baseline

Outliers:
  None
```
]

---
name: schlumpf-graph

<!-- # Schlumpf - App LEDs -->

.center.responsive[![apps/leds](img/app_leds_efficiency.png)]

---
name: schlumpf-steps

.center.responsive[![efficiency steps](img/schlumpf_efficiency_steps.png)]

---
name: tools

# Tools

- *dump_func.sh*: Disassemble one function in ELF-file
- *gen_empty_syscalls.sh*: generate empty syscalls stubs for nosytare
- *mspdebugger.py et al.*: read MSP430 memory via debugger
- serial helpers:
  - *hexterm*: terminal that formats output to hexadecimal bytes
  - *pcsampler*: dump program counter to serial and find line in source

---
name: driver-interface

# register/save/restore Driver Interface

- drivers register save and/or restore callbacks
- kernel assigns device context memory to driver
- drivers only see pointer to their ctx, kernel handles checkpoints
- drivers handle dirtyness themselves (or don't)

## Why?

- clean driver interface to kernel
- decouple drivers from kernel
  - no include of any driver in kernel code anymore
  - allow better optimization

---
name: ld-script

# Flexible Linker Script

.col-7[
```
.checkpoint0 :
{
 PROVIDE(LINKER_syt_checkpoint_usr_stack_start = .);
 . += LINKER_syt_usr_stack_size;

 PROVIDE(LINKER_syt_checkpoint_usr_data_start = .);
 . += SIZEOF(.usr_data);

 PROVIDE(LINKER_syt_checkpoint_usr_bss_start = .);
 . += SIZEOF(.usr_bss);

 *(.checkpoint)

 PROVIDE(LINKER_syt_checkpoint_dev_ctx_start = .);
 *(.dev_ctx)
 PROVIDE(LINKER_syt_checkpoint_dev_ctx_end = .);

} > NVRAM
```
]

.col-1[<br/>]

.col-4[
```
/* Duplicate checkpoint */
.checkpoint1 :
{
 . += SIZEOF(.checkpoint0);
} > NVRAM
```

- grows with `.usr_data` and `.usr_bss`
- allocates memory for device contexts
- mirror 2nd checkpoint
]

---
name: ld-script2

# Memory allocation in checkpoints

- Access memory by same offset in sections `.checkpoint{0,1}`
- Section `.checkpoint`: contains metadata about checkpoint (like `sp`)
- Section `.dev_ctx`: kernel assigns memory to each driver
  - *drv_register()* internally keeps track of assigned offsets

```c
#define syt_drv_register(save, restore, ctx_size) do
{
    static __attribute__((section(".dev_ctx")))   // allocate
    uint8_t dev_ctx[ctx_size];                    // memory

    __asm__ __volatile__("" :: "r" (&dev_ctx));   // prevent optimization

    int drv_register(drv_save_func_t, drv_restore_func_t, size_t);
    drv_register(save, restore, ctx_size);        // wrap kernel function
} while(0)
```

---
name: gpio

# Inlinable GPIO driver

## Motivation

- GPIO accesses ...
  - happen a lot on embedded systems
  - are time critical
- Sytare syscall wrappers and context handling impose too much overhead

## Solution

- GPIO state is completely mirrored by memory-mapped registers
  - save GPIO state only on power loss by DMA-copying registers
- allow using `P1OUT` et al. directly

---
name: gpio-details

# GPIO driver details

- describe GPIO pins in one line `gpio_t gpio = GPIO(port, pin_num)`
  - compiler can optimize function argument
- define `struct` that can be casted to memory-mapped port registers
  - cleaner code (`gpio.port->out |= gpio.bitmask`)
- introduce new header type `gpio_impl.h`
  - inlined functions can be implemented here
  - allows different GPIO backends (Multi-MCU support, each its own `*_impl.h`)
  - old `gpio.h` only defines (clean) interface, hides internals

---
name: uart

# UART driver

- debugging TPS is challenging with traditional tools
  - GDB doesn't like if target looses power
  - timing?
- simple driver
  - hardcoded baudrates (cf. datasheet)
  - hardcoded UART pins
  - relies on fixed SMCLK
  - save/restore by DMA-copying plain registers
- small and dumb printf
- can also receive (cf. code by William)

---
name: multi-mcu

# Multi-MCU support

.col-9[
- add support for MSP430FR5969 board
  - `cmake .. -DBOARD=fr5969`
- refactor commonalities
  - shared peripheral drivers (DMA, UART, GPIO, COMP)
  - linker script per CPU only defines memory map
- some more items in `src/` 😐
]


.col-3[
```
src/
├── apps
*├── boards
*├── Boards.cmake
├── cmake
├── CMakeLists.txt
├── core
*├── cpu
├── drivers
├── include
*├── ldscripts
└── tests
```
]

.row[**CPU**: "SoC", ISA, memory map, (internal) peripherals]
.row[**Board**: CPU, external peripherals, debugger]

---
name: riot

# RIOT PoC

## What we have

RIOT branch with basic FR5969 support working (UART shell, threads)

.left-column[
## ToDo

- checkpoint allocation and handling
- comparator interrupt
- driver persistency (save/restore)
- syscall wrapping
- integrate with Sytare
]

.right-column[
## Ideas

- like in Sytare (mostly copy-paste)
- like in Sytare (mostly copy-paste)
- implement manually for needed drivers
- GCC `-finstrument-functions`
- compile RIOT to static lib, link Sytare?
  - treat RIOT as user program
]

---
name: end
class: impact

# the end

## questions?
