\input{figures/radio-send-128-coordinates}
\documentclass{beamer}

\mode<presentation>{
\usetheme{Madrid}
}

\usepackage{multirow}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{width=6.5cm,compat=1.12}
\usetikzlibrary{calc,patterns}

\tikzset{
  state/.style={
    rectangle,
    line width=1pt,
    draw=black,
    text centered,
    inner sep=1pt}
}

\title[Power Consumption Evaluation]{Accurate Power Consumption Evaluation for Peripherals in Ultra Low-Power embedded systems}

\author[Gautier Berthou]{\underline{Gautier~Berthou} \and Kevin~Marquet \and Tanguy~Risset \and Guillaume~Salagnac}
\date[GIoTS'20]{Global IoT Summit, June 3rd, 2020}

\begin{document}

\defverbatim[colored]\snippet{
    \begin{block}{Sample code}
    \begin{lstlisting}[language=C]
void sense_and_send() {
    unsigned int data = adc_sense();
    unsigned int temperature = to_celsius(data);
    radio_send(&temperature, sizeof(temperature));
}
    \end{lstlisting}
    \end{block}
}

\defverbatim[colored]\lstWCEC{
    \begin{block}{WCEC}
    \begin{lstlisting}[language=C]
for(size_t i = 0; i < 3; ++i) {
    while(!(ADC10IFG & ADC10IFG0)); /* Infinite? */
    buf[i] = (ADC10MEM0 >> 6);
}
    \end{lstlisting}
    \end{block}
}

\begin{frame}
    \titlepage
\end{frame}

\section{Introduction}
\begin{frame}
    \frametitle{Context}

    Low-power platforms
    \begin{itemize}
        \item From a few $\mu$A to dozens mA.
        \item Scarce energy (\textit{e.g.}, harvested from environment).
        \item Limited time and energy budgets.
    \end{itemize}

    \begin{center}
        \input{figures/tps}
    \end{center}

    \vspace{-1em}

    \begin{block}{}
        Need energy-aware mechanisms.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Problem statement}
    How to statically estimate the energy consumption of some piece of code \textbf{involving peripherals}?

    \bigskip

    \snippet
\end{frame}

\begin{frame}
    \frametitle{Roadmap}
    \centering
    \input{figures/roadmap}
\end{frame}

\section{Power model}
\begin{frame}
    \frametitle{Roadmap}
    \centering
    \input{figures/roadmap2}
\end{frame}

\begin{frame}[t]
    \frametitle{Power model - design}

        \begin{minipage}[t]{0.45\textwidth}
            \vspace{0pt}
            \textbf{API-level} Finite State Machine
            \begin{itemize}
                \item State $\equiv$ current.
                \item Transition $\equiv (\Delta t, \Delta E)$.
            \end{itemize}
        \end{minipage}
        \begin{minipage}[t]{0.45\textwidth}
            \vspace{0pt}
            Assumptions
            \begin{itemize}
                \item $V_{CC}$ is constant (voltage regulator).
                \item Peripheral state changes \textbf{only} upon peripheral calls.
            \end{itemize}
        \end{minipage}

    \bigskip

    \centering
    \input{figures/fsm-onoff}
\end{frame}

\begin{frame}
    \frametitle{Realistic driver: radio}

    \centering
    \input{figures/fsm-cc2500}
    \input{figures/driver-calls-cc2500}
\end{frame}

\begin{frame}
    \frametitle{Driver call model example: radio send}

    \centering

    \texttt{send\_packet(128 bytes)}

    \only<1>{\input{figures/radio-send-128-real}}\only<2>{\input{figures/radio-send-128-model}}

    \begin{columns}
        \begin{column}{.3\linewidth}
            $\int$ = 98.8~$\mu$C

            $I_{avg}$ = 17.2~mA
        \end{column}
        \begin{column}{.3\linewidth}
            $\Delta t$ = 5.73~ms

            $\Delta E$ = 325~$\mu$J
        \end{column}
    \end{columns}
\end{frame}

\section{Energy measurements}
\begin{frame}
    \frametitle{Roadmap}
    \centering
    \input{figures/roadmap2}
\end{frame}

\begin{frame}
    \frametitle{Power monitoring device - design}
    \begin{minipage}[t]{0.45\textwidth}
        Purposes
        \begin{itemize}
            \item Simple and low-cost.
            \item Anyone can build it.
            \item Not invasive.
        \end{itemize}
    \end{minipage}
    \begin{minipage}[t]{0.45\textwidth}
        Properties
        \begin{itemize}
            \item Dynamic range: a few $\mu$A to 26~mA.
            \item 170~ksps.
            \item 6~bit resolution.
        \end{itemize}
    \end{minipage}

    \bigskip

    \centering
    \input{figures/circuit}
\end{frame}

\begin{frame}
    \frametitle{Results}

    \begin{equation}
        \Delta E = V_{CC} \times (I_{step} \times \overline{X_{ADC}} + I_{offset}) \times (t_1 - t_0)
    \end{equation}

    Where $$\overline{X_{ADC}} = \frac{1}{n} \times \sum_{i < n} X_{ADC,i}$$

    \bigskip

    \begin{columns}
        \begin{column}{0.45\textwidth}
            Calibration
            \begin{itemize}
                \item $I_{step}$ = 6.4~$\mu$A/step.
                \item $I_{offset}$ = -4.0~$\mu$A.
            \end{itemize}
        \end{column}
        \begin{column}{0.45\textwidth}
            \centering
            \texttt{send\_packet}: $\Delta E = f(\text{length})$
            \input{figures/radio-send-energy}
        \end{column}
    \end{columns}
\end{frame}

\section{Simulation}
\begin{frame}
    \frametitle{Roadmap}
    \centering
    \input{figures/roadmap2}
\end{frame}

\begin{frame}
    \frametitle{Simulator - design}

    \begin{minipage}[t]{0.40\textwidth}
        \textbf{Software: cycle-accurate}
        \begin{itemize}
            \item \texttt{sense\_and\_send}
            \item \texttt{to\_celsius}
        \end{itemize}
    \end{minipage}
    \begin{minipage}[t]{0.55\textwidth}
        \textbf{Peripherals: symbolic execution},\\
        using FSM's $\Delta t$ and $\Delta E$.
        \begin{itemize}
            \item \texttt{adc\_sense}
            \item \texttt{radio\_send}
        \end{itemize}
    \end{minipage}

    \bigskip

    \snippet

    \bigskip

    \begin{itemize}
        \item Supports binary images for MSP430FR5739.
        \item Simulates both continuous and intermittent power.
    \end{itemize}
\end{frame}

%    \frametitle{Implementation}
%
%    \begin{itemize}
%        \item Supports binary images for MSP430FR5739.
%        \item Uses ArchC [TODO ref] (SytemC).
%        \item Simulates both continuous and intermittent power.
%    \end{itemize}
%\end{frame}

\begin{frame}
    \frametitle{Results 1/2}

    \centering
    \input{figures/complete-wsn}
    Complete-WSN application
\end{frame}

\begin{frame}
    \frametitle{Results 2/2}

    \centering
    \input{figures/table-simu}
\end{frame}

\section{Conclusion}
\begin{frame}
    \frametitle{Roadmap}
    \centering
    \input{figures/roadmap2}
\end{frame}

\begin{frame}
    \frametitle{Conclusion}
    \begin{itemize}
        \item Fair energy consumption estimation using low-cost devices.
        \item API-level FSM for drivers.
        \item Cycle-accurate and symbolic simulation altogether.
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Perspectives}
    Towards finer Worst-Case Energy Consumption with peripherals.
    \lstWCEC

    \bigskip
    Protocol-level analysis (\textit{e.g.}, frame/packet aggregation).
    \begin{itemize}
        \item \textit{e.g.}, IEEE 802.11e/802.11n/02.11ac.
        \item Sending $X$ frames $\neq$ sending 1 frame $X$ times.
    \end{itemize}
\end{frame}

\begin{frame}
    \Huge
    \centering
    Thanks!
\end{frame}

\end{document}
