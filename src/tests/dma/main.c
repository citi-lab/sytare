#include <msp430.h>

#include <kernel.h>
#include <drivers/dma.h>

__attribute__((aligned(2)))
static char src[] = { "SytareSytare" };

__attribute__((aligned(2)))
static char dst_memcpy[sizeof (src)];

__attribute__((aligned(2)))
static char dst_memset[33];

int main (void)
{
    WDTCTL = WDTPW | WDTHOLD;

    // reset manually to 0 because global variables are persistent
    for(unsigned int i = 0; i < sizeof(dst_memcpy); i++) {
        dst_memcpy[i] = 0;
    }
    for(unsigned int i = 0; i < sizeof(dst_memset); i++) {
        dst_memset[i] = 0;
    }

    dma_memcpy(dst_memcpy, src, sizeof(src));
    dma_memset(dst_memset, 0x42, sizeof(dst_memset));

    // spin so we can verify via debugger
    while(1);

    return 0;
}
