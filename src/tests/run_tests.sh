#!/bin/bash

TOTAL=0
PASSED=0

for entry in *
do
	if [ ! -d $entry ] || [[ $entry == "build"* ]] ; then
		continue
	fi

	TOTAL=$(($TOTAL + 1))

	echo -n "=== Run test: $entry"

	mkdir -p $entry/build
	cd $entry/build

	OUT_CMAKE="$(cmake .. 2>&1)"
	RES_CMAKE=$?

	if [ ! $RES_CMAKE -eq 0 ]; then
		echo -e " -> \e[31mFAILED\e[0m"
		echo "$OUT_CMAKE"
	else
		OUT_MAKE="$(make tests 2>&1)"
		RES_MAKE=$?

		if [ ! $RES_MAKE -eq 0 ]; then
			echo -e " -> \e[31mFAILED\e[0m"
			echo "$OUT_MAKE"
		else
			echo -e " -> \e[32mPASSED\e[0m"
			PASSED=$(($PASSED + 1))
		fi
	fi

	cd ..
	rm -r build
	cd ..
done

FAILED=$(($TOTAL - $PASSED))

echo "=== Summary:"
echo "Total tests: $TOTAL"
echo "     Passed: $PASSED"
echo "     Failed: $FAILED"

exit $FAILED
