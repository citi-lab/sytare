include(${CMAKE_CURRENT_LIST_DIR}/Utils.cmake)
include_guard()


### Path definitions

# root of software, i.e. /src/ in Git repository
set(SYTARE_ROOT ${CMAKE_CURRENT_LIST_DIR}/..)

# global includes can be found here
set(SYTARE_INCLUDE ${SYTARE_ROOT}/include)

# here we store our debugging tools
set(SYTARE_TOOLS ${SYTARE_ROOT}/../tools)

### Toolchain and MCU selection

# if no other MCU is specified via `cmake -DMCU=msp430xxxx`, use default
if(NOT MCU)
	set(MCU msp430fr5739)
endif()

# if no other toolchain file is supplied, use the default one
if(NOT CMAKE_TOOLCHAIN_FILE)
	set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_LIST_DIR}/msp430-toolchain.cmake)
endif()

# force compiler detection now, so we can add global flags
enable_language(C)

get_filename_component(DEVICE_DIR ${CMAKE_AR} DIRECTORY)

### Compiler flags

add_compile_options(
    -Wall
    -Wextra
    -Wno-old-style-declaration
    -Werror
    -std=c99
	-mmcu=${MCU}        # same MCU for all code that is compiled
	-g                  # always generate symbols for debugging (doesn't hurt)
	-fno-common         # avoid COMMON symbols so we can rename sections
	-ffunction-sections # this will save around 5KB of memory together with
	                    # linker garbage collection for simple programs
	-Os					# for now we compile everything with size optimization
    -mdevices-csv-loc=${DEVICE_DIR}/../include/devices.csv
    )

set(SYTARE_KERNEL_CFLAGS
	-ffreestanding)


### Linker flags

# We cannot add the linker script via `link_libraries()` because this would
# append it multiple times for each linked library. This is a problem because
# each script specified via -T accumulates to the global linker script.
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -L${SYTARE_ROOT}/core -Tsytare.ld")


# workaround for newer toolchains that seem to ignore -mhwmult and cannot ded$
# which multiplication library to link
if(CMAKE_C_COMPILER_VERSION VERSION_GREATER 5)
	set(LIBMUL -lmul_f5)
endif()

# global linking flags that apply to kernel and applications
link_libraries(
	-mmcu=${MCU}
	-Wl,--gc-sections
	-nostartfiles -nodefaultlibs
	-Wl,--start-group		# declaring a group might help resolving symbols
	-lc
	-lgcc
	${LIBMUL}				# see workaround above
	-Wl,--end-group
	)
