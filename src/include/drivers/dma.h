/**
 *  \file   dma.h
 *  \brief  TI FR5739 board lib, dma
 *  \author Tristan Delizy
 *  \author Daniel Krebs
 *  \date   2015
 **/

#ifndef DMA_H
#define DMA_H

#include <stddef.h>

/** Fill the first 'len' bytes of 'dst' with the constant byte 'val'
 *
 * The CPU is halted during the DMA transfer.
 *
 * Returns:
 *  - 'dst' on success
 *  - NULL  on failure (wrong size, alignment)
 */
void * dma_memset(void * dst, unsigned char val, size_t len);

/** Copy 'len' bytes from 'src' to 'dst' via DMA
 *
 * The memory areas must not overlap. The CPU is halted during the DMA transfer.
 *
 * Returns:
 *  - 'dst' on success
 *  - NULL  on failure (wrong size, alignment)
 */
void * dma_memcpy(void * dst, const void * src, size_t len);

#endif
