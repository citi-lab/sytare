#ifndef DRIVERS_COMPARATOR_H
#define DRIVERS_COMPARATOR_H

#include <stdbool.h>
#include <msp430.h>

/* COMPARATOR_D CONFIG
 * The comparator will use VREF 1.5V with no resistor
 * ladder on minus input and compare it to the positive input.
 */
#define COMP_D_PORT_IN         1
#define COMP_D_PIN_IN          1

void comp_init(bool interrupts_en);
void comp_start(void);
void comp_stop(void);
void comp_set_res(unsigned int index);
void comp_set_vref(unsigned int index);
unsigned int comp_get_res(void);
unsigned int comp_get_vref(void);

static inline __attribute__((always_inline))
void comp_acknowledge_interrupt(void)
{
    CDINT &= ~CDIFG;
}

#endif // DRIVERS_COMPARATOR_H
