/**
 *  \file   leds.c
 *  \brief  TI FR5739 board lib, leds
 *  \author Tristan Delizy
 *  \date   2015
 *  \from   eZ430-RF2500 tutorial, leds
 *          (Antoine Fraboulet, Tanguy Risset,
 *          Dominique Tournier, Sebastien Mazy)
 **/


/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/led.h>
#include <drivers/port.h>

#include "kernel.h"

#define LED_PORT_1      5 // PJOUT
#define LED_PORT_2      3 // P3OUT

/******************************************************************/
/***************************************************** VARIABLES **/

static unsigned int leds[] = {LED_1, LED_2, LED_3, LED_4, LED_5, LED_6, LED_7, LED_8};

/************** ****************************************************/
/***************************************************** FUNCTIONS **/

/* void led_off(unsigned int led_num)
 * set a argument specified led to  off state
 * /!\ led numbering start at 1
 */
void led_off(unsigned int led_num)
{
    if(led_num < 5)
        syt_prt_out_bic(LED_PORT_1, leds[led_num-1]);
    else if(led_num < 9)
        syt_prt_out_bic(LED_PORT_2, leds[led_num-1]);

}

/* void led_on(unsigned int led_num)
 * set a argument specified led to on state
 * /!\ led numbering start at 1
 */
void led_on(unsigned int led_num)
{
    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(120);
    // SYT_DBG_PIN_LOW();

    if(led_num < 5)
        syt_prt_out_bis(LED_PORT_1,leds[led_num-1]);
    else if(led_num < 9)
        syt_prt_out_bis(LED_PORT_2,leds[led_num-1]);

    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(120);
    // SYT_DBG_PIN_LOW();
}

/* void led_switch(unsigned int led_num)
 * toggle an argument specified led state
 * /!\ led numbering start at 1
 */
void led_switch(unsigned int led_num)
{
    if(led_num < 5)
        (syt_prt_out_get(LED_PORT_1) & leds[led_num-1]) ? syt_prt_out_bic(LED_PORT_1,leds[led_num-1]) : syt_prt_out_bis(LED_PORT_1,leds[led_num-1]);
    else if(led_num < 9)
        (syt_prt_out_get(LED_PORT_2) & leds[led_num-1]) ? syt_prt_out_bic(LED_PORT_2,leds[led_num-1]) : syt_prt_out_bis(LED_PORT_2,leds[led_num-1]);
}

/* void leds_off(unsigned int led_num)
 * set all leds to off state
 */
void leds_off(void)
{
    syt_prt_out_bic(LED_PORT_1, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_out_bic(LED_PORT_2, (LED_5 | LED_6 | LED_7 | LED_8));
}

/* void leds_on(void)
 * set all leds to on state
 */
void leds_on(void)
{
    syt_prt_out_bis(LED_PORT_1, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_out_bis(LED_PORT_2, (LED_5 | LED_6 | LED_7 | LED_8));
}

/* void leds_init(void)
 * init of leds pins
 */
void leds_init(void)
{
    syt_prt_out_bic(LED_PORT_1, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_out_bic(LED_PORT_2, (LED_5 | LED_6 | LED_7 | LED_8));
    syt_prt_dir_bis(LED_PORT_1, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_dir_bis(LED_PORT_2, (LED_5 | LED_6 | LED_7 | LED_8));
    syt_prt_sel0_bic(LED_PORT_1, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_sel0_bic(LED_PORT_2, (LED_1 | LED_2 | LED_3 | LED_4));
    syt_prt_sel1_bic(LED_PORT_1, (LED_5 | LED_6 | LED_7 | LED_8));
    syt_prt_sel1_bic(LED_PORT_2, (LED_5 | LED_6 | LED_7 | LED_8));
}
