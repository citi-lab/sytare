#include <msp430.h>
#include <stdbool.h>

#include <drivers/comparator.h>
#include <drivers/utils.h>
#include <drivers/clock.h>

/******************************************************************/
/****************************** COMPARATOR_D MANAGMENT FUNCTIONS **/

/* void stop_comp(void) - ensure COMP_B is not running / stops it
 * /!\ this will force CDOUT to low.
 */
void comp_stop(void)
{
    CDCTL1 &= ~CDON;
}

/* void start_comp(void) - start the comparator_D
 */
void comp_start(void)
{
    CDINT &= ~(CDIFG | CDIIFG);     // clear any errant interrupts
    CDCTL1 |= CDON;                 // turn on the comparator
}

/* unsigned int init_comp(unsigned int interrupts_en) - init the comparator_D module
 * /!\ Does not start the module
 */
void comp_init(bool enable_interrupt)
{
    comp_stop();

    // configure input
    PXSEL0(COMP_D_PORT_IN) |= BITX(COMP_D_PIN_IN);
    PXSEL1(COMP_D_PORT_IN) |= BITX(COMP_D_PIN_IN);

    CDCTL0 &= ~(CDIMEN);                    // ensure not to use digital input for minus comparator input
    CDCTL0 |= (CDIPEN | CDIPSEL_1);         // Enable V+, input channel CD1
    CDCTL1 |= (CDMRVS | CDFDLY_3);          // enable VREF selection
    CDCTL1 &= ~(CDMRVL);                    // VREF0 is used
    CDCTL2 |= CDRSEL;                       // VREF is applied to V- input terminal
    CDCTL2 |= (CDRS_2 | CDREFL_1);          // shared reference vooltage at 1.5V
    CDCTL2 &= ~(CDREF0_31 | CDREF1_31);     // clear the resistor ladder configuration
    CDCTL3 |= BIT1;                         // Input Buffer Disable on input channel 1

    // 30us settling time for VREF (cf datasheet FR5739 p. 35 slas639i.pdf)
    clk_delay_micro(30);

    if(enable_interrupt) {
        CDINT  |= CDIE;                     // Enable CompB Interrupt
        CDCTL1 |= CDIES;                    // Choose falling edge
        CDINT &= ~(CDIFG);                  // Clear any errant interrupts
    }
}

/* unsigned int get_res_comp(void) - return the index of the resistor ladder
 * /!\ index between 1 and 32 (VREFx = Vin/index), 0 means no voltage division is made
 */
unsigned int comp_get_res(void)
{
    return (CDCTL2 & (CDREF00 | CDREF01 | CDREF02 | CDREF03 | CDREF04));
}

/* void set_res_comp(unsigned int index) - set the index of the resistor ladder
 * /!\ index between 1 and 32 (VREFx = Vin/index) index 0 will disable the resistor string.
 */
void comp_set_res(unsigned int index)
{
    CDCTL2 &= ~(CDREF0_31 | CDREF1_31 | CDRS_3);    // clear the resistor ladder configuration
    if(index == 0){
        CDCTL2 |= CDRS_3;                           // R-ladder off;
    } else {
        CDCTL2 |= (CDRS_2 | CDREFL_1);                  // R-ladder on & source ref 1.5V;
        CDCTL2 |= ((index-1) | ((index-1)<<8));     // set the resistors up
    }

}

/* unsigned int get_vref_comp(void) - return the reference voltage used for comparison (before resistor ladder)
* /!\ index between 1 and 3 (1=1.5V, 2=2.0V, 3=2.5V). Index 0 means the voltage source reference is disabled.
*/
unsigned int comp_get_vref(void)
{
    return ((CDCTL2 & (CDRS0 | CDRS1))>>13);
}

/* void set_vref_comp(unsigned int index) - set the reference voltage used for comparison (before resistor ladder)
* /!\ index between 1 and 3 (1=1.5V, 2=2.0V, 3=2.5V). Index 0 will disable the voltage source reference.
*/
void comp_set_vref(unsigned int index)
{
    CDCTL2 &= ~(CDRS0 | CDRS1);
    CDCTL2 |= (index << 13);
}
