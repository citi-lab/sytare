#include <msp430.h>
#include <drivers/adc.h>

/** Ensure ADC is not running
 *
 * Should not be mandatory as the ADC is able to stop itself if inactive
 * (see datasheet) but could be power saving on sensing application using ADC
 */
void adc_stop(void)
{
    ADC10CTL0 &= ~ADC10ENC;
    ADC10CTL0 &= ~ADC10ON;
}
