/**
 *  \file   drv_port.c
 *  \brief  TI FR5739 board lib, ports (GPIO and pin functions)
 *  \author Tristan Delizy
 *  \date   2016
 **/

/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <drivers/utils.h>
#include <drivers/port.h>
#include <drivers/dma.h>

// dbg
#include "kernel.h"


/******************************************************************/
/********************************************** INTERNAL DEFINES **/
#define NB_PORTS        5


/******************************************************************/
/********************************************************* TYPES **/
// structure describing the different configuration registers for an
// hardware port of the MSp430 digital I/O system.
struct hw_port_registers
{
    unsigned char                   out;
    unsigned char                   in;
    unsigned char                   dir;
    unsigned char                   sel0;
    unsigned char                   sel1;
    unsigned char                   ie;
    unsigned char                   ies;
    unsigned char                   ifg;
    unsigned char                   ren;
};

// sytare port driver data descriptor for persistency
struct prt_device_context_t
{
    struct hw_port_registers        p1;
    struct hw_port_registers        p2;
    struct hw_port_registers        p3;
    struct hw_port_registers        p4;
    struct hw_port_registers        pj;
};


/******************************************************************/
/***************************************************** VARIABLES **/

static
syt_dev_ctx_changes_t ctx_changes;

// local copy in RAM
static
struct prt_device_context_t prt_device_context;

static unsigned char * const out_ports[]   = {(unsigned char *)PAOUT_, (unsigned char *)(PAOUT_+1),    (unsigned char *)PBOUT_,    (unsigned char *)(PBOUT_+1),    (unsigned char *)PJOUT_ };
static unsigned char * const in_ports[]    = {(unsigned char *)PAIN_,  (unsigned char *)(PAIN_+1),     (unsigned char *)PBIN_,     (unsigned char *)(PBIN_+1),     (unsigned char *)PJIN_ };
static unsigned char * const dir_ports[]   = {(unsigned char *)PADIR_, (unsigned char *)(PADIR_+1),    (unsigned char *)PBDIR_,    (unsigned char *)(PBDIR_+1),    (unsigned char *)PJDIR_ };
static unsigned char * const sel0_ports[]  = {(unsigned char *)PASEL0_,(unsigned char *)(PASEL0_+1),   (unsigned char *)PBSEL0_,   (unsigned char *)(PBSEL0_+1),   (unsigned char *)PJSEL0_ };
static unsigned char * const sel1_ports[]  = {(unsigned char *)PASEL1_,(unsigned char *)(PASEL1_+1),   (unsigned char *)PBSEL1_,   (unsigned char *)(PBSEL1_+1),   (unsigned char *)PJSEL1_ };
static unsigned char * const ie_ports[]    = {(unsigned char *)PAIE_,  (unsigned char *)(PAIE_+1),     (unsigned char *)PBIE_,     (unsigned char *)(PBIE_+1),     0};
static unsigned char * const ies_ports[]   = {(unsigned char *)PAIES_, (unsigned char *)(PAIES_+1),    (unsigned char *)PBIES_,    (unsigned char *)(PBIES_+1),    0};
static unsigned char * const ifg_ports[]   = {(unsigned char *)PAIFG_, (unsigned char *)(PAIFG_+1),    (unsigned char *)PBIFG_,    (unsigned char *)(PBIFG_+1),    0};
static unsigned char * const ren_ports[]   = {(unsigned char *)PAREN_, (unsigned char *)(PAREN_+1),    (unsigned char *)PBREN_,    (unsigned char *)(PBREN_+1),    (unsigned char *)PJREN_ };

/******************************************************************/
/********************************************** DRIVER FUNCTIONS **/
void prt_drv_init(void)
{
    syt_drv_register(prt_drv_save, prt_drv_restore,
                    sizeof(struct prt_device_context_t));

    // p1
    prt_device_context.p1.out = P1OUT;
    prt_device_context.p1.dir = P1DIR;
    prt_device_context.p1.sel0 = P1SEL0;
    prt_device_context.p1.sel1 = P1SEL1;
    prt_device_context.p1.ie = P1IE;
    prt_device_context.p1.ies = P1IES;
    prt_device_context.p1.ifg = P1IFG;
    prt_device_context.p1.ren = P1REN;

    // p2
    prt_device_context.p2.out = P2OUT;
    prt_device_context.p2.dir = P2DIR;
    prt_device_context.p2.sel0 = P2SEL0;
    prt_device_context.p2.sel1 = P2SEL1;
    prt_device_context.p2.ie = P2IE;
    prt_device_context.p2.ies = P2IES;
    prt_device_context.p2.ifg = P2IFG;
    prt_device_context.p2.ren = P2REN;

    // p3
    prt_device_context.p3.out = P3OUT;
    prt_device_context.p3.dir = P3DIR;
    prt_device_context.p3.sel0 = P3SEL0;
    prt_device_context.p3.sel1 = P3SEL1;
    prt_device_context.p3.ie = P3IE;
    prt_device_context.p3.ies = P3IES;
    prt_device_context.p3.ifg = P3IFG;
    prt_device_context.p3.ren = P3REN;

    // p4
    prt_device_context.p4.out = P4OUT;
    prt_device_context.p4.dir = P4DIR;
    prt_device_context.p4.sel0 = P4SEL0;
    prt_device_context.p4.sel1 = P4SEL1;
    prt_device_context.p4.ie = P4IE;
    prt_device_context.p4.ies = P4IES;
    prt_device_context.p4.ifg = P4IFG;
    prt_device_context.p4.ren = P4REN;

    // pj
    prt_device_context.pj.out = PJOUT;
    prt_device_context.pj.dir = PJDIR;
    prt_device_context.pj.sel0 = PJSEL0;
    prt_device_context.pj.sel1 = PJSEL1;
    prt_device_context.pj.ren = PJREN;

    drv_mark_dirty(&ctx_changes);
}


void prt_drv_save(int drv_handle)
{
    drv_save(&ctx_changes,
             syt_drv_get_ctx_next(drv_handle),
             &prt_device_context,
             sizeof(prt_device_context));
}


void prt_drv_restore(int drv_handle)
{
    dma_memcpy(&prt_device_context,
               syt_drv_get_ctx_last(drv_handle),
               sizeof(prt_device_context));

    // p1
    P1OUT = prt_device_context.p1.out;
    P1DIR = prt_device_context.p1.dir;
    P1SEL0 = prt_device_context.p1.sel0;
    P1SEL1 = prt_device_context.p1.sel1;
    P1IE = prt_device_context.p1.ie;
    P1IES = prt_device_context.p1.ies;
    P1IFG = prt_device_context.p1.ifg;
    P1REN = prt_device_context.p1.ren;

    // p2
    P2OUT = prt_device_context.p2.out;
    P2DIR = prt_device_context.p2.dir;
    P2SEL0 = prt_device_context.p2.sel0;
    P2SEL1 = prt_device_context.p2.sel1;
    P2IE = prt_device_context.p2.ie;
    P2IES = prt_device_context.p2.ies;
    P2IFG = prt_device_context.p2.ifg;
    P2REN = prt_device_context.p2.ren;

    // p3
    P3OUT = prt_device_context.p3.out;
    P3DIR = prt_device_context.p3.dir;
    P3SEL0 = prt_device_context.p3.sel0;
    P3SEL1 = prt_device_context.p3.sel1;
    P3IE = prt_device_context.p3.ie;
    P3IES = prt_device_context.p3.ies;
    P3IFG = prt_device_context.p3.ifg;
    P3REN = prt_device_context.p3.ren;

    // p4
    P4OUT = prt_device_context.p4.out;
    P4DIR = prt_device_context.p4.dir;
    P4SEL0 = prt_device_context.p4.sel0;
    P4SEL1 = prt_device_context.p4.sel1;
    P4IE = prt_device_context.p4.ie;
    P4IES = prt_device_context.p4.ies;
    P4IFG = prt_device_context.p4.ifg;
    P4REN = prt_device_context.p4.ren;

    // pj
    PJOUT = prt_device_context.pj.out;
    PJDIR = prt_device_context.pj.dir;
    PJSEL0 = prt_device_context.pj.sel0;
    PJSEL1 = prt_device_context.pj.sel1;
    PJREN = prt_device_context.pj.ren;

    // mark as dirty to make sure that it will be saved to the new checkpoint
    // that is still uninitialized after driver restoration
    drv_mark_dirty(&ctx_changes);
}

// OUT -----------------------------------------------------------//
void prt_out_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;
    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(60);
    // SYT_DBG_PIN_LOW();

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(out_ports[port-1]) |= mask;
    p->out |= mask;

    // SYT_DBG_PIN_HIGH();
    // __delay_cycles(60);
    // SYT_DBG_PIN_LOW();

    drv_dirty_range(&ctx_changes, &(p->out), 1);

    return;
}

void prt_out_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(out_ports[port-1]) &= ~(mask);
    p->out &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->out), 1);

    return;
}

unsigned char prt_out_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(out_ports[port-1]);
}

// IN ------------------------------------------------------------//
unsigned char prt_in_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(in_ports[port-1]);
}

// DIR -----------------------------------------------------------//
void prt_dir_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(dir_ports[port-1]) |= mask;
    p->dir |= mask;

    drv_dirty_range(&ctx_changes, &(p->dir), 1);

    return;
}

void prt_dir_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(dir_ports[port-1]) &= ~(mask);
    p->dir &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->dir), 1);

    return;
}

unsigned char prt_dir_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(dir_ports[port-1]);
}

// SEL0 ----------------------------------------------------------//
void prt_sel0_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(sel0_ports[port-1]) |= mask;
    p->sel0 |= mask;

    drv_dirty_range(&ctx_changes, &(p->sel0), 1);

    return;
}

void prt_sel0_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(sel0_ports[port-1]) &= ~(mask);
    p->sel0 &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->sel0), 1);

    return;
}

unsigned char prt_sel0_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(sel0_ports[port-1]);
}

// SEL1 ----------------------------------------------------------//
void prt_sel1_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(sel1_ports[port-1]) |= mask;
    p->sel1 |= mask;

    drv_dirty_range(&ctx_changes, &(p->sel1), 1);

    return;
}

void prt_sel1_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(sel1_ports[port-1]) &= ~(mask);
    p->sel1 &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->sel1), 1);

    return;
}

unsigned char prt_sel1_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(sel1_ports[port-1]);
}

// IE ------------------------------------------------------------//
void prt_ie_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ie_ports[port-1]) |= mask;
    p->ie |= mask;

    drv_dirty_range(&ctx_changes, &(p->ie), 1);

    return;
}

void prt_ie_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ie_ports[port-1]) &= ~(mask);
    p->ie &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->ie), 1);

    return;
}

unsigned char prt_ie_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(ie_ports[port-1]);
}

// IES -----------------------------------------------------------//
void prt_ies_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ies_ports[port-1]) |= mask;
    p->ies |= mask;

    drv_dirty_range(&ctx_changes, &(p->ies), 1);

    return;
}

void prt_ies_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ies_ports[port-1]) &= ~(mask);
    p->ies &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->ies), 1);

    return;
}

unsigned char prt_ies_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(ies_ports[port-1]);
}

// IFG -----------------------------------------------------------//
void prt_ifg_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ifg_ports[port-1]) |= mask;
    p->ifg |= mask;

    drv_dirty_range(&ctx_changes, &(p->ifg), 1);

    return;
}

void prt_ifg_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ifg_ports[port-1]) &= ~(mask);
    p->ifg &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->ifg), 1);

    return;
}

unsigned char prt_ifg_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(ifg_ports[port-1]);
}

// REN -----------------------------------------------------------//
void prt_ren_bis(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ren_ports[port-1]) |= mask;
    p->ren |= mask;

    drv_dirty_range(&ctx_changes, &(p->ren), 1);

    return;
}

void prt_ren_bic(unsigned char port, unsigned char mask)
{
    struct hw_port_registers *p;

    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    p = ((struct hw_port_registers *)(&prt_device_context.p1) + (port-1));

    *(ren_ports[port-1]) &= ~(mask);
    p->ren &= ~(mask);

    drv_dirty_range(&ctx_changes, &(p->ren), 1);

    return;
}

unsigned char prt_ren_get(unsigned char port)
{
    if(port > NB_PORTS || !port)
        syt_kernel_panic();

    return *(ren_ports[port-1]);
}
