/**
 *  \file   demo_sense.c
 *  \brief  SYTARE demonstration application for temperature
            sensor hardware and driver state persistance.
 *  \author Tristan Delizy
 *  \date   2016
 **/

#include <msp430.h>
#include <drivers/led.h>
#include <drivers/clock.h>
#include <drivers/utils.h>
#include <drivers/port.h>
#include <drivers/temperature.h>
#include "kernel.h"

/* this application is a demonstration for the SYTARE project
 * compiling this along with the SYTARE project will allow to
 * run this application on TI FR5739 board under intermittent
 * power supply conditions.
 */


int main (void)
{
    unsigned int data[8] = {0};
    char temp = 0;
    unsigned int mean = 0;
    unsigned int min = 0xFFFF;
    unsigned int max = 0;

    size_t j = 0;

    WDTCTL = WDTPW | WDTHOLD;

    syt_prt_drv_init();
    leds_init();
    syt_clk_drv_init();
    syt_tmp_drv_init();

    for(;;)
    {
        // collect temperature each 5 millisecond for 10 times
        for(size_t i = 0; i < 10; i++)
        {
            temp = (char)tmp_drv_sample();
            mean += temp;
            max = max_int((int)max, (int)temp);
            min = min_int((int)min, (int)temp);
            clk_delay_ms(5);
        }

        // store the mean inside an array
        data[j] = mean;
        temp = data[j]; // dummy read to justify the existence of "data" from the compiler's point of view
        j = (j+1)%8;

        // end of work loop signal
        SYT_DBG_PIN_HIGH();
        __delay_cycles(10000);
        SYT_DBG_PIN_LOW();
        __delay_cycles(10000);
        SYT_DBG_PIN_HIGH();
        __delay_cycles(10000);
        SYT_DBG_PIN_LOW();
        __delay_cycles(10000);
        SYT_DBG_PIN_HIGH();
        __delay_cycles(10000);
        SYT_DBG_PIN_LOW();
        __delay_cycles(10000);
        SYT_DBG_PIN_HIGH();
        __delay_cycles(10000);
        SYT_DBG_PIN_LOW();
        for(;;){
            __asm__ __volatile__(   "nop\n\t"
                                    : // no output
                                    : // no input
                                    );
        }
    }
    return 0;
}

