cmake_minimum_required(VERSION 3.0)
include(../../cmake/Sytare-Application.cmake)

project(app_leds C)

add_executable(app_leds main.c)
target_program(app_leds)
target_dump_listing(app_leds)
