/**
 *  \file   demo_leds.c
 *  \brief  SYTARE demonstration application for leds
            state persistance.
 *  \author Tristan Delizy
 *  \date   2016
 **/

#include <msp430.h>
#include <drivers/led.h>
#include <drivers/utils.h>
#include <drivers/port.h>

#include "kernel.h"

/* this application is a demonstration for the SYTARE project
 * compiling this along with the SYTARE project will allow to
 * run this application on TI FR5739 board under intermittent
 * power supply conditions.
 */

int main(void)
{
    unsigned int c = 1;

    syt_prt_drv_init();

    leds_init();
    leds_off();

    while(1)
    {
        c=0;
        while(c < 256) {
            leds_off();
            
            for(unsigned int i = 0; i < 8; i++) {
                if((1 << i) & c) {
                    led_on(i + 1);
                }
            }
            
            c++;

            __delay_cycles(3000000);// Note: sytare assumes CPU@24MHz
        }
    }
    return 0;
}
