# Sources for the .deb packages:

## Linux intel 32-bits

- https://launchpad.net/ubuntu/precise/i386/libudev-dev/175-0ubuntu9
- https://launchpad.net/ubuntu/precise/i386/libusb-1.0-0/2:1.0.9~rc3-2ubuntu1

## Linux intel 64-bits

- http://packages.ubuntu.com/precise/amd64/libudev0/download
- http://packages.ubuntu.com/precise/amd64/libudev-dev/download
- http://packages.ubuntu.com/precise/amd64/libusb-1.0-0-dev/download

## Linux ARM Raspberry pi

- https://packages.debian.org/wheezy/libudev-dev
- http://archive.raspbian.org/raspbian/pool/main/libu/libusbx/libusb-1.0-0-dev_1.0.11-1_armhf.deb
