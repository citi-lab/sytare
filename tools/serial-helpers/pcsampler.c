#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/time.h>
#include <libserialport.h>

bool syncword(uint8_t c)
{
	static const uint8_t sfd[] = {0x12, 0x34, 0x56, 0x78};
	static size_t idx = 0;

	if(sfd[idx] == c) {
		idx++;
	} else {
		idx = 0;
	}

	if(idx == sizeof(sfd)) {
		idx = 0;
		return true;
	}

	return false;
}

int main(int argc, const char* argv[])
{
	struct sp_port* port;
	const char* port_name = argv[1];
	const char* file_name = argv[2];

	if(sp_get_port_by_name(port_name, &port) != SP_OK) {
		printf("Cannot open port %s\n", argv[1]);
		return 1;
	}

	if(access(file_name, F_OK) != 0) {
		printf("Cannot open file %s\n", file_name);
		return 1;
	}

	sp_open(port, SP_MODE_READ);
	sp_set_baudrate(port, 115200);
	sp_set_xon_xoff(port, SP_XONXOFF_DISABLED);


	bool sync = false;
	while(!sync) {
		uint8_t c;
		int ret = 0;
		while(ret <= 0) {
			ret = sp_blocking_read(port, &c, sizeof(c), 0);
		}

		sync = syncword(c);
	}

	while(1) {

		uint8_t pc[2];
		int ret = 0;
		while(ret <= 0) {
			ret = sp_blocking_read(port, &pc, sizeof(pc), 0);
		}

		if(syncword(pc[0])) {
			// pc incomplete
			pc[0] = pc[1];
			sp_blocking_read(port, &pc[1], 1, 0);
			printf("\n");
			continue;
		} else if(syncword(pc[1])) {
			printf("\n");
			continue;
		}

		char cmd[128];
		snprintf(cmd, sizeof(cmd), "msp430-elf-addr2line -i -f -a -p -s -e %s 0x%02x%02x", file_name, pc[0], pc[1]);

		struct timeval t;
		gettimeofday(&t, NULL);
		printf("[%d.%06d] ", t.tv_sec, t.tv_usec);
		fflush(0);

		system(cmd);
	}
}
